def coef_dir(p,q):
    return (p[1] - q[1])/(p[0] - q[0])

def abscisse_origine(p,q):
    return p[1] - coef_dir(p,q) * p[0]

def param_droite(p,q):
    return (coef_dir(p,q),abscisse_origine(p,q))

def point_sur_la_droite(a,b,x):
    return (x,a*x+b)


def dist2(p,q):
    return (p[0]-q[0])**2+(p[1]-q[1])**2

def tension(p,a,b):
    return(dist2(p,point_sur_la_droite(a,b,p[0])))

def action(nuage,a,b):
    action = 0
    for p in nuage:
        action += tension(p,a,b)
    return action