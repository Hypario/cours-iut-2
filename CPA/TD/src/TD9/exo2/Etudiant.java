package TD9.exo2;

public class Etudiant {

	public String prenom, dpt;

	public int age;

	Etudiant(String prenom, int age, String dpt) {
		this.prenom = prenom;
		this.age = age;
		this.dpt = dpt;
	}

	@Override
	public String toString() {
		return prenom + " a " + age + " ans et, est au département " + dpt;
	}
}
