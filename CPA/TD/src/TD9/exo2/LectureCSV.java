package TD9.exo2;

import java.util.ArrayList;
import java.util.Scanner;

public class LectureCSV {

	public static void main(String[] args) {

		ArrayList<Etudiant> liste = new ArrayList<>();

		Scanner scanner = new Scanner(System.in);

		scanner.useDelimiter("[,\n]");

		while (scanner.hasNext()) {
			liste.add(new Etudiant(scanner.next(), scanner.nextInt(), scanner.next()));
		}

		for (Etudiant etudiant : liste) {
			System.out.println(etudiant);
		}
	}

}
