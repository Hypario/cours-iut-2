package TD9.exo1;

public interface Collection {

	void addChannel(Channel chaine);

	void removeChannel(Channel chaine);

	Iterateur getIterateur(ChaineLangue langue);

}
