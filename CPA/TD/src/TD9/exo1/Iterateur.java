package TD9.exo1;

public interface Iterateur {

	void first();

	boolean hasNext();

	Channel next();
}
