package TD9.exo1;

public class Test {

	public static void main(String[] args) {

		Channel anglais = new Channel(ChaineLangue.ANGLAIS.name(), "108.0");
		Channel anglais2 = new Channel(ChaineLangue.ANGLAIS.name(), "188.0");
		Channel arab = new Channel(ChaineLangue.ARABE.name(), "123");

		ChannelCollection collection = new ChannelCollection();
		collection.addChannel(anglais);
		collection.addChannel(anglais2);
		collection.addChannel(arab);

		Iterateur it = collection.getIterateur(ChaineLangue.ANGLAIS);

		System.out.println(it.hasNext());

		while(it.hasNext()) {
			Channel chaine = it.next();
			System.out.println("chaine : " + chaine.getLangue() + " fréquence : " + chaine.getFrq());
		}

	}

}
