package TD9.exo1;

import java.util.ArrayList;

public class ChannelCollection implements Collection {

	ArrayList<Channel> liste = new ArrayList<>();

	@Override
	public void addChannel(Channel chaine) {
		liste.add(chaine);
	}

	@Override
	public void removeChannel(Channel chaine) {
		liste.remove(chaine);
	}

	@Override
	public Iterateur getIterateur(ChaineLangue langue) {
		return new ChannelIterateur(langue.name(), liste);
	}
}
