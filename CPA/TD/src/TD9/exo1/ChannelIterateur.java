package TD9.exo1;

import java.util.List;

public class ChannelIterateur implements Iterateur {

	String langue;
	List<Channel> liste;

	int index = 0;

	ChannelIterateur(String langue, List<Channel> liste) {
		this.langue = langue;
		this.liste = liste;
	}

	@Override
	public void first() {
		Channel chaine = liste.get(index);
		System.out.println("Chaine " + chaine.getLangue() + " fréquence : " + chaine.getFrq());
	}

	@Override
	public boolean hasNext() {

		boolean found = false;

		while (index < liste.size() && !found) {
			if (liste.get(index).getLangue().equals(langue) || langue.equals("TOUTE")) {
				found = true;
			} else {
				index++;
			}
		}
		return found;
	}

	public Channel next() {
		return liste.get(index++);
	}
}
