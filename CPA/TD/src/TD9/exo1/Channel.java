package TD9.exo1;

public class Channel {

	private String langue, frq;

	Channel(String langue, String frequence) {
		this.langue = langue;
		this.frq = frequence;
	}

	public String getLangue() {
		return langue;
	}

	public String getFrq() {
		return frq;
	}
}
