package TD6.exo2;

import java.io.Reader;

public class MajReader extends DecoratorReader {
	public MajReader(Reader in, int sz) {
		super(in, sz);
	}

	public MajReader(Reader in) {
		super(in);
	}

	@Override
	public String traite(String string) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c >= 'a' && c <= 'z') c += ('A' - 'a');
			result.append(c);
		}
		return result.toString();
	}
}
