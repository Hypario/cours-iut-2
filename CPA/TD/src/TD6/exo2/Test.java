package TD6.exo2;

import java.io.IOException;
import java.io.InputStreamReader;

public class Test {

	public static void main(String[] args) {
		DecoratorReader reader = new MajReader(new InputStreamReader(System.in));
		DecoratorReader reader2 = new CryptoReader(new InputStreamReader(System.in));

		try {
			System.out.println("Mis en majuscule");
			String text = reader.readLine();
			System.out.println(reader.traite(text));
			System.out.println("Chiffré");
			text = reader2.readLine();
			System.out.println(reader2.traite(text));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
