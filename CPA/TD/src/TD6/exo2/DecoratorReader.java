package TD6.exo2;

import java.io.BufferedReader;
import java.io.Reader;

public abstract class DecoratorReader extends BufferedReader {

	public DecoratorReader(Reader in, int sz) {
		super(in, sz);
	}

	public DecoratorReader(Reader in) {
		super(in);
	}

	public abstract String traite(String string);


}
