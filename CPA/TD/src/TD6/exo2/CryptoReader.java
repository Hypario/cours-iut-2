package TD6.exo2;

import java.io.Reader;

public class CryptoReader extends DecoratorReader {
	public CryptoReader(Reader in, int sz) {
		super(in, sz);
	}

	public CryptoReader(Reader in) {
		super(in);
	}

	@Override
	public String traite(String string){
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c >= 'a' && c <= 'm') c += 13;
			else if (c >= 'A' && c <= 'M') c += 13;
			else if (c >= 'n' && c <= 'z') c -= 13;
			else if (c >= 'N' && c <= 'Z') c -= 13;
			result.append(c);
		}
		return result.toString();
	}
}
