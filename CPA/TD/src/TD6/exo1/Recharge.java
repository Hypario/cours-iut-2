package TD6.exo1;

public abstract class Recharge extends Recipient {

	private Recipient recipient;

	Recharge(float prix, Recipient recipient) {
		super(prix);
		this.recipient = recipient;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	@Override
	public float getPrix() {
		return super.getPrix() + recipient.getPrix();
	}
}
