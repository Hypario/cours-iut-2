package TD6.exo1;

public class Pralines extends Recharge {

	Pralines(Recipient recipient) {
		super(1.25f, recipient);
	}

	Pralines(float prix, Recipient recipient) {
		super(prix, recipient);
	}

	@Override
	public String getLabel() {
		return getRecipient().getLabel() + " remplis de praline";
	}
}
