package TD6.exo1;

public class SeauSquelette extends Recipient {

	SeauSquelette() {
		super(1.5f);
	}

	SeauSquelette(float prix) {
		super(prix);
	}

	@Override
	public String getLabel() {
		return super.getLabel() + "un seau squelette";
	}
}
