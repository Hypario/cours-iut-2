package TD6.exo1;

public class Caramels extends Recharge {
	Caramels(Recipient recipient) {
		super(1.75f, recipient);
	}

	Caramels(float prix, Recipient recipient) {
		super(prix, recipient);
	}

	public String getLabel() {
		return getRecipient().getLabel() + " remplis de caramel";
	}

}
