package TD6.exo1;

public class Guimauves extends Recharge {

	Guimauves(Recipient recipient) {
		super(1.5f, recipient);
	}

	Guimauves(float prix, Recipient recipient) {
		super(prix, recipient);
	}

	@Override
	public String getLabel() {
		return getRecipient().getLabel() + " remplis de guimauve";
	}
}
