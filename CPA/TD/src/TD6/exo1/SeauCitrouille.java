package TD6.exo1;

public class SeauCitrouille extends Recipient {

	SeauCitrouille() {
		super(2.5f);
	}

	SeauCitrouille(float prix) {
		super(prix);
	}

	@Override
	public String getLabel() {
		return super.getLabel() + " un seau citrouille";
	}
}
