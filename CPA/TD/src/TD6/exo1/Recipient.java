package TD6.exo1;

public abstract class Recipient {

	private float prix;

	private String label = "Il a acheté";

	Recipient(float prix) {
		this.prix = prix;
	}

	public String getLabel() {
		return this.label;
	}

	public float getPrix() {
		return prix;
	}

	@Override
	public String toString() {
		return getLabel() + " au prix de " + getPrix() + " euros";
	}
}
