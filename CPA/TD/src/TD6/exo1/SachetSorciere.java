package TD6.exo1;

public class SachetSorciere extends Recipient {

	SachetSorciere() {
		super(0.5f);
	}

	SachetSorciere(float prix) {
		super(prix);
	}

	@Override
	public String getLabel() {
		return super.getLabel() + " un sachet citrouille";
	}
}
