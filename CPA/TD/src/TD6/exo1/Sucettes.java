package TD6.exo1;

public class Sucettes extends Recharge {

	Sucettes(Recipient recipient) {
		super(2f, recipient);
	}

	Sucettes(float prix, Recipient recipient) {
		super(prix, recipient);
	}

	@Override
	public String getLabel() {
		return getRecipient().getLabel() + " remplis de sucette";
	}
}
