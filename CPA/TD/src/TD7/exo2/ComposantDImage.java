package TD7.exo2;


import javax.swing.*;
import java.awt.*;

class ComposantDImage extends JPanel {
    private Icon icone = null;

    public ComposantDImage() {
        this.setPreferredSize(new Dimension(800,600));
        this.setMinimumSize(new Dimension(800,600));
        setBorder(BorderFactory.createLineBorder(Color.BLUE));
        setOpaque(true);
    }

    public void setIcon(Icon icone) {
        this.icone = icone;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        System.out.println(String.format("taille composant %d %d", this.getWidth(),this.getHeight()));
        if (icone != null) {
            int w = icone.getIconWidth();
            int h = icone.getIconHeight();
            int x = (this.getWidth() - w) / 2;
            int y = (this.getHeight() - h) / 2;
            System.out.println(String.format("taille icone %d %d", w,h));
            icone.paintIcon(this,g,x,y);
        } else {
            g.drawString("Pas d'icone ...",(this.getWidth() - 15)/2, this.getHeight()/2);
        }
    }
}
