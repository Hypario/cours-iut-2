package TD7.exo2;

import java.io.IOException;
import java.net.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class TestProxyDImage extends JFrame {
    private ComposantDImage composant;
    private Map<String, String> cd = new Hashtable<>();

    private void initCollection() {
        cd.put("La marche de l'empereur", " http://images-eu.amazon.com/images/P/B00070FX3M.08.LZZZZZZZ.jpg");
        cd.put("Ferré : L'espoir", " http://images-eu.amazon.com/images/P/B00008ZPD3.08.LZZZZZZZ.jpg ");
        cd.put("Excalibur", "http://images-eu.amazon.com/images/P/B00004VRKV.08.LZZZZZZZ.jpg");
        cd.put("Carlos Nunez", "http://images-eu.amazon.com/images/P/B000063WSL.08.LZZZZZZZ.jpg");
        cd.put("Variations Goldberg", "http://images-eu.amazon.com/images/P/B000025NYA.08.LZZZZZZZ.jpg");
        cd.put("Aubry : Signes", " http://images-eu.amazon.com/images/P/B0000085FR.08.LZZZZZZZ.jpg ");
        cd.put("Rokia Traoré", "http://images-eu.amazon.com/images/P/B0002M5T9I.01.LZZZZZZZ.jpg");
    }

    private JMenuBar createMenu() {
        JMenuBar barreDeMenus = new JMenuBar();
        JMenu menu = new JMenu("CD favoris");
        barreDeMenus.add(menu);
        for (String nom : cd.keySet()) {
            JMenuItem menuItem = new JMenuItem(nom);
            menu.add(menuItem);
            menuItem.addActionListener(event -> composant.setIcon(new ProxyImage(getUrlCD(event.getActionCommand()))));
        }
        return barreDeMenus;
    }

    public TestProxyDImage() {
        setTitle("Pochettes de CD");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initCollection();
        setJMenuBar(createMenu());
        composant = new ComposantDImage();
        getContentPane().add(composant);
        pack();
        setVisible(true);
    }

    URL getUrlCD(String nom) {
        try {
            return (new URL(cd.get(nom)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void main(String[] args) {
        new TestProxyDImage();
    }


}
