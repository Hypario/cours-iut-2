package TD7.exo2;

import javax.swing.*;
import java.awt.*;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

public class ProxyImage implements Icon {

    /**
     * url de la pochette
     */
    private URL urlCD;

    /**
     * l'image
     */
    private ImageIcon image;

    /**
     * Dans le cas où on est à l'IUT
     */
    private Proxy proxy;

    private boolean loading;

    public ProxyImage(URL urlCD) {
        this.urlCD = urlCD;
        loading = true;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int x, int y) {
        // si chargement
        if (loading) {
            // on lance une nouvelle tâche
            Thread threadChargement = new Thread(() -> {
                // on définie une co HTTP
                HttpURLConnection connexion;

                // nombre octet lu
                int nbLu;

                try {
                    // on se connecte
                    if (proxy != null)
                        connexion = (HttpURLConnection) urlCD.openConnection(proxy);
                    else
                        connexion = (HttpURLConnection) urlCD.openConnection();

                    // tableau de byte après connexion
                    byte[] bytes = new byte[connexion.getContentLength()];

                    int totalLu = 0;

                    // on lis tout les bytes
                    while (totalLu < bytes.length) {
                        nbLu = connexion.getInputStream().read(bytes, totalLu, bytes.length - totalLu);
                        totalLu += nbLu;
                    }

                    // on ajoute l'image au composant et le repaint
                    image = new ImageIcon(bytes, "Pochettes de CD");
                    component.repaint();

                    // download et chargement terminé
                    loading = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            // on lance le thread
            threadChargement.start();
        } else {
            image.paintIcon(component, graphics, x, y);
        }
    }

    @Override
    public int getIconWidth() {
        return loading ? 0 : image.getIconWidth();
    }

    @Override
    public int getIconHeight() {
        return loading ? 0 : image.getIconHeight();
    }
}
