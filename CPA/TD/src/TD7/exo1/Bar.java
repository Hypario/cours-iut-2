package TD7.exo1;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Bar{

	private String filename;
	private ProxyBarman barman = new ProxyBarman();

	public HashMap<Integer, Client> clients = new HashMap<>();

	Bar(String filename) {
		this.filename = filename;
		readFile();
	}

	public void boireTous() {
		for (int i = 1; i <= clients.size(); i++) {
			barman.servirVodka(clients.get(i));
		}
	}

	public boolean veutBoire(int idClient) {
		Client client;
		return ((client = clients.get(idClient)) != null && client.getAge() >= 18);
	}

	private void readFile() {
		try (Scanner in = new Scanner(new File(filename))) {
			in.useDelimiter("\\s+");
			while (in.hasNext()) {
				clients.put(in.nextInt(), new Client(in.next(), in.nextInt()));
			}
		} catch (IOException e) {
			System.out.println("Le fichier " + filename + " est inacessible, êtes-vous sûr qu'il existe et que vous avez les droits ?");
		}
	}

}
