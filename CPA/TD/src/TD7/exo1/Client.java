package TD7.exo1;

public class Client {

	private String name;

	private int age;

	Client(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return this.name;
	}

	public int getAge() {
		return this.age;
	}

}
