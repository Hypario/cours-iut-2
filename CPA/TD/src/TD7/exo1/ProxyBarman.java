package TD7.exo1;

public class ProxyBarman {

	private Barman barman;

	ProxyBarman() {
		this.barman = new Barman();
	}

	public void servirVodka(Client client) {
		if (client.getAge() >= 18) {
			barman.servirVodka(client);
		} else {
			System.out.println("Le barman ne sert pas de vodka à " + client.getName());
		}
	}

}
