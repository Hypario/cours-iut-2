package TD3;

public class Jeu {

    public static void main(String[] args) {
        Ecureuil ec = new Ecureuil(new BombardeNoisette(), new DansArbre());
        ec.combat();

        Mouffette mouf = new Mouffette(new ProjetteLiquide(), new Trottine());
        mouf.combat();
        mouf.deplacer();

        mouf.setsAtt(new Rouler());
        mouf.combat();
        mouf.setsDep(new DansArbre());
        mouf.deplacer();
    }

}
