package TD3;

public abstract class Animal {

    protected StratCombat sAtt;
    protected StratDeplacement sDep;

    Animal(StratCombat sAtt, StratDeplacement sDep) {
        this.sAtt = sAtt;
        this.sDep = sDep;
    }

    public void deplacer() {
        sDep.seDeplacer(getClass().getName());
    }

    public void combat() {
        sAtt.combattre(getClass().getName());
    }

    public void setsAtt(StratCombat sAtt) {
        this.sAtt = sAtt;
    }

    public void setsDep(StratDeplacement sDep) {
        this.sDep = sDep;
    }
}
