package TD5.exo1;

public interface ISujet {
    void ajouterObservateur(IObservateur observateur);

    void supprimerObservateur(IObservateur observateur);

    void notifierObservateurs();

}
