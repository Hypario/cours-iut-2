package TD5.exo1;

public class Test {

    public static void main(String[] args) {
        Quotidien journal = new Quotidien(1, "Hello World", "Bienvenu dans le monde de la programmation");

        AbonnePapier abonne = new AbonnePapier();
        journal.ajouterObservateur(abonne);

        journal.notifierObservateurs();

    }

}
