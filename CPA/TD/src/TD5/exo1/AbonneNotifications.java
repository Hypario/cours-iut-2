package TD5.exo1;

public class AbonneNotifications implements IObservateur {

    @Override
    public void actualiser(ISujet sujet) {
        if (sujet instanceof Journal) {
            int numero = ((Journal) sujet).getNumero();
            String titre = ((Journal) sujet).getTitre();
            System.out.println("nouveau journal " + sujet.getClass().getSimpleName() + " numéro " + numero + " " + titre);
        }
    }
}
