package TD5.exo1;

import java.util.ArrayList;

public abstract class Journal implements ISujet{

    protected ArrayList<IObservateur> listeAbonnes = new ArrayList<>();

    protected int numero;
    protected String titre, contenu;

    Journal(int numero, String titre, String contenu) {
        this.numero = numero;
        this.titre = titre;
        this.contenu = contenu;
    }

    @Override
    public void ajouterObservateur(IObservateur observateur) {
        listeAbonnes.add(observateur);
    }

    @Override
    public void supprimerObservateur(IObservateur observateur) {
        listeAbonnes.remove(observateur);
    }

    @Override
    public void notifierObservateurs() {
        for (IObservateur abonnes : listeAbonnes) {
            abonnes.actualiser(this);
        }
    }

    public int getNumero() {
        return numero;
    }

    public String getTitre() {
        return titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

}
