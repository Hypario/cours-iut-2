package TD5.exo1;

public class AbonnePapier implements IObservateur {

    @Override
    public void actualiser(ISujet sujet) {
        if (sujet instanceof Journal) {
            int numero = ((Journal) sujet).getNumero();
            System.out.println(sujet.getClass().getSimpleName() + " numéro " + numero);
        }
    }

}
