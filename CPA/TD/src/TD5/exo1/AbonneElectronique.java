package TD5.exo1;

public class AbonneElectronique implements IObservateur {


    public void actualiser(ISujet sujet) {
        if (sujet instanceof Journal) {
            int numero = ((Journal) sujet).getNumero();
            String titre = ((Journal) sujet).getTitre();
            String contenu = ((Journal) sujet).getContenu();
            System.out.println("journal " + sujet.getClass().getSimpleName() + " numéro " + numero + " " + titre + " " + contenu);
        }
    }

}
