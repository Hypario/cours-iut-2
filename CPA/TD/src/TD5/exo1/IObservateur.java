package TD5.exo1;

public interface IObservateur {
    void actualiser(ISujet sujet);
}
