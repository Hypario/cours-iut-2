package TD5.exo2;


import java.beans.PropertyChangeListener;

public abstract class Observable {

    abstract void ajouterObservateur(PropertyChangeListener observateur);

    abstract void supprimerObservateur(PropertyChangeListener observateur);

    abstract void notifierObservateurs();

}
