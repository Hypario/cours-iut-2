package TD5.exo2;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class AbonnePapier implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println(evt.getPropertyName() + " : " + evt.getNewValue());
    }

}
