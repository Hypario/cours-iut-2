package TD5.exo2;

public class Test {

    public static void main(String[] args) {
        Quotidien journal = new Quotidien(1, "Virgile le fabuleux DJ", "Virgile est un magnifique DJ");

        AbonnePapier abonne = new AbonnePapier();

        journal.ajouterObservateur(abonne);

        journal.notifierObservateurs();

        journal.setNumero(2);
        journal.setTitre("DJ gigi à Paris !");
        journal.setContenu("DJ gigi est encore apparu hier soir à Paris à la place de David Ghetto, " +
                "celui-ci a fait fuir toute la foule, quand s'arrêtera-t-il ? " +
                "cela fait déjà ça 10e apparition ! mais que fait la police" +
                "Celui-ci était cette fois-ci accompagné de son amis Paul, qui joue très bien au football" +
                "Le nom de scène de DJ gigi est Virgile62150");

        journal.notifierObservateurs();
    }

}
