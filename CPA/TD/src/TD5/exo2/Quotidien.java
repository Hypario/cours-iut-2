package TD5.exo2;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Quotidien extends Observable {

    ArrayList<PropertyChangeListener> listeAbonnee = new ArrayList<>();

    int numero;
    String titre, contenu;

    Quotidien(int numero, String titre, String contenu) {
        this.numero = numero;
        this.titre = titre;
        this.contenu = contenu;
    }

    @Override
    void ajouterObservateur(PropertyChangeListener observateur) {
        listeAbonnee.add(observateur);
    }

    @Override
    void supprimerObservateur(PropertyChangeListener observateur) {
        listeAbonnee.remove(observateur);
    }

    @Override
    void notifierObservateurs() {
        for(PropertyChangeListener abo : listeAbonnee) {
            if (abo instanceof AbonnePapier) {
                abo.propertyChange(new PropertyChangeEvent(this, "Quotidien", null, toString()));
            }
        }
    }

    public int getNumero() {
        return numero;
    }

    public String getTitre() {
        return titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    @Override
    public String toString() {
        return "journal numéro " + getNumero() + ", " + getTitre() + ", " + getContenu();
    }
}
