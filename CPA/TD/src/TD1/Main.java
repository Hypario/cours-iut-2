package TD1;

public class Main {

    public static void main(String[] args) {
        Zoo zoo = new Zoo("Hypario");
        Tigre tigre = new Tigre();
        Zebre zebre = new Zebre();

        try {
            zoo.ajout(tigre);
            zoo.ajout(tigre);

            zoo.ajout(zebre);
            zoo.ajout(zebre);
        } catch (TableauComplet e) {
            System.out.println(e.getMessage());
        }
        zoo.affiche();
        System.out.println(zoo.count(zebre));
    }

}
