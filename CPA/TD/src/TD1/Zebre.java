package TD1;

public class Zebre extends Animal {
    @Override
    public EnumAlimentation alimentation() {
        return EnumAlimentation.HERBIVORE;
    }

    @Override
    public boolean vertebre() {
        return true;
    }

    @Override
    public String criAnimal() {
        return "hennissement";
    }
}
