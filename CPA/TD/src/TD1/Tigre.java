package TD1;

public class Tigre extends Animal {

    @Override
    public EnumAlimentation alimentation() {
        return EnumAlimentation.CARNIVORE;
    }

    @Override
    public boolean vertebre() {
        return true;
    }

    @Override
    public String criAnimal() {
        return "GRAAAAAAA";
    }
}
