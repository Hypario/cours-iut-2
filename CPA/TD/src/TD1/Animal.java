package TD1;

public abstract class Animal{

    public abstract EnumAlimentation alimentation();

    public abstract boolean vertebre();

    public abstract String criAnimal();

    public void afficher() {
        System.out.println("alimentation : " + alimentation() + " vertébré : " + vertebre() + " cri : " + criAnimal());
    }

}
