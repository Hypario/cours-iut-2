package TD1;

public class Zoo {

    public static final int MAX = 100;

    private Animal[] liste;
    private int index = 0;

    private String nom;

    public Zoo(String nom) {
        this.liste = new Animal[MAX];
        this.nom = nom;
    }

    public String getNom() {
        return this.nom;
    }

    public boolean ajout(Animal animal) throws TableauComplet {
        if (index < MAX) {
            liste[index++] = animal;
            return true;
        }
        throw new TableauComplet();
    }

    public void affiche() {
        for (Animal animal : liste) {
            if (animal != null) {
                animal.afficher();
            }
        }
    }

    public int count(Animal animal) {
        int count = 0;
        for(int i = 0; i < MAX; i++) {
            if (liste[i].getClass().getName().equals(animal.getClass().getName())) {
                count++;
            }
        }
        return count;
    }

}
