package TD2;

public class Multiplication extends OpBinaire {

    Multiplication(ExpressionArithmetique op1, ExpressionArithmetique op2)
    {
        super(op1, op2, '*');
    }

    @Override
    public double eval() {
        return this.op1.eval() * this.op2.eval();
    }
}
