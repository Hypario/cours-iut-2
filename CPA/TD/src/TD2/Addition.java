package TD2;

public class Addition extends OpBinaire {

    public Addition(ExpressionArithmetique op1, ExpressionArithmetique op2){
        super(op1, op2, '+');
    }

    @Override
    public double eval() {
        return this.op1.eval() + this.op2.eval();
    }

    @Override
    public String toString() {
        return this.op1.toString()  + " + " + this.op2.toString();
    }
}
