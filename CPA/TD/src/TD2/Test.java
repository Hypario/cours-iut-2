package TD2;

public class Test {

    public static void main(String[] args) {
        ExpressionArithmetique expression1 = new Multiplication(new Constante(3.0), new Constante(2.0));
        ExpressionArithmetique expression2 = new Addition(expression1, new Constante(5));

        System.out.println(expression2.toString());
        System.out.println(expression2.toString() + " = " + expression2.eval());

        ExpressionArithmetique expression3 = new Multiplication(new Constante(3.0), new Variable("x"));

        System.out.println(expression3.toString());
        System.out.println(expression3.toString() + " = " + expression3.eval());

        Addition add = new Addition(new Constante(3), new Multiplication(new Addition(new Constante(4.7), new Constante(2.3)), new Constante(5)));

        System.out.println(add.eval());
        System.out.println(add.prefixe());

    }

}
