package TD2;

public abstract class OpBinaire extends ExpressionArithmetique {

    protected ExpressionArithmetique op1, op2;
    private char operande;

    public OpBinaire(ExpressionArithmetique op1, ExpressionArithmetique op2, char operande) {
        this.op1 = op1;
        this.op2 = op2;
        this.operande = operande;
    }

    public String prefixe() {
        return this.operande +
                "(" +
                ((op1 instanceof OpBinaire) ? ((OpBinaire) this.op1).prefixe() : this.op1.toString()) +
                " " +
                ((op2 instanceof OpBinaire) ? ((OpBinaire) this.op2).prefixe() : this.op2.toString()) +
                ")";
    }

    public String toString() {
        return this.op1.toString() + " " + this.operande + " " + this.op2.toString();
    }
}
