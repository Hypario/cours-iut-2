package TD2;

public abstract class ExpressionArithmetique {

    abstract public double eval();

    abstract public String toString();

    public boolean equals(Object o){
        if (o instanceof ExpressionArithmetique)
            return this.eval() == ((ExpressionArithmetique) o).eval();
        return false;
    }

}
