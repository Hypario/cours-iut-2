package TD2;

public class Variable extends ExpressionArithmetique {

    private String v;

    public Variable(String v) {
        this.v = v;
    }

    @Override
    public double eval() {
        return 0;
    }

    @Override
    public String toString() {
        return this.v;
    }
}
