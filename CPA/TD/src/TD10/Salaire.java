package TD10;

import java.util.ArrayList;

public interface Salaire {

	public float calculSalaire();

	public void ajouteEmploye(Employe employe);

	public void supprimeEmploye(Employe employe);

	public ArrayList<Employe> getEmployes();

}
