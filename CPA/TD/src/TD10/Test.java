package TD10;

public class Test {

	public static void main(String[] args) {
		Employe boss = new Employe(20000);

		Employe dirP = new Employe(15000);

		Employe chefM = new Employe(10000);

		Employe secM = new Employe(2500);

		Employe chefExp = new Employe(7500);

		Employe vend1 = new Employe(2000);
		Employe vend2 = new Employe(2500);

		Employe dirV = new Employe(15000);

		Employe chefV = new Employe(10000);

		Employe emp = new Employe(2750);

		Employe chefP = new Employe(5000);

		Employe e01 = new Employe(2000);
		Employe e02 = new Employe(2500);
		Employe CM = new Employe(3000);

		Employe e03 = new Employe(2000);
		Employe e04 = new Employe(2000);

		boss.ajouteEmploye(dirP);
		boss.ajouteEmploye(dirV);

		dirP.ajouteEmploye(chefP);
		dirP.ajouteEmploye(chefExp);

		dirV.ajouteEmploye(chefM);
		dirV.ajouteEmploye(chefV);

		chefP.ajouteEmploye(e01);
		chefP.ajouteEmploye(e02);
		chefP.ajouteEmploye(CM);

		chefExp.ajouteEmploye(emp);

		chefV.ajouteEmploye(vend1);
		chefV.ajouteEmploye(vend2);

		chefM.ajouteEmploye(secM);

		CM.ajouteEmploye(e03);
		CM.ajouteEmploye(e04);


		System.out.println(boss.calculSalaire() + "€");
		System.out.println(dirV.calculSalaire() + "€");
	}

}
