package TD10;

import java.util.ArrayList;

public class Employe implements Salaire {

	private ArrayList<Employe> employes = new ArrayList<>();

	private float salaire;

	Employe(float salaire) {
		this.salaire = salaire;
	}

	@Override
	public float calculSalaire() {
		float somme = salaire;
		for (Employe employe:
				employes) {
			somme += employe.calculSalaire();
		}
		return somme;
	}

	@Override
	public void ajouteEmploye(Employe employe) {
		employes.add(employe);
	}

	@Override
	public void supprimeEmploye(Employe employe) {
		employes.remove(employe);
	}

	@Override
	public ArrayList<Employe> getEmployes() {
		return employes;
	}
}
