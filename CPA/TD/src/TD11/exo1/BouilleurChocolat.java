package TD11.exo1;

public class BouilleurChocolat {

	private static BouilleurChocolat instance = null;

	private boolean empty = true;

	private BouilleurChocolat() {
	}

	public static BouilleurChocolat getInstance() {
		if (instance == null) {
			instance = new BouilleurChocolat();
		}
		return instance;
	}

	public void remplir() {
		empty = false;
	}

	public void vider() {
		empty = true;
	}

	public boolean isEmpty() {
		return empty;
	}
}
