package TD11.exo1;

public class Test {

	public static void main(String[] args) {
		BouilleurChocolat bouilleur = BouilleurChocolat.getInstance();
		BouilleurChocolat bouilleur2 = BouilleurChocolat.getInstance();

		System.out.println(bouilleur.isEmpty());
		System.out.println(bouilleur2.isEmpty());

		bouilleur.remplir();

		System.out.println(bouilleur.isEmpty());
		System.out.println(bouilleur2.isEmpty());

		bouilleur.vider();

		System.out.println(bouilleur.isEmpty());
		System.out.println(bouilleur2.isEmpty());
	}
}


