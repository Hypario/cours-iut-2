package TD11.exo2;

public class Compte {

	private int numero;

	private float solde = 0;

	public void ajouterArgent(float ammount) {
		solde += ammount;
	}

	public void retirerArgent(float ammount) {
		solde -= ammount;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public float getSolde() {
		return solde;
	}
}
