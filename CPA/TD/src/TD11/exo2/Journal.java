package TD11.exo2;

import java.util.ArrayList;

public class Journal {

	private static Journal instance;

	private ArrayList<String> log;

	private Journal() {}

	public static Journal getInstance() {
		if (instance == null) {
			instance = new Journal();
		}
		return instance;
	}

	public void ajouterOperation(String operation) {
		log.add(operation);
	}

	public ArrayList getLog() {
		return log;
	}
}
