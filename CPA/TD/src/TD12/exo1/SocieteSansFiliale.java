package TD12.exo1;

public class SocieteSansFiliale extends Societe {
	@Override
	public double calculCout() {
		return nbVehic * coutEntretien;
	}

	@Override
	public boolean addFiliale(Societe societe) {
		return false;
	}

}
