package TD12.exo1;

import java.util.ArrayList;

public class SocieteMere extends Societe {
	private ArrayList<Societe> filiales = new ArrayList<>();

	@Override
	public double calculCout() {
		double sum = 0;
		for (Societe filiale :
				filiales) {
			sum += filiale.calculCout();
		}
		return sum + nbVehic * coutEntretien;
	}

	@Override
	public boolean addFiliale(Societe societe) {
		if (!societe.equals(this))
			return filiales.add(societe);
		return false;
	}
}
