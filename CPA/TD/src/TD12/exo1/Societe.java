package TD12.exo1;

public abstract class Societe {
	protected int nbVehic;
	protected static double coutEntretien = 55.99;

	public Societe() {
	}

	public void addVehic() {
		nbVehic++;
	}

	public abstract double calculCout();

	public abstract boolean addFiliale(Societe societe);


}
