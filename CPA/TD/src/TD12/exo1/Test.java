package TD12.exo1;

public class Test {

	public static void main(String[] args) {
		SocieteSansFiliale s1 = new SocieteSansFiliale();
		SocieteSansFiliale s2 = new SocieteSansFiliale();

		for (int i = 0; i < 10; i++) {
			s1.addVehic();
			s2.addVehic();
		}

		SocieteMere m = new SocieteMere();
		m.addVehic();

		m.addFiliale(s1);
		m.addFiliale(s2);

		System.out.println("Coût : " + m.calculCout());
	}

}
