package TD12.exo2;

public class SocieteSansFiliale extends Societe {
	@Override
	public double calculCout() {
		return nbVehic * coutEntretien;
	}

	@Override
	public boolean addFiliale(Societe societe) {
		return false;
	}

	@Override
	public void accepteVisiteur(VisiteurInterface v) {
		v.visite(this);
	}

}
