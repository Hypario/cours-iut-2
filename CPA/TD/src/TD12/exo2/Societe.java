package TD12.exo2;

public abstract class Societe {
	protected int nbVehic;
	protected static double coutEntretien = 55.99;

	protected String name, mail;

	public Societe() {
	}

	public void addVehic() {
		nbVehic++;
	}

	public abstract double calculCout();

	public abstract boolean addFiliale(Societe societe);

	public void sendMail() {
		System.out.println("Mail envoyé à " + name + " à l'adresse : " + mail);
	}

	public abstract void accepteVisiteur(VisiteurInterface v);

	public void setName(String name) {
		this.name = name;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}
