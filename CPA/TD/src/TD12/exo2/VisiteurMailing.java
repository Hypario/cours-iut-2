package TD12.exo2;

import java.util.ArrayList;

public class VisiteurMailing implements VisiteurInterface {
	@Override
	public void visite(SocieteMere s) {
		ArrayList<Societe> filiales = s.getFiliales();

		for (Societe filiale:
			 filiales) {
			filiale.sendMail();
		}
	}

	@Override
	public void visite(SocieteSansFiliale s) {
		s.sendMail();
	}
}
