package TD12.exo2;

public interface VisiteurInterface {

	public void visite(SocieteMere s);
	
	public void visite(SocieteSansFiliale s);

}
