package TD12.exo2;

import java.util.ArrayList;

public class SocieteMere extends Societe {
	private ArrayList<Societe> filiales = new ArrayList<>();

	@Override
	public double calculCout() {
		double sum = 0;
		for (Societe filiale :
				filiales) {
			sum += filiale.calculCout();
		}
		return sum + nbVehic * coutEntretien;
	}

	@Override
	public boolean addFiliale(Societe societe) {
		if (!societe.equals(this))
			return filiales.add(societe);
		return false;
	}

	@Override
	public void accepteVisiteur(VisiteurInterface v) {
		v.visite(this);
		for (Societe filiale :
				filiales) {
			if (filiale instanceof SocieteSansFiliale) {
				v.visite((SocieteSansFiliale) filiale);
			} else if (filiale instanceof SocieteMere) {
				v.visite((SocieteMere) filiale);
			}
		}
	}

	public ArrayList<Societe> getFiliales() {
		return filiales;
	}
}
