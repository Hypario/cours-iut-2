package TD4;

public class ComposantPDF {

    String content;

    public void pdfFixeContenu(String Contenu) {
        this.content = Contenu;
    }

    public void pdfPrepareAffichage() {
        System.out.println("--- Préparation affichage ---");
    }

    public void pdfAffiche() {
        System.out.println(content);
    }

    public void pdfTermineAffichage() {
        System.out.println("--- Termine affichage ---");
    }

    public void pdfEnvoieImprimante() {
        System.out.println("PDF envoyé à l'imprimante");
    }
}
