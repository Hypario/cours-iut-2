package TD4;

public class DocumentHTML implements IDocument {

    String content;

    @Override
    public void setContenu(String contenu) {
        this.content = contenu;
    }

    @Override
    public void affiche() {
        System.out.println(content);
    }

    @Override
    public void imprime() {
        System.out.println("Document HTML envoyé à l'imprimante");
    }
}
