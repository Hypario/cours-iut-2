package TD4;

public class ServeurWeb {

    public static void main(String[] args) {
        PDFAdapter adapter = new PDFAdapter();
        DocumentHTML document = new DocumentHTML();

        document.setContenu("Salut le doc HTML");
        document.affiche();
        document.imprime();

        adapter.setContenu("Salut les gens");
        adapter.affiche();
        adapter.imprime();
    }

}
