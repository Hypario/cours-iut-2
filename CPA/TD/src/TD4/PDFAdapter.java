package TD4;

public class PDFAdapter implements IDocument {

    ComposantPDF pdf;

    PDFAdapter() {
        this.pdf = new ComposantPDF();
    }

    @Override
    public void setContenu(String contenu) {
        pdf.pdfFixeContenu(contenu);
    }

    @Override
    public void affiche() {
        pdf.pdfPrepareAffichage();
        pdf.pdfAffiche();
        pdf.pdfTermineAffichage();
    }

    @Override
    public void imprime() {
        pdf.pdfEnvoieImprimante();
    }
}
