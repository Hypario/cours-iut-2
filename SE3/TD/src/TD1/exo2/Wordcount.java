package TD1.exo2;

import java.io.File;
import java.util.Scanner;

public class Wordcount {

    public static void wc(String filename, String option) {
        try {
            Scanner in = new Scanner(new File(filename));

            // int word, line, character counter
            int wc = 0, lc = 0, cc = 0;

            in.useDelimiter("\\n");
            String line;
            String[] words;
            while (in.hasNext()) {
                line = in.nextLine();
                words = line.split("\\s+");
                cc += line.length() + 1;
                wc += words.length;
                lc += 1;
            }
            if (option.matches("-.*w.*"))
                System.out.println("Nombre de mot : " + wc);
            if (option.matches("-.*l.*"))
                System.out.println("Nombre de ligne : " + lc);
            if (option.matches("-.*c.*"))
                System.out.println("Nombre de charactère : " + cc);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Syntaxe error, please use this syntax : wc --options filename");
            System.exit(1);
        }
        wc(args[1], args[0]);
    }

}
