package TD1.exo3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Scanner;

public class CSVConverter {

    private String filename;

    private char columnDelimiter = ';';

    private char stringDelimiter = '"';

    CSVConverter(String filename) {
        this.filename = filename;
    }

    public String[] découperLigne(String ligne) {
        return ligne.split(String.valueOf(this.columnDelimiter));
    }

    private ArrayList<String[]> lire(String filename) {
        ArrayList<String[]> result = new ArrayList<>();
        try {
            Scanner in = new Scanner(new File(filename));

            while (in.hasNext()) {
                result.add(découperLigne(in.nextLine()));
            }

            in.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        return result;
    }

    public void afficher() {
        ArrayList<String[]> csv = lire(this.filename);
        for (String[] ligne : csv) {
            System.out.println(String.join(" ", ligne));
        }
    }

    public void ecrire(String content) throws IOException {
        content = echapper(content);
        boolean flag = Files.exists(Paths.get(filename));
        Files.write(
                Paths.get(filename),
                (content + '\n').getBytes(),
                StandardOpenOption.WRITE,
                flag ? StandardOpenOption.APPEND : StandardOpenOption.CREATE
        );
    }

    public void changeColumnDelimiter() {
        columnDelimiter = (columnDelimiter == ',') ? ',' : ';';
    }

    public char oppositeColumnDelimiter() {
        return (columnDelimiter == ',') ? ';' : ',';
    }

    public void changeStringDelimiter() {
        stringDelimiter = (stringDelimiter == '"') ? '\'' : '"';
    }

    public char oppositeStringDelimiter() {
        return (stringDelimiter == '"') ? '"' : '\'';
    }

    public static void main(String[] args) {
        CSVConverter converter = new CSVConverter("table.csv");
        try {
            converter.ecrire("toto");
            converter.ecrire("toto;titi;tata");
            converter.ecrire("50, rue Xavier");
            converter.ecrire("il a dit \"bonjour\", et");
        } catch (IOException e) {
            System.out.println("Impossible d'écrire dans table.csv");
        }
        converter.afficher();
    }

    /**
     * Échappe les caractères tel que le délimiteur et les doubles guillemets
     *
     * @param s the string you want to escape
     * @return String
     */
    private String echapper(String s) {
        boolean flag = false;
        StringBuilder copy = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == this.oppositeColumnDelimiter()) {
                flag = true;
                copy.append(s.charAt(i));
            } else if (s.charAt(i) == this.oppositeStringDelimiter()) {
                flag = true;
                copy.append("\"\"");
            } else
                copy.append(s.charAt(i));
        }
        if (flag) {
            return '"' + copy.toString() + '"';
        }
        return copy.toString();
        /* works only if delimiters are string
        if (s.contains("\"")) {
            s = s.replace("\"", "\"\"");
            s = "\"" + s + "\"";
        } else if (s.contains(this.columnDelimiter)) {
            s = "\"" + s + "\"";
        }
        return s;
         */
    }

}
