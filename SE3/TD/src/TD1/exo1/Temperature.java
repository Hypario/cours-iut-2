package TD1.exo1;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;

class Temperature {

    static void ecrire(String filename) {
        try {
            PrintWriter out = new PrintWriter(filename);

            for (int y = 2016; y <= 2019; y++) {
                for (int i = 1; i <= 12; i++) {
                    for (int j = 1; j < 31; j++) {
                        out.println(j + "/" + i + "/" + y + " " + (10 + Math.random() * (40 - 10)));
                    }
                }
            }

            out.close();
        } catch (Exception e) {
            e.getStackTrace();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        moyenne("temp.txt");
    }

    static void lire(String filename) {
        try {
            Scanner in = new Scanner(new File(filename));
            // Le séparateur est un slash ou une suite d'espace
            in.useDelimiter("/|\\s+");
            in.useLocale(Locale.US);
            int d, m, y;
            float temp;
            while (in.hasNext()) {
                d = in.nextInt();
                m = in.nextInt();
                y = in.nextInt();
                temp = in.nextFloat();
                System.out.println(d + "/" + m + "/" + y + " " + temp);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    static void moyenne(String filename) {
        try {
            Scanner in = new Scanner(new File(filename));
            in.useDelimiter("/|\\s+");
            in.useLocale(Locale.US);

            int d, m, y, currentMonth = -1, currentYear = -1;
            float temp, sum = 0;
            while (in.hasNext()) {
                d = in.nextInt();
                m = in.nextInt();
                y = in.nextInt();
                temp = in.nextFloat();
                if (m == currentMonth && y == currentYear) {
                    sum += temp;
                } else {
                    if (currentMonth != -1 && currentYear != -1)
                        System.out.println("température moyenne pour mois " + currentMonth + "/" + currentYear + " " + (sum / 31));
                    currentMonth = m;
                    sum = temp;
                    currentYear = y;
                }
            }
            if (currentMonth != -1 && currentYear != -1) {
                System.out.println("température moyenne pour mois " + currentMonth + "/" + currentYear + " " + (sum / 31));
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

}
