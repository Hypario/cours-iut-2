package TD1.exo4;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Replace {

	private void replace(String keywords, String source, String destination) {
		try (
				Scanner keysFile = new Scanner(new File(keywords));
				Scanner sourceFile = new Scanner(new File(source));
				PrintWriter out = new PrintWriter(destination)
		) {

			// on initialise nos variable
			ArrayList<String[]> listKeyWords = new ArrayList<>();
			StringBuilder content = new StringBuilder();

			// on set les délimiteurs
			keysFile.useDelimiter("[\t\n]");
			sourceFile.useDelimiter("\n");

			// on remplis notre tableau de mot clef
			while (keysFile.hasNextLine()) {
				String[] pair = keysFile.nextLine().split("\t");
				listKeyWords.add(pair);
			}

			// on récupère le contenu source
			while (sourceFile.hasNextLine()) {
				content.append(sourceFile.nextLine()).append("\n");
			}

			for (String[] keys :
					listKeyWords) {
				content = new StringBuilder(content.toString().replaceAll("\\b" + keys[0], keys[1]));
			}

			out.print(content.toString());

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Syntaxe error, please use this syntax : replace keywords source destination");
			System.exit(1);
		}

		Replace wr = new Replace();
		wr.replace(args[0], args[1], args[2]);
	}


}
