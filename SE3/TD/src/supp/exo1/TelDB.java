package supp.exo1;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;

import static java.nio.charset.StandardCharsets.*;

public class TelDB {

    class Telephone {
        public int id;
        public String numero;

        Telephone(int id, String numero) {
            this.id = id;
            this.numero = numero;
        }

        static final int BYTES = Integer.BYTES + 10;
    }

    FileChannel db;
    ByteBuffer buffer;

    TelDB() {
        try {
            File directory = new File("./bdd");
            if (!directory.isDirectory()) {
                directory.mkdir();
            }

            db = FileChannel.open(
                    FileSystems.getDefault().getPath("./bdd/telephone.db"),
                    StandardOpenOption.READ,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE
            );

            db.truncate(0);

            buffer = ByteBuffer.allocate(Telephone.BYTES);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public boolean insert(int id, String mobile) {
        if (mobile.length() <= 10) {
            try {
                // on garantis l'unicité de l'id
                db.position(0);
                Telephone tel;
                while ((tel = read()) != null) {
                    if (tel.id == id)
                        return false;
                }

                // on insère
                buffer.clear();
                buffer.putInt(id);
                for (Byte b : mobile.getBytes(US_ASCII)) {
                    buffer.put(b);
                }

                buffer.flip();
                while (buffer.hasRemaining())
                    db.write(buffer);

                return true;
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public String select1(int min, int max) {
        try {
            db.position(0);
            Telephone tel;
            StringBuilder result = new StringBuilder();
            while ((tel = read()) != null) {
                if (tel.id >= min && tel.id <= max) {
                    result.append(tel.id).append(" ").append(tel.numero).append("\n");
                }
            }
            return result.toString();
        } catch (IOException e) {
            return null;
        }
    }

    private Telephone read() throws IOException {
        buffer.clear();
        while (buffer.hasRemaining()) {
            if (db.read(buffer) == -1) {
                return null;
            }
        }

        buffer.flip();
        int id = buffer.getInt();
        StringBuilder num = new StringBuilder();
        while (buffer.hasRemaining()) {
            num.append((char) buffer.get());
        }

        return new Telephone(id, num.toString());
    }

}
