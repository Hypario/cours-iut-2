package TD3.exo2;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;

public class LSFilewalk {

	public static void main(String[] args) {
		LSFilewalk lsFile = new LSFilewalk();
		lsFile.list(".");
	}

	public void list(String parent) {
		Path path = FileSystems.getDefault().getPath(parent);
		try {
			HashMap<String, ArrayList<String>> files = new HashMap<>();

			Files.walkFileTree(path, new SimpleFileVisitor<>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					ArrayList<String> liste = files.computeIfAbsent(file.getParent().toString(), k -> new ArrayList<>());

					liste.add(file.getFileName().toString());
					return super.visitFile(file, attrs);
				}
			});

			files.forEach((k, v) -> System.out.println(k + "\n" + v));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
