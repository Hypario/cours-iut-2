package TD3.exo2;

import java.io.File;
import java.util.Arrays;

public class LSFile {

	public static void main(String[] args) {
		LSFile lsFile = new LSFile();
		lsFile.list(".");
	}

	public void list(String parent) {
		// on créer un objet file de notre fichier / dossier courrant
		File f = new File(parent);
		// on fais la liste des fichiers
		String[] files = f.list();

		// si on a des fichiers
		if (files != null && files.length > 0) {
			// pour chaque fichier
			for (String file : files) {
				// on créer un objet fichier des fichiers enfants
				f = new File(parent + "/" + file);
				if (f.isDirectory()) {
					// si c'est un directory on liste
					list(parent + "/" + file);
				}
			}
			// on affiche
			System.out.println(parent);
			System.out.println(Arrays.toString(files));
		}
	}

}
