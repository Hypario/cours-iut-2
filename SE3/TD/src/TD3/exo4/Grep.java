package TD3.exo4;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Scanner;

public class Grep {

	public static void main(String[] args) {
		Grep grep = new Grep();

		grep.find("petit poney", ".");
	}

	public void find(String needle, String directory) {
		Path path = FileSystems.getDefault().getPath(directory);
		ArrayList<String> files = new ArrayList<>();
		try {
			Files.walkFileTree(path, new SimpleFileVisitor<>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

					// on lis le fichier
					Scanner in = new Scanner(file);

					// on regarde s'il contient notre chaîne
					while (in.hasNextLine()) {
						String line = in.nextLine();

						if (line.contains(needle)) {
							files.add(file.toString());
						}
					}

					// on le ferme
					in.close();

					return super.visitFile(file, attrs);
				}
			});
			System.out.println(files);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
