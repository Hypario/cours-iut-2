package TD3.exo1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LSCommand {

	public static void main(String[] args) {
		LSCommand command = new LSCommand();

		try {
			command.execute(".");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void execute(String rep) throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yy hh:mm");
		File f = new File(rep);
		String[] liste = f.list();
		if (liste != null) {
			for (String file : liste) {
				Path path = Paths.get(rep, file);
				// récupère les attributs
				BasicFileAttributes basic = Files.getFileAttributeView(path, BasicFileAttributeView.class).readAttributes();
				// récupère les droits
				PosixFileAttributes posix = Files.getFileAttributeView(path, PosixFileAttributeView.class).readAttributes();

				char type = '?';
				if (basic.isRegularFile()) type = '-';
				else if (basic.isDirectory()) type = 'd';
				else if (basic.isSymbolicLink()) type = 'l';

				// on format l'output
				System.out.format("%c%s %8s %8s %8d %15s %s\n",
						type, // affiche le type
						PosixFilePermissions.toString(posix.permissions()), // récupère les permissions
						posix.owner().getName(), // récupère propiétaire
						posix.group().getName(), // récupère le groupe
						Files.size(path), // récupère la taille
						df.format(basic.lastModifiedTime().toMillis()), // récupère la date de dernière modification
						file // récupère le nom du fichier
				);
			}
		}
	}
}
