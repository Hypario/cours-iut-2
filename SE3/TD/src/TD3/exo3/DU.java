package TD3.exo3;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Objects;

public class DU {

	public static void main(String[] args) {
		DU du = new DU();
		du.list(".");
	}

	public void list(String parent) {
		Path path = FileSystems.getDefault().getPath(parent);

		try {
			Files.walkFileTree(path, new SimpleFileVisitor<>() {

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					System.out.println(getFolderSize(dir.toFile()) + " " + dir);
					return super.postVisitDirectory(dir, exc);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static long getFolderSize(File dir) {
		long size = 0;
		for (File file : Objects.requireNonNull(dir.listFiles())) {
			if (file.isFile()) {
				size += file.length();
			}
			else
				size += getFolderSize(file);
		}
		return size;
	}

}
