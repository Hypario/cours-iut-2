package TD2.exo1and2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;

public class FichierBinaire {

    // représente un produit, le static car indépendant de notre classe, la meilleur solution est le mettre dans un autre fichier
    static class Produit {
        int ref, quantite;
        float prix;

        static final int BYTES = 2 * Integer.BYTES + Float.BYTES;

        Produit() {
        }

        Produit(int ref, int quantite, float prix) {
            this.ref = ref;
            this.quantite = quantite;
            this.prix = prix;
        }
    }

    FileChannel f; // le fichier binaire
    ByteBuffer buffer; // Le tampon pour écrire dans le fichier

    FichierBinaire(String filename) throws IOException {
        // Ouverture en lecture / écriture, avec création de fichier
        f = FileChannel.open(
                FileSystems.getDefault().getPath(filename),
                StandardOpenOption.READ,
                StandardOpenOption.WRITE,
                StandardOpenOption.CREATE
        );
        // création d'un buffer juste assez grand pour contenir un produit
        buffer = ByteBuffer.allocate(Produit.BYTES);
    }

    void ecrireProduit(Produit prod) throws IOException {

        // copier le produit dans le tampon
        buffer.clear();
        buffer.putInt(prod.ref);
        buffer.putFloat(prod.prix);
        buffer.putInt(prod.quantite);

        // copier le tampon dans le fichier
        buffer.flip();
        while (buffer.hasRemaining()) {
            f.write(buffer);
        }
    }

    Produit lireProduit() throws IOException {
        // copie du fichier dans le tampon
        buffer.clear();
        while (buffer.hasRemaining()) {
            if (f.read(buffer) == -1) {
                return null;
            }
        }
        buffer.flip();
        Produit prod = new Produit();

        // il faut relire  les données dans le même ordre que lors de l'écriture
        prod.ref = buffer.getInt();
        prod.prix = buffer.getFloat();
        prod.quantite = buffer.getInt();
        return prod;
    }

    void ecrire() throws IOException {
        Produit prod = new Produit();
        for (int id = 1; id <= 5; id++) {
            prod.ref = id;
            prod.prix = id * 10;
            prod.quantite = 1;
            ecrireProduit(prod);
        }
    }

    /**
     * relecture du fichier
     */
    void lire() throws IOException {
        Produit prod;
        f.position(0); // revenir au début du fichier
        System.out.println("référence" + "\t" + "prix" + "\t" + "quantité");
        while ((prod = lireProduit()) != null)
            System.out.println(prod.ref + "\t" + prod.prix + "\t" + prod.quantite);
    }

    /**
     * relecture du fichier à l’envers
     */
    void lireALEnvers() throws IOException {
        Produit prod;
        long pos = f.size() - Produit.BYTES; // position du dernier produit
        System.out.println("référence" + "\t" + "prix" + "\t" + "quantité");
        while (pos >= 0) {
            f.position(pos);
            prod = lireProduit();
            System.out.println(prod.ref + "\t" + prod.prix + "\t" + prod.quantite);
            pos -= Produit.BYTES;
        }
    }

    // récupère les informations d'un produit à partir d'un identifiant
    void getProduit(int id) throws IOException {
        Produit prod;
        f.position(0); // revenir au début du fichier
        System.out.println("référence" + "\t" + "prix" + "\t" + "quantité");
        while ((prod = lireProduit()) != null) {
            if (prod.ref == id) {
                System.out.println(prod.ref + "\t" + prod.prix + "\t" + prod.quantite);
                return;
            }
        }
        System.out.println("ce produit n'existe pas");
    }

    void ajouterProduit(Produit newProd) throws IOException {
        search(newProd.ref);
        ajouterProduit(newProd);
    }

    void ajouterProduitTrie(Produit newProd) throws IOException {
        // nous positionne au bon endroit
        // search(newProd.ref);
        searchTrie(newProd.ref);

        // ArrayList<Produit> tmp = new ArrayList<>();

        // si on écrase, on met en mémoire

		/*
		if (lireProduit() != null) {
			f.position(f.position() - Produit.BYTES);
			tmp.add(lireProduit());
			f.position(f.position() - Produit.BYTES);
		}
		 */

		/*
		// on remplace par le nouveau
		ecrireProduit(newProd);
		 */

        // tant qu'on a en mémoire
		/*
		while(!tmp.isEmpty()) {

			// si on écrase, on rajoute en mémoire
			if (lireProduit() != null) {
				f.position(f.position() - Produit.BYTES);
				tmp.add(lireProduit());
				f.position(f.position() - Produit.BYTES);
			}
			// on écrit le dernier écrasé
			ecrireProduit(tmp.get(0));

			// on le supprime de la mémoire
			tmp.remove(0);
		}
		 */

        Produit tmp1, tmp2 = null;

        if ((tmp1 = lireProduit()) != null) {
            f.position(f.position() - Produit.BYTES);
        }

        // on remplace par le nouveau

        ecrireProduit(newProd);

        // tant qu'on a quelque chose en mémoire on décale
        // pas nécessaire quand on overwrite par même id
        while (tmp1 != null && tmp1.ref != newProd.ref) {
            if ((tmp2 = lireProduit()) != null) {
                f.position(f.position() - Produit.BYTES);
            }
            ecrireProduit(tmp1);
            tmp1 = tmp2;
        }
    }

    void supprimerProduit(int id) throws IOException {
        f.position(dernierProduit()); // se positionne avant le dernier produit
        Produit last = lireProduit();

        if (searchTrie(id)) {
            ecrireProduit(last); // on remplace le produit à supprimer par le dernier
        }
        f.truncate(f.size() - Produit.BYTES); // on supprime le dernier produit
    }

    void supprimeProduitTrie(int id) throws IOException {
        if (search(id)) {
            Produit next;
            f.position(f.position() + Produit.BYTES); // skip current one we want to erase
            while ((next = lireProduit()) != null) {
                f.position(f.position() - 2 * Produit.BYTES); // go back to current
                ecrireProduit(next); // replace it
                f.position(f.position() + Produit.BYTES); // go to next one
            }
            f.truncate(f.size() - Produit.BYTES); // delete last one (because last one is duplicated
        }
    }

    // fais varier la quantité pour un produit id
    boolean varierQuantite(int id, int quantite) throws IOException {
        Produit prod;
        f.position(0); // revenir au début du fichier
        while ((prod = lireProduit()) != null) {
            if (prod.ref == id) {
                f.position(f.position() - Produit.BYTES);
                prod.quantite += quantite;
                ecrireProduit(prod);
                return true;
            }
        }
        return false;
    }

    void clear() throws IOException {
        f.truncate(0);
    }

    void run() throws IOException {
        ecrire();
        lire();
        lireALEnvers();
        f.close();
    }

    void close() throws IOException {
        f.close();
    }

    public static void main(String[] args) {
        try {
            FichierBinaire bin = new FichierBinaire("/tmp/catalogue.bin");
            bin.clear();
            bin.ajouterProduitTrie(new Produit(10, 1, 100));
            bin.ajouterProduitTrie(new Produit(10, 1, 200));
            bin.ajouterProduitTrie(new Produit(8, 1, 100));
            bin.ajouterProduitTrie(new Produit(6, 1, 100));
            bin.ajouterProduitTrie(new Produit(5, 1, 100));
            bin.ajouterProduitTrie(new Produit(5, 5, 200));
            bin.ajouterProduitTrie(new Produit(7, 1, 100));
            bin.ajouterProduitTrie(new Produit(9, 120, 1000));
            bin.lire();
            bin.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Donne la position avant le dernier produit
     *
     * @return dernier produit
     * @throws IOException une exception
     */
    private long dernierProduit() throws IOException {
        return f.size() - Produit.BYTES;
    }

    /**
     * Cherche un produit via son id, sauf qu'on positionne le curseur au produit
     * si return false, le curseur est à la fin du fichier
     *
     * @param id l'identifiant
     * @return boolean
     * @throws IOException une exception
     */
    private boolean search(int id) throws IOException {
        Produit prod;
        f.position(0);
        while ((prod = lireProduit()) != null && prod.ref <= id) {
            if (prod.ref == id) {
                f.position(f.position() - Produit.BYTES);
                return true;
            }
        }
        if (f.size() > 0)
            f.position(f.position() - Produit.BYTES);
        return false;
    }

    private boolean searchTrie(int id) throws IOException {
        // on regarde de 0 à la taille (f.size) non inclus
        long begin, mid, end;
        begin = 0;
        end = f.size() / Produit.BYTES;
        Produit prod;
        // tant que le départ est différent de la fin
        while (begin != end) {
            // on cherche le milieux
            mid = (begin + end) / 2;

            // on s'y positionne et lit le produit
            f.position(mid * Produit.BYTES);
            prod = lireProduit();

            if (prod.ref == id) {
                f.position(f.position() - Produit.BYTES);
                return true;
            }

            // if l'id est supérieur, le début c'est le milieu + 1 sinon la fin est le milieu
            if (id > prod.ref) {
                begin = mid + 1;
            } else {
                end = mid;
            }

        }
        f.position(begin * Produit.BYTES); // ici que ce soit begin ou end, c'est pareil
        return false;
    }
}
