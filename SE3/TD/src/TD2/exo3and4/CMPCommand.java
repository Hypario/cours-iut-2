package TD2.exo3and4;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;

public class CMPCommand extends Command {

	@Override
	public void execute(String... args) {
		try (FileChannel a = FileChannel.open(
				FileSystems.getDefault().getPath(args[0]),
				StandardOpenOption.READ
		); FileChannel b = FileChannel.open(
				FileSystems.getDefault().getPath(args[1]),
				StandardOpenOption.READ
		)) {
			ByteBuffer bufferA = ByteBuffer.allocate(2048);
			ByteBuffer bufferB = ByteBuffer.allocate(2048);

			// on regarde la taille en premier
			if (a.size() != b.size()) {
				System.out.println("Les tailles sont différentes.");
				System.exit(1);
			}

			// on regarde le contenu (pas besoin si ils ont pas la même taille)
			while (a.position() != a.size() || b.position() != b.size()) {
				// on lis le fichier a
				bufferA.clear();
				bufferB.clear();
				while (bufferA.hasRemaining() && bufferB.hasRemaining()) {
					if (a.read(bufferA) == -1 || b.read(bufferB) == -1)
						break;
				}

				// on flip les buffer
				bufferA.flip();
				bufferB.flip();

				// on compare les buffers
				if (!bufferA.equals(bufferB)) {
					System.out.println("Les fichiers sont différents");
					System.exit(1);
				}
			}

			System.out.println("Les fichiers sont identiques");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
