package TD2.exo3and4;

import java.io.IOException;

public abstract class Command {

    public abstract void execute(String ...args) throws IOException;

}
