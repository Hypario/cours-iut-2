package TD2.exo3and4;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;

public class CatCommand extends Command {

	@Override
	public void execute(String... args) throws IOException {
		FileChannel in = FileChannel.open(
				FileSystems.getDefault().getPath(args[0]),
				StandardOpenOption.READ
		);

		// byte de 8ko
		ByteBuffer buffer = ByteBuffer.allocate(8192);

		while (in.position() != in.size()) {

			buffer.clear();
			while (buffer.hasRemaining()) {
				if (in.read(buffer) == -1)
					break;
			}

			System.out.write(buffer.array(), 0, buffer.position());
		}
		in.close();
	}

}
