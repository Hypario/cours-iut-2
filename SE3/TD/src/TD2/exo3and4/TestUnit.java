package TD2.exo3and4;

import java.io.IOException;

public class TestUnit {

	public static void main(String[] args) {
		if (args.length == 2 || args.length == 3) {
			if (args[0].equals("cat")) {
				TestUnit test = new TestUnit();
				test.cat(args[1]);
			} else if (args[0].equals("cmp")) {
				TestUnit test = new TestUnit();
				test.cmp(args[1], args[2]);
			} else {
				System.err.println("Please use one of those : cat, cmp");
			}
		} else {
			System.err.println("Usage : TestUnit cat <filename> OR" + "\n" + "TestUnit cmp <filename> <filename>");
		}
	}

	public void cat(String filename) {
		Command command = new CatCommand();
		try {
			command.execute(filename);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void cmp(String a, String b) {
		Command command = new CMPCommand();
		try {
			command.execute(a, b);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
