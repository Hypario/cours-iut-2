package TD4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class exo3 implements Runnable {

	// nombre de thread et nombre d'entier aléatoire
	private static final int numThread = 4, n = 11;

	// index du thread
	private int index = 0;

	// liste d'entier
	private ArrayList<Integer> liste;

	// liste des max récupéré par thread
	private ArrayList<Integer> maxes = new ArrayList<>();

	public static void main(String[] args) {

		exo3 exo = new exo3();

		// list of random integers
		exo.liste = exo.generateRandomArray(n);

		ArrayList<Thread> listeThread = new ArrayList<>();

		long start = System.currentTimeMillis();

		for (int i = 0; i < numThread; i++) {
			Thread th = new Thread(exo);
			th.start();
			listeThread.add(th);
		}

		for (Thread th : listeThread) {
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		System.out.println(exo.liste);
		System.out.println(exo.max(0, exo.maxes.size(), exo.maxes));

		long end = System.currentTimeMillis();

		System.out.println("Le programme à mis " + (end - start) + " millisecondes");
	}

	public ArrayList<Integer> generateRandomArray(int n) {
		ArrayList<Integer> list = new ArrayList<>();
		Random random = new Random();

		for (int i = 0; i < n; i++) {
			list.add(random.nextInt(50));
		}
		return list;
	}

	public int max(int start, int end, List<Integer> liste) {
		int max = liste.get(start);
		for (int i = start; i < end; i++) {
			if (liste.get(i) > max) {
				max = liste.get(i);
			}
		}
		return max;
	}


	@Override
	public void run() {
		index++;
		// on calcul l'index de début
		double offset = (index - 1) * ((double) liste.size() / (double) numThread);
		// on calcul l'index de fin
		double end = index * ((double) liste.size() / (double) numThread);

		maxes.add(max((int) offset, (int) end, liste));
	}
}
