package TD4;

import java.util.ArrayList;
import java.util.Arrays;

public class exo4 implements Runnable {

	// nb de lignes par thread, ce qui reste à faire (si taille non multiple de nbThreads)
	// et l'index du thread
	public int nbLignes, reste, index;

	// initialise mes deux vecteurs de même taille
	public static final int SIZE = 3;
	public int[] vecteur1 = new int[SIZE], vecteur2 = new int[SIZE], result = new int[SIZE];

	public static void main(String[] args) {

		// can be modified
		final int nbThreads = 1;

		ArrayList<Thread> threads = new ArrayList<>();
		exo4 exo4 = new exo4();

		exo4.vecteur1[0] = 3;
		exo4.vecteur1[1] = 2;
		exo4.vecteur1[2] = 8;

		exo4.vecteur2[0] = 5;
		exo4.vecteur2[1] = 4;
		exo4.vecteur2[2] = 9;

		if (exo4.vecteur1.length == exo4.vecteur2.length && exo4.vecteur1.length >= nbThreads) {
			exo4.nbLignes = exo4.vecteur1.length / nbThreads;
			exo4.reste = exo4.vecteur1.length % nbThreads;

			for (int i = 0; i < nbThreads; i++) {
				Thread th = new Thread(exo4);
				th.start();
				threads.add(th);
			}

			for (Thread th : threads) {
				try {
					th.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}

			System.out.println(Arrays.toString(exo4.vecteur1));
			System.out.println(Arrays.toString(exo4.vecteur2));
			System.out.println(Arrays.toString(exo4.result));
		} else {
			if (exo4.vecteur1.length != exo4.vecteur2.length)
				System.err.println("Le vecteur 1 et 2 ne sont pas de même taille !");
			else
				System.err.println("Trop de thread !");
		}
	}

	@Override
	public void run() {
		index++;
		// on veut pas ajouter le reste au début du tableau
		int offset = (index - 1) * nbLignes + (index - 1 == 0 ? 0 : reste);
		int end = index * nbLignes + reste;

		add(offset, end);
	}

	public void add(int offset, int end) {
		for (int i = offset; i < end; i++) {
			result[i] = vecteur1[i] + vecteur2[i];
		}
	}
}
