package TD4;

public class exo1 implements Runnable{

	public static int x = 0;

	public static void main(String[] args) {
		Thread thread1 = new Thread(new exo1());
		Thread thread2 = new Thread(new exo1());

		thread1.start();
		thread2.start();

		try {
			thread1.join(); // wait for thread to die
			thread2.join(); // wait for thread to die
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println(x);
	}

	@Override
	public void run() {
		for (int i = 0; i < 50_000; i++) {
			x += 1;
		}
	}
}
