#!/bin/env python3

import random
import matplotlib.pyplot as plt


# exo 1


def simulation():
    number = random.random()
    if number <= 0.3:
        return 1
    elif 0.3 < number <= 0.9:
        return 2
    elif number > 0.9:
        return 3


def show():
    x = [1, 2, 3]
    y = [0, 0, 0]
    count = [0, 0, 0]  # index 0 = 1, 1 = 2, 2 = 3

    # count numbers
    for i in range(10000):
        number = simulation()
        if number == 1:
            count[0] += 1
        if number == 2:
            count[1] += 1
        if number == 3:
            count[2] += 1

    # calculate frequencies
    for i in range(len(y)):
        y[i] = count[i] / 10000

    # show the result
    plt.bar(x, y, align="center")
    plt.show()


# exo 2


def AjoutBitParite(message):
    somme = 0
    for bit in message:
        if bit == "1":
            somme += 1
    if somme % 2 == 0:
        return message + "1"
    else:
        return message + "0"


def SimulTransmission(paquet, p):
    copy = ""
    for i in range(len(paquet)):
        proba = random.random()
        if proba <= p:
            if paquet[i] == "1":
                copy += "0"
            else:
                copy += "1"
        else:
            copy += paquet[i]
    return copy


def controle(paquet):
    parite = paquet[-1]
    somme = 0
    for i in range(len(paquet) - 1):
        if paquet[i] == "1":
            somme += 1
    return somme % 2 == 0 and parite == "1" or somme % 2 == 1 and parite == "0"


if __name__ == '__main__':
    pass
