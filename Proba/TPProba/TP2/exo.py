#!/bin/env python3

import random
import matplotlib.pyplot as plt


# exo 1

def tirage_de():
    return random.randint(1, 6)


def moyenne(N):
    liste = []
    for i in range(N):
        liste.append(tirage_de())
    return sum(liste) / len(liste)


def frequence(N):
    liste = {}
    result = {}
    for i in range(N):
        face = tirage_de()
        if face in liste:
            liste[face] += 1
        else:
            liste[face] = 1
    for couple in liste.items():
        result[couple[0]] = couple[1] / N
    return result


def show():
    frq = frequence(1000)

    # transforme le dico en liste
    x = []
    y = []
    for i in frq.items():
        x.append(i[0])
        y.append(i[1])

    # affiche
    plt.bar(x, y, align="center", width=0.5)
    plt.show()


# show()

# exo 2
def AjoutBitParite(message):
    somme = 0
    for bit in message:
        if bit == "1":
            somme += 1
    if somme % 2 == 0:
        return message + "1"
    else:
        return message + "0"


def SimulTransmission(paquet, p):
    copy = ""
    for i in range(len(paquet)):
        proba = random.random()
        if proba <= p:
            if paquet[i] == "1":
                copy += "0"
            else:
                copy += "1"
        else:
            copy += paquet[i]
    return copy


def controle(paquet):
    parite = paquet[-1]
    somme = 0
    for i in range(len(paquet) - 1):
        if paquet[i] == "1":
            somme += 1
    return somme % 2 == 0 and parite == "1" or somme % 2 == 1 and parite == "0"


# print(controle(SimulTransmission(AjoutBitParite("110"), 0.2)))

# exo 3

# 15 pas de long
# 4 pas de large

# ou 1 pas en avant
# ou 1 diagonale vers la droite
# ou 1 diagonale vers la gauche

# maximum 15 pas

direction = [
    [0, 1],  # 1 pas en avant
    [1, 1],  # 1 pas diagonale vers la droite
    [-1, 1]  # 1 pas diagonale vers la gauche
]


def simulation():
    success = 0
    for i in range(10000):
        x = 2
        y = 0
        nbPas = 0
        while (y < 15 and x >= 0 and x <= 4 and nbPas < 15):
            pas = direction[random.randint(0, 2)]
            x += pas[0]
            y += pas[1]
            nbPas += 1
        if (y == 15 and x >= 0 and x <= 4 and nbPas <= 15):
            success += 1
    return success / 10000


# approche de a,b,c,d,e 15
# et on retourne la proba
def approche():
    a = 0
    b = 0
    c = 1
    d = 0
    e = 0
    for n in range(15):
        oa = a
        ob = b
        oc = c
        od = d
        oe = e

        a = (oa + ob) / 3
        b = (oa + ob + oc) / 3
        c = (ob + oc + od) / 3
        d = (oc + od + oe) / 3
        e = (od + oe) / 3
    return a + b + c + d + e


# exo 4

def sondage(N):
    frequences = []
    for i in range(500):
        oui = 0
        for j in range(N):
            if (random.random() <= 0.49):
                oui += 1
        frequences.append(oui / N)
    return frequences


"""
result = []
result.append(sondage(1000))
result.append(sondage(5000))

# on affiche les deux fréquences dans la même fenêtre
plt.boxplot(result, widths=0.2)
plt.show()
"""
