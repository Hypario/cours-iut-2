#!/bin/env python3

# les différents mots que l'on retrouve dans les spams
attributes = ['password', 'review', 'send', 'us', 'your', 'account']

# échantillon connu de mail
tableau_donnees = {
    'spam' : ["Send us your password", "Review us", "Send your password", "Send us your account"],
    'ham': ["Send us your review", "Review your password"]
}

# retourne toute les instances 
# de tout les mails dans la base de donnée connu (spam ou ham)
def base(data):
    result = []
    for mails in data.items(): 
        for mail in mails[1]:
            instance = []
            sentence = mail.lower().split()
            for attribute in attributes:
                if attribute not in sentence:
                    instance.append('not_' + attribute)
                else:
                    instance.append(attribute)
            instance.append(mails[0])    
            result.append(instance)
    return result

# équivalent de base, mais pour 1 mail inconnu
def baseN(mail):
    instance = []
    sentence = mail.lower().split()
    for attribute in attributes:
        if attribute not in sentence:
            instance.append('not_' + attribute)
        else:
            instance.append(attribute)
    return instance


# tableau issu de la fonction base
def instances(base, c):
    instances = []
    for attributes in base:
        if attributes[-1] == c:
            instances.append(attributes)
    return instances

# calcule la probabilité d'avoir un C (spam ou ham)
def probabilite(c):
    data = base(tableau_donnees)
    nb = 0
    count = 0
    for attributes in data:
        if attributes[-1] == c:
            nb += 1
        count += 1
    if (count != 0):
        return nb / count
    return 0
    

# retourne le nombre d'instance de C qui contiens l'/les attribue(s) a 
# dans le tableau de donnée
# exemple : nombre_instances(tableau_donnees, 'send', 'spam') = 3
def nombre_instances(data, a, c):
    ins = instances(base(data), c)
    count = 0
    for instance in ins:
        if a in instance:
            count += 1
    return count


def proba_conditionnelle(data, a, c):
    return nombre_instances(data, a, c) / len(data[c])

# an est une liste d'attribut 
def produit_probas_conditionnelle(data, an, c):
    prod = 1
    for a in an:
        prod *= proba_conditionnelle(data, a, c)
    return prod * probabilite(c)

def prediction(data, mail):
    an = baseN(mail)
    p_spam = produit_probas_conditionnelle(data, an, 'spam')
    p_ham = produit_probas_conditionnelle(data, an, 'ham')
    print(p_spam, p_ham)
    if (p_spam > p_ham):
        return "Spam !"
    else:
        return "Ham !"

# print(produit_probas_conditionnelle(tableau_donnees, ['not_password', 'send'], 'spam'))

print(prediction(tableau_donnees, 'review us now'))