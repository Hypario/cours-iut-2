#!/usr/bin/env python3
import copy

def permutation(mat, i, j):
    if 1 <= i <= len(mat) and 1 <= j <= len(mat):
        clone = copy.deepcopy(mat)  # clone la matrice
        # permute
        tmp = clone[i - 1]
        clone[i - 1] = clone[j - 1]
        clone[j - 1] = tmp
        return clone  # retourne le clone
    else:
        raise Exception("i or j out of bound, limit is :", 1, "to", len(mat))


def elimination(mat, i, j):
    if 1 <= i <= len(mat) and 1 <= j <= len(mat[i]):
        clone = copy.deepcopy(mat)  # clone la matrice
        for line in range(i, len(clone)):
            if clone[i - 1][j - 1] != 0:
                clone[line][j - 1] = clone[line][j - 1] * clone[i - 1][j - 1] - clone[i - 1][j - 1] * clone[line][j - 1]
            else:
                attempt = 1
                while clone[i - 1][j - 1] == 0 and (j + attempt) < len(mat[i]):
                    clone = permutation(clone, i, j + attempt)
                    attempt += 1
        return clone
    else:
        raise Exception('i or j out of bound')


def methode_zero():
    matrice = []
    while True:
        try:
            n = int(input("Quel est la dimension de votre espace vectoriel : "))
        except:
            continue
        for i in range(0, n):
            try:
                value = int(input("Valeur : "))
                matrice[0].append(value)
            except:
                continue
        break
    print(matrice)


matrice = [
    [0, 1, 2],
    [1, 0, 2],
    [1, 2, 0],
    [2, 3, 4]
]

if __name__ == '__main__':
    methode_zero()
