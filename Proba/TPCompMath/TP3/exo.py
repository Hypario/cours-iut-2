import numpy as np

# 2)
p_0 = np.matrix([1, 0, 0])

# 3)
M = np.matrix([
    [8/10, 1/10, 1/10],
    [2/10, 7/10, 1/10],
    [3/10, 1/10, 6/10]
])

p_1 = p_0.dot(M)
p_2 = p_1.dot(M)

print(p_1) # [0.8, 0.1, 0.1]
print(p_2) # [0.69, 0.16, 0.15]

"""
4) p_1 = p_0 * M = [0.8, 0.1, 0.1]
à l'étape 1, si on interroge une personne il y a 80% de chance qu'elle reste favorable au modèle A
10% de chance qu'elle s'oriente vers le modèle B
10% de chance qu'elle s'oriente vers le modèle C

p_2 = p_1 * M = [0.69, 0.16, 0.15]
à l'étape 2, si on interroge une personne il y a 69% de chance qu'elle reste favorable au modèle A
16% de chance qu'elle s'oriente vers le modèle B
15% de chance qu'elle s'oriente vers le modèle C

5) Pn = P_0 * M^n

6)

"""