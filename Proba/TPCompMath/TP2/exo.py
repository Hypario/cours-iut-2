from sympy.matrices import Matrix

# 1
A = Matrix((
    0, 1, 1, 0, 0,
    0, 0, 1, 1, 1,
    0, 1, 0, 1, 1,
    0, 0, 0, 0, 1,
    0, 0, 0, 0, 0)).reshape(5, 5)

# 2.a lambda => valeurs propres
# 2.b Elambda => sous espaces vectoriels propres
# 2.c eigenvals de sympy

# 3.a
vp = list(A.eigenvals().keys())  # les valeur propres (sans les multiplicités)
ep = [list(_[2][0]) for _ in A.eigenvects()]  # les vecteurs propres (sans les multiplicités)

# 3.b
# on trouve comme vecteurs propre [0, -1, 1, 0, 0] impossible car valeur négative
# [1, 0, 0, 0, 0]
# [2, 1, 1, 0, 0]

# on aura donc la page 1 en premier, le reste ne sera pas référencé

# 4) les pages 2 et 3 en premier puis la 1 puis la 4 et la 5

# modèle Page Rank :

# l1 = 0 l2 = 2 l3 = 3 l4 = 2 l5 = 3 l6 = 1 l7 = 0

# L1 = [0, 0, 0, 1/2, 1/3, 0, 0]

# parce qu'on considère que l'utilisateur a la même probabilité de cliquer sur un autre lien, un lien différent

A = Matrix((
    0, 0, 0, 1/2, 1/3, 0, 0,
    0, 0, 1/3, 0, 1/2, 0, 0,
    0, 1/2, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 1/3, 0, 0,
    0, 0, 0, 1/2, 0, 0, 0,
    0, 1/2, 1/3, 0, 0, 0, 0,
    0, 0, 1/3, 0, 0, 0, 0
)).reshape(7, 7)

vp2 = list(A.eigenvals().keys())
"""
[
    -sqrt(6)/6, sqrt(6)/6, 1/(6*(sqrt(3)/36 + 1/12)**(1/3)) + (sqrt(3)/36 + 1/12)**(1/3), 
    1/(6*(-1/2 + sqrt(3)*I/2)*(sqrt(3)/36 + 1/12)**(1/3)) + (-1/2 + sqrt(3)*I/2)*(sqrt(3)/36 + 1/12)**(1/3), 
    (-1/2 - sqrt(3)*I/2)*(sqrt(3)/36 + 1/12)**(1/3) + 1/(6*(-1/2 - sqrt(3)*I/2)*(sqrt(3)/36 + 1/12)**(1/3)), 
    0
]

Les vecteurs propres ne sont pas calculable (python ne me donne pas de résultat)

Pas de conclusion possible
"""
