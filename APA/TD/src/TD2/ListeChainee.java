package TD2;

public class ListeChainee {

    protected Element first;

    public ListeChainee() {
        first = null;
    }

    public Element getFirst() {
        return first;
    }

    public Element getLast() {
        Element current = first;
        while(current.getSuivant() != null) {
            current = current.getSuivant();
        }
        return current;
    }

    public int size() {
        int sum = 0;
        Element current = first;
        while (current != null) {
            sum += 1;
            current = current.getSuivant();
        }
        return sum;
    }

    public void print() {
        Element current = first;
        while (current != null) {
            System.out.println(current.getInfo());
            current = current.getSuivant();
        }
    }

    public boolean search(int i) {
        Element current = first;
        while (current != null) {
            if (current.getInfo() == i) {
                return true;
            }
            current = current.getSuivant();
        }
        return false;
    }

    public boolean remove(int i) {
        Element current = first;
        Element previous = first;
        if (first.getInfo() == i) {
            first = first.getSuivant();
            return true;
        }
        while (current != null) {
            if (current.getInfo() == i) {
                previous.setSuivant(current.getSuivant());
                return true;
            }
            previous = current;
            current = current.getSuivant();
        }
        return false;
    }

    public boolean removeAll(int i) {
        Element current = first;
        Element previous = first;
        boolean deleted = false;
        while (first.getInfo() == i) {
            first = first.getSuivant();
            current = first;
            previous = first;
            deleted = true;
        }
        while (current != null) {
            if (current.getInfo() == i) {
                previous.setSuivant(current.getSuivant());
                current = current.getSuivant();
                deleted = true;
            } else {
                previous = current;
                current = current.getSuivant();
            }
        }
        return deleted;
    }

    public boolean removePosition(int pos) {
        int currentPos = 0;
        Element current = first;
        Element previous = first;
        if (pos == currentPos) {
            first = first.getSuivant();
            return true;
        }
        while (current != null) {
            if (pos == currentPos) {
                previous.setSuivant(current.getSuivant());
                return true;
            }
            previous = current;
            current = current.getSuivant();
            currentPos += 1;
        }
        return false;
    }

    public void ajouterTete(int i) {
        first = new Element(i, first);
    }

    public void ajouterQueue(int i) {
        Element current = first;
        while (current.getSuivant() != null) {
            current = current.getSuivant();
        }
        current.setSuivant(new Element(i));
    }
}
