package TD2;

public class Test {

    public static void main(String[] args) {
        ListeChainee liste = new ListeChainee();
        System.out.println(liste.search(2));

        liste.ajouterTete(15);

        ListeChaineeDeuxPointeurs liste2 = new ListeChaineeDeuxPointeurs(liste);
        liste2.print();
        liste2.remove(15);
        liste2.print();
    }

}
