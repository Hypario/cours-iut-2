package TD2;

public class ListeChaineeDeuxPointeurs {

    private Element tete;
    private Element queue;

    public ListeChaineeDeuxPointeurs() {
        tete = null;
        queue = null;
    }

    public ListeChaineeDeuxPointeurs(ListeChainee next) {
        tete = next.getFirst();
        queue = next.getLast();
    }

    public boolean search(int i) {
        Element current = tete;
        while (current != null) {
            if (current.getInfo() == i) {
                return true;
            }
            current = current.getSuivant();
        }
        return false;
    }

    public boolean remove(int i) {
        Element current = tete;
        Element previous = tete;
        if (tete == queue) {
            tete = null;
            queue = null;
            return true;
        }
        if (tete.getInfo() == i) {
            tete = tete.getSuivant();
            return true;
        }
        while (current.getSuivant() != null) {
            if (current.getInfo() == i) {
                previous.setSuivant(current.getSuivant());
                return true;
            }
            previous = current;
            current = current.getSuivant();
        }
        if (current.getInfo() == i) {
            queue = previous;
            queue.setSuivant(null);
            return true;
        }
        return false;
    }

    public void print() {
        Element current = tete;
        while (current != null) {
            System.out.println(current.getInfo());
            current = current.getSuivant();
        }
    }

}
