package TD3;

import java.util.ArrayList;
import java.util.Arrays;

public class ABR {

    private int valeur;

    private ABR gauche = null;

    private ABR droite = null;

    public int size = 0;

    public ABR(int i) {
        valeur = i;
        size++;
    }

    public ABR(int... i) throws Exception {
        ABR current, parent;
        if (i.length == 0) {
            throw new Exception("Le tableau ne peut pas être vide");
        }
        valeur = i[0];
        for (int j = 1; j < i.length; j++) {
            current = this;
            parent = this;
            while (current != null) {
                parent = current;
                current = i[j] <= current.getValeur() ? current.getGauche() : current.getDroite();
            }
            if (i[j] <= parent.getValeur()) {
                parent.setGauche(new ABR(i[j]));
            } else {
                parent.setDroite(new ABR(i[j]));
            }
            size++;
        }
    }

    public void insert(int i) {
        ABR current = this;
        ABR parent = this;

        while (current != null) {
            parent = current;
            int value = current.getValeur();
            boolean test = i <= valeur;
            current = i <= current.getValeur() ? current.getGauche() : current.getDroite();
        }

        if (i <= parent.getValeur()) {
            parent.setGauche(new ABR(i));
        } else {
            parent.setDroite(new ABR(i));
        }
        size++;
    }

    public int maximum() {
        ABR current = this;
        ABR parent = this;
        while (current != null) {
            parent = current;
            current = current.getDroite();
        }
        return parent.getValeur();
    }

    public int minimum() {
        ABR current = this;
        ABR parent = this;
        while (current != null) {
            parent = current;
            current = current.getGauche();
        }

        return parent.getValeur();
    }

    public int size() {
        return size;
    }

    public int hauteurR() {
        int gauche = 1, droite = 1;

        // on récupère la partie gauche
        if (this.getGauche() != null) {
            gauche = 1 + this.getGauche().hauteurR();
        }
        // on récupère la partie droite
        if (this.getDroite() != null) {
            droite = 1 + this.getDroite().hauteurR();
        }

        // on retourne le maximum
        return Math.max(droite, gauche);
    }

    public int hauteurS() {
        ArrayList<ABR> stack = new ArrayList<>();
        stack.add(this);
        int hauteur = 1;
        while (true) {
            if (stack.isEmpty()) {
                return hauteur;
            }
            ABR current = stack.remove(0);
            if (current.getGauche() != null || current.getDroite() != null) {
                hauteur++;
            }
            if (current.getGauche() != null) {
                stack.add(current.getGauche());
            }
            if (current.getDroite() != null) {
                stack.add(current.getDroite());
            }
        }
    }

    public boolean equilibre() {
        return gauche.hauteurS() == droite.hauteurS() ||
                gauche.hauteurS() + 1 == droite.hauteurS() ||
                gauche.hauteurS() == droite.hauteurS() + 1;
    }

    public boolean equals(ABR a) {
        boolean equals = a.getValeur() == getValeur();

        if (!equals)
            return equals;

        if (
                (
                        getDroite() != null && a.getDroite() == null
                ) || (
                        getDroite() == null && a.getDroite() != null
                ) || (
                        getGauche() != null && a.getGauche() == null
                ) || (
                        getGauche() == null && a.getGauche() != null
                )
        ) {
            equals = false;
        }

        if (getGauche() != null)
            equals = equals && getGauche().equals(a.getGauche());

        if (getDroite() != null)
            equals = equals && getDroite().equals(a.getDroite());

        return equals;
    }

    public int getValeur() {
        return valeur;
    }

    public ABR getDroite() {
        return droite;
    }

    public void setDroite(ABR droite) {
        this.droite = droite;
    }

    public ABR getGauche() {
        return gauche;
    }

    public void setGauche(ABR gauche) {
        this.gauche = gauche;
    }
}
