package TD3;

public class Test {

    public static void main(String[] args) {

        int[] tab = {5, 1, 3, 7};
        ABR arbre = new ABR(5); // hauteur = 1

        arbre.insert(1); // hauteur = 2 (insérer à gauche)
        arbre.insert(3); // hauteur = 3 (insérer à gauche puis droite)
        arbre.insert(7); // hauteur = 2 (insérer à droite)

        try {
            ABR arbre2 = new ABR(tab);
            System.out.println(arbre.equals(arbre2));
        }catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        System.out.println(arbre.size());
        System.out.println(arbre.hauteurR()); // return 2
        System.out.println(arbre.hauteurS()); // return 2
    }

}
