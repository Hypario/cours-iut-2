package TD1;

import java.util.Arrays;

public class Tableau{

    private int[] t;

    public Tableau(int[] init) {
        this.t = new int[init.length];
        for(int i = 0; i < init.length; i++)
            this.t[i] = init[i];
    }

    public static void main(String[] args) {
        int[] t = {2, 1};
        Tableau tab = new Tableau(t);
        System.out.println(tab.somme());
        System.out.println(tab.max());
        System.out.println(tab);
        tab.tri();;
        System.out.println(tab);
    }

    public String toString()
    {
        String s = "{";
        for(int i = 0; i < this.t.length; i++)
        {
            if (i == this.t.length - 1) {
                s += this.t[i];
            } else {
                s += this.t[i] + " ";
            }
        }
        return s + "}";
    }

    public int get(int i) throws Exception {
        if (i < 0 || i >= this.t.length)
            throw new Exception("indice_interdit");
        return this.t[i];
    }

    public int somme() {
        int sum = 0;
        for (int i = 0; i < this.t.length; i++) {
            sum += this.t[i];
        }
        return sum;
    }

    public int compteNegatifs()
    {
        int sum = 0;
        for (int i = 0; i < t.length; i++) {
            if (t[i] < 0)
                sum++;
        }
        return sum;
    }

    public int max()
    {
        int max = this.t[0];
        for (int i = 1; i < this.t.length; i++) {
            if (max < this.t[i])
                max = this.t[i];
        }
        return max;
    }

    public int cherche(int i) {
        for (int j = 0; j < this.t.length; i++) {
            if (this.t[j] == i) return j;
        }
        return -1;
    }

    // tri par sélection
    public void tri() {
        int min;
        for (int i = 0; i < this.t.length; i++) {
            min = i;
            // on cherche le minimum
            for (int j = i; j < this.t.length; j++) {
                if (this.t[min] > this.t[j]) min = j;
            }
            // on switch l'élément sélectionner et le minimum
            int temp = this.t[i];
            this.t[i] = this.t[min];
            this.t[min] = temp;
        }
    }

    /*
    public int chercheTri(int i) {
        tri();
    }
    */

}
