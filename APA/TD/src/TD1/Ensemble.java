package TD1;

class Ensemble
{

    public boolean[] tab;
    static final int MAX = 101;
    private int n = 0;

    Ensemble() {
        this.tab = new boolean[MAX];
        for (int i = 0; i < MAX; i++) {
            this.tab[i] = false;
        }
    }

    Ensemble(int min, int max) {
        super();
        if (this.contient(max)) {
            for (int i = min; i < max; i++) {
                this.tab[i] = true;
            }
        }
    }

    public static void main(String[] args) {
        Ensemble e = new Ensemble();
        e.add(1);
    }

    public int cardinal() {
        return this.n;
    }

    public boolean estVide() {
        return n == 0;
    }

    public boolean contient(int i) {
        if (i > 0 && i < this.MAX) {
            return this.tab[i];
        }
        return false;
    }

    public boolean add(int i) {
        if (!this.contient(i)) {
            this.tab[i] = true;
            return true;
        }
        return false;
    }

    public boolean inclus(Ensemble e) {
        for (int i = 0; i < this.MAX; i++) {
            if (!(e.contient(i) && this.tab[i]))  {
                return false;
            }
        }
        return false;
    }

    public Ensemble union(Ensemble e) {
        Ensemble e2 = new Ensemble();
        for (int i = 0; i < this.MAX; i++) {
            if (e.tab[i] || this.tab[i]) {
                e2.add(i);
            }
        }
        return e2;
    }
}
