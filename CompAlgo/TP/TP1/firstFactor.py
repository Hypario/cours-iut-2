#!/usr/bin/env python3

n = int(input("Entrez un entier positif : "))
print("Les facteurs premiers sont :")
i = 1
while (i <= n):
    k = 0
    if (n % i == 0):
        j = 1
        while(j <= i):
            if (i % j != 0):
                k = k + 1
            j = j + 1
        if (k == 2):
            print(i)
    i = i + (k - 1)

