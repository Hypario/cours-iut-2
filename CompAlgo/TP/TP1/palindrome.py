#!/usr/bin/env python3

n = int(input("Entrez un entier positif : "))
temp=n
rev=0
while (n > 0):
    dig = n % 10
    rev = rev * 10 + dig
    n = n // 10
if (temp == rev):
    print("Le nombre est un palindrome!")
else:
    print("Le nombre n'est pas un palindrome!")
