<?php

use PHPUnit\Framework\TestCase;
use TP2\Hello;

class Exo1Test extends TestCase
{

    public function testHello()
    {
        $this->expectOutputString("Hello World !");
        Hello::bonjour();
    }

}
