<?php


namespace TP2;


class TravailleurHoraire extends Employe
{

    /**
     * @var float
     */
    private $salaire;

    /**
     * @var float
     */
    private $taux;

    /**
     * @var int
     */
    private $quantite;

    public function __construct(string $nom, string $prenom, float $salaire)
    {
        parent::__construct($nom, $prenom);
        $this->salaire = $salaire;
    }

    /**
     * @return float
     */
    function traitement(): float
    {
        return $this->salaire + ($this->getTaux() * $this->getQuantite());
    }

    /**
     * @return float
     */
    public function getTaux(): float
    {
        return $this->taux;
    }

    /**
     * @param float $taux
     */
    public function setTaux(float $taux): void
    {
        $this->taux = $taux;
    }

    /**
     * @return int
     */
    public function getQuantite(): int
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite(int $quantite): void
    {
        $this->quantite = $quantite;
    }
}
