<?php


namespace TP2;


class Rectangle extends Figure
{

    /**
     * @var float
     */
    protected $longueur = 0.0;

    /**
     * @var float
     */
    protected $largeur = 0.0;

    public function __construct(?Point $origine = null, float $longueur = 0.0, float $largeur = 0.0)
    {
        parent::__construct($origine);
        $this->longueur = $longueur;
        $this->largeur = $largeur;
    }

    public function __toString(): string
    {
        return sprintf(parent::__toString() . ", longueur : %5.2f, largeur : %5.2f", $this->getLongueur(), $this->getLargeur());
    }

    /**
     * @return float
     */
    public function getLongueur(): float
    {
        return $this->longueur;
    }

    /**
     * @param float $longueur
     */
    public function setLongueur(float $longueur): void
    {
        $this->longueur = $longueur;
    }

    /**
     * @return float
     */
    public function getLargeur(): float
    {
        return $this->largeur;
    }

    /**
     * @param float $largeur
     */
    public function setLargeur(float $largeur): void
    {
        $this->largeur = $largeur;
    }

    /**
     * @return float
     */
    public function aire(): float
    {
        return $this->getLongueur() * $this->getLargeur();
    }

}
