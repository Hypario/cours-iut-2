<?php


namespace TP2;


class Figure implements Drawable
{

    /**
     * @var Point
     */
    protected $origine;

    public function __construct(Point $origine = null)
    {
        if (is_null($origine)) {
            $this->origine = new Point();
        } else {
            $this->origine = $origine;
        }
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function __toString(): string
    {
        $reflectionClass = new \ReflectionClass($this);
        return $reflectionClass->getShortName() . " : origine : " . $this->getOrigine();
    }

    /**
     * @return Point
     */
    public function getOrigine(): Point
    {
        return $this->origine;
    }

    /**
     * @param Point $origine
     */
    public function setOrigine(Point $origine = null)
    {
        if (!is_null($origine)) {
            $this->origine = $origine;
        }
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function dessine()
    {
        return "dessine " . $this->__toString();
    }
}
