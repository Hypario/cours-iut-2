<?php


namespace TP2;


abstract class Employe
{

    public static $numEmp = 1;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $nom;

    /**
     * @var string
     */
    public $prenom;

    public function __construct(string $nom, string $prenom)
    {
        $this->id = $this::$numEmp++;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return float
     */
    abstract function traitement(): float;

    public function __toString()
    {
        return "id: " . $this->getId() . ", " . $this->getPrenom() . " " . $this->getNom() . ', traitement: ' . sprintf("%7.2f", $this->traitement());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function __clone()
    {
        $this->id = $this::$numEmp++;
    }

}
