<?php


namespace TP2;


class Parallelepipede extends Rectangle
{

    /**
     * @var float
     */
    private $hauteur;

    public function __construct(?Point $origine = null, float $longueur = 0.0, float $largeur = 0.0, float $hauteur = 0.0)
    {
        parent::__construct($origine, $longueur, $largeur);
        $this->hauteur = $hauteur;
    }

    public function __toString(): string
    {
        return sprintf(parent::__toString() . ', hauteur : %5.2f', $this->getHauteur());
    }

    /**
     * @return float
     */
    public function getHauteur(): float
    {
        return $this->hauteur;
    }

    /**
     * @param float $hauteur
     */
    public function setHauteur(float $hauteur): void
    {
        $this->hauteur = $hauteur;
    }

    /**
     * @return float
     */
    public function volume(): float
    {
        return $this->getLongueur() * $this->getLargeur() * $this->getHauteur();
    }

}
