<?php


namespace TP2;


class Patron extends Employe
{

    /**
     * @var float
     */
    private $salaire;

    public function __construct(string $nom, string $prenom, float $salaire)
    {
        parent::__construct($nom, $prenom);
        $this->salaire = $salaire;
    }

    /**
     * @return float
     */
    function traitement(): float
    {
        return $this->salaire;
    }

    /**
     * @return float
     */
    public function getSalaire(): float
    {
        return $this->salaire;
    }

    /**
     * @param float $salaire
     */
    public function setSalaire(float $salaire): void
    {
        $this->salaire = $salaire;
    }

}
