<?php


namespace TP2;


class TravailleurCom extends Employe
{

    /**
     * @var float
     */
    private $salaire;

    /**
     * @var float
     */
    private $commission;

    /**
     * @var int
     */
    private $quantite;

    public function __construct(string $nom, string $prenom, float $salaire)
    {
        parent::__construct($nom, $prenom);
        $this->salaire = $salaire;
    }

    /**
     * @return float
     */
    function traitement(): float
    {
        return $this->salaire + ($this->getCommission() * $this->getQuantite());
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     */
    public function setCommission($commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param mixed $quantite
     */
    public function setQuantite($quantite): void
    {
        $this->quantite = $quantite;
    }
}
