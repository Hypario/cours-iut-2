<?php

namespace tp2;

class Point
{
    /**
     * @var float
     */
    private $x;
    /**
     * @var float
     */
    private $y;

    /**
     * Point constructor.
     * @param float $x
     * @param float $y
     */
    public function __construct(float $x = 0.0, float $y = 0.0)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function __toString(): string
    {
        return sprintf("Point (%5.2f, %5.2f)", $this->x, $this->y);
    }

}
