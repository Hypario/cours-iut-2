<?php

use PHPUnit\Framework\TestCase;
use TP2\Figure;
use TP2\MaFigure;
use TP2\Parallelepipede;
use TP2\Point;
use TP2\Rectangle;

/**
 * @testdox Les concepts objets
 */
class TP2Section1Test extends TestCase {

    /**
     * @test
     * @testdox déclaration des attributs x et y
     */
    function test_attributs_x_et_y() {
        $this->assertClassHasAttribute('x', Point::class);
        $this->assertClassHasAttribute('y', Point::class);
    }

    /**
     * @test
     * @testdox test classe Point
     */
    function test_point() {
        $p = new Point;
        $this->assertInstanceOf(Point::class, $p);
    }

    /**
     * @test
     * @testdox test toString point
     */
    function test_toString_point() {
        $p = new Point();
        $this->assertEquals('Point ( 0.00,  0.00)', "" . $p);
        $p = new Point(10.52, 20.13);
        $this->assertEquals('Point (10.52, 20.13)', "" . $p);
    }


    /**
     * @test
     * @testdox test classe figure
     */
    function test_class_figure() {
        $f = new Figure(null);
        $this->assertInstanceOf(Figure::class, $f);
    }

    /**
     * @test
     * @testdox test toString figure
     */
    function test_toString_figure() {
        $f = new Figure(null);
        $this->assertEquals('Figure : origine : Point ( 0.00,  0.00)', "" . $f);
        $f = new Figure(new Point(10.52, 20.13));
        $this->assertEquals('Figure : origine : Point (10.52, 20.13)', "" . $f);
    }


    /**
     * @test
     * @testdox test getOrigine figure
     */
    function test_get_Origine() {
        $f = new Figure(null);
        $p = new Point();
        $this->assertEquals("" . $p, "" . $f->getOrigine());
        $p = new Point(10.52, 20.13);
        $f = new Figure($p);
        $this->assertEquals("" . $p, "" . $f->getOrigine());
    }

    /**
     * @test
     * @testdox test setOrigine figure
     */
    function test_set_Origine() {
        $f = new Figure();
        $p = new Point();
        $f->setOrigine();
        $this->assertEquals("" . $p, "" . $f->getOrigine());
        $p = new Point(10.52, 20.13);
        $f->setOrigine($p);
        $this->assertEquals("" . $p, "" . $f->getOrigine());
    }

    /**
     * @test
     * @testdox déclaration des attributs dans la classe Rectangle
     */
    function test_attributs_Rectangle_x_et_y() {
        $this->assertClassHasAttribute('origine', Rectangle::class);
        $this->assertClassHasAttribute('longueur', Rectangle::class);
        $this->assertClassHasAttribute('largeur', Rectangle::class);
    }


    /**
     * @test
     * @testdox test class Rectangle
     */
    function test_Rectangle() {
        $r = new Rectangle();
        $this->assertInstanceOf(Rectangle::class, $r);
    }


    /**
     * @test
     * @testdox test toString rectangle
     */
    function test_toString_rectangle() {
        $r = new Rectangle(null);
        $this->assertEquals('Rectangle : origine : Point ( 0.00,  0.00), longueur :  0.00, largeur :  0.00', "" . $r);
        $r = new Rectangle(new Point(10.52, 20.13), 10, 20);
        $this->assertEquals('Rectangle : origine : Point (10.52, 20.13), longueur : 10.00, largeur : 20.00', "" . $r);
    }

    /**
     * @test
     * @testdox test getXXX rectangle
     */
    function test_get_rectangle() {
        $r = new Rectangle(null);
        $p = new Point();
        $this->assertEquals("" . $p, "" . $r->getOrigine());
        $this->assertEquals(0.0, "" . $r->getLongueur());
        $this->assertEquals(0.0, "" . $r->getLargeur());
        $p = new Point(10.52, 20.13);
        $r = new Rectangle($p, 10, 20);
        $this->assertEquals("" . $p, "" . $r->getOrigine());
        $this->assertEquals(10.0, "" . $r->getLongueur());
        $this->assertEquals(20.0, "" . $r->getLargeur());
    }

    /**
     * @test
     * @testdox test setXXX Rectangle
     */
    function test_set_rectangle() {
        $r = new Rectangle(null);
        $p = new Point();
        $r->setOrigine();
        $this->assertEquals("" . $p, "" . $r->getOrigine());
        $p = new Point(10.52, 20.13);
        $r->setOrigine($p);
        $this->assertEquals("" . $p, "" . $r->getOrigine());
        $r->setLongueur(10);
        $this->assertEquals(10.0 , $r->getLongueur());
        $r->setLargeur(20);
        $this->assertEquals(20.0 , $r->getLargeur());
        $this->assertEquals('Rectangle : origine : Point (10.52, 20.13), longueur : 10.00, largeur : 20.00', "" . $r);
    }

    /**
     * @test
     * @testdox test calcul de l'aire d'un Rectangle
     */
    function test_aire_rectangle() {
        $r = new Rectangle(null);
        $p = new Point();
        $r->setOrigine();
        $this->assertEquals(0.0,  $r->aire());
        $r->setLongueur(10);
        $r->setLargeur(20);
        $this->assertEquals(200.0 , $r->aire());
    }

    /**
     * @test
     * @testdox test class Parallelepipede
     */
    function test_Parallelepipede() {
        $p = new Parallelepipede();
        $this->assertInstanceOf(Parallelepipede::class, $p);
    }

    /**
     * @test
     * @testdox test toString parallélépipède
     */
    function test_toString_parallelepipede() {
        $p = new Parallelepipede(null);
        $this->assertEquals('Parallelepipede : origine : Point ( 0.00,  0.00), longueur :  0.00, largeur :  0.00, hauteur :  0.00', "" . $p);
        $p = new Parallelepipede(new Point(10.52, 20.13), 10, 20, 2);
        $this->assertEquals('Parallelepipede : origine : Point (10.52, 20.13), longueur : 10.00, largeur : 20.00, hauteur :  2.00', "" . $p);
    }

    /**
     * @test
     * @testdox test getXXX parallélépipède
     */
    function test_get_parallelepipede() {
        $pa = new Parallelepipede(null);
        $p = new Point();
        $this->assertEquals("" . $p, "" . $pa->getOrigine());
        $this->assertEquals(0.0, "" . $pa->getLongueur());
        $this->assertEquals(0.0, "" . $pa->getLargeur());
        $this->assertEquals(0.0, "" . $pa->getHauteur());
        $p = new Point(10.52, 20.13);
        $pa = new Parallelepipede($p, 10, 20,3);
        $this->assertEquals("" . $p, "" . $pa->getOrigine());
        $this->assertEquals(10.0, "" . $pa->getLongueur());
        $this->assertEquals(20.0, "" . $pa->getLargeur());
        $this->assertEquals(3.0, "" . $pa->getHauteur());
    }

    /**
     * @test
     * @testdox test setXXX parallélépipède
     */
    function test_set_parallelepipede() {
        $pa = new Parallelepipede(null);
        $p = new Point();
        $pa->setOrigine();
        $this->assertEquals("" . $p, "" . $pa->getOrigine());
        $p = new Point(10.52, 20.13);
        $pa->setOrigine($p);
        $this->assertEquals("" . $p, "" . $pa->getOrigine());
        $pa->setLongueur(10);
        $this->assertEquals(10.0 , $pa->getLongueur());
        $pa->setLargeur(20);
        $this->assertEquals(20.0 , $pa->getLargeur());
        $pa->setHauteur(15.5);
        $this->assertEquals(15.5 , $pa->getHauteur());
        $this->assertEquals('Parallelepipede : origine : Point (10.52, 20.13), longueur : 10.00, largeur : 20.00, hauteur : 15.50', "" . $pa);
    }

    /**
     * @test
     * @testdox test calcul du volume d'un parallélépipède
     */
    function test_volume_parallelepipede() {
        $r = new Parallelepipede(null);
        $p = new Point();
        $r->setOrigine();
        $this->assertEquals(0.0,  $r->volume());
        $r->setLongueur(10);
        $r->setLargeur(20);
        $r->setHauteur(1);
        $this->assertEquals(200.0 , $r->volume());
    }

    /**
     * @test
     * @testdox test interface Drawable
     */
    function test_dessine() {
        $f = new Figure(null);
        $this->assertEquals('dessine Figure : origine : Point ( 0.00,  0.00)' , $f->dessine());
        $r = new Rectangle();
        $this->assertEquals('dessine Rectangle : origine : Point ( 0.00,  0.00), longueur :  0.00, largeur :  0.00' , $r->dessine());
        $p = new Parallelepipede();
        $this->assertEquals('dessine Parallelepipede : origine : Point ( 0.00,  0.00), longueur :  0.00, largeur :  0.00, hauteur :  0.00' , $p->dessine());
    }

}
