<?php


use PHPUnit\Framework\TestCase;
use TP2\Employe;
use TP2\Patron;
use TP2\TravailleurCom;
use TP2\TravailleurHoraire;

class TP2Section2Test extends TestCase
{

    function test_attributs_Employe()
    {
        $this->assertClassHasAttribute('nom', Employe::class);
        $this->assertClassHasAttribute('prenom', Employe::class);
        $this->assertClassHasAttribute('id', Employe::class);
        $this->assertClassHasAttribute('numEmp', Employe::class);
    }

    function test_attributs_Patron()
    {
        $this->assertClassHasAttribute('salaire', Patron::class);
        $this->assertClassHasAttribute('nom', Patron::class);
        $this->assertClassHasAttribute('prenom', Patron::class);
        $this->assertClassHasAttribute('id', Patron::class);
    }

    function test_creation_Patron()
    {
        $p = new Patron('Duchmol', 'Robert', 4378.89);
        $this->assertInstanceOf(Patron::class, $p);
    }

    function test_traitement_Patron()
    {
        $p = new Patron('Duchmol', 'Robert', 4378.89);
        $this->assertEquals(4378.89, $p->traitement());
    }

    function test_clone_Patron()
    {
        $p = new Patron('Duchmol', 'Robert', 4378.89);
        $p1 = clone $p;
        $this->assertInstanceOf(Patron::class, $p1);
        $this->assertEquals($p->getNom(), $p1->getNom());
        $this->assertEquals($p->getPrenom(), $p1->getPrenom());
        $this->assertNotEquals($p->getId(), $p1->getId());
    }

    function test_toString_Patron()
    {
        $p = new Patron('Duchmol', 'Robert', 4378.89);
        $this->assertEquals('id: 5, Robert Duchmol, traitement: 4378.89', "" . $p);
    }

    function test_attributs_TravailleurCom()
    {
        $this->assertClassHasAttribute('salaire', TravailleurCom::class);
        $this->assertClassHasAttribute('commission', TravailleurCom::class);
        $this->assertClassHasAttribute('quantite', TravailleurCom::class);
        $this->assertClassHasAttribute('nom', TravailleurCom::class);
        $this->assertClassHasAttribute('prenom', TravailleurCom::class);
        $this->assertClassHasAttribute('id', TravailleurCom::class);
    }

    function test_traitement_TravailleurCom()
    {
        $p = new TravailleurCom('Duchmol', 'Robert', 2000.00);
        $p->setCommission(5.25);
        $p->setQuantite(10);
        $this->assertEquals(2052.5, $p->traitement());
    }

    function test_toString_TravailleurCom()
    {
        $p = new TravailleurCom('Duchmol', 'Robert', 2000.00);
        $p->setCommission(5.25);
        $p->setQuantite(10);
        $this->assertEquals('id: 7, Robert Duchmol, traitement: 2052.50', "" . $p);
    }

    function test_attributs_TravailleurHoraire()
    {
        $this->assertClassHasAttribute('salaire', TravailleurHoraire::class);
        $this->assertClassHasAttribute('taux', TravailleurHoraire::class);
        $this->assertClassHasAttribute('quantite', TravailleurHoraire::class);
        $this->assertClassHasAttribute('nom', TravailleurHoraire::class);
        $this->assertClassHasAttribute('prenom', TravailleurHoraire::class);
        $this->assertClassHasAttribute('id', TravailleurHoraire::class);
    }

    function test_traitement_TravailleurHoraire()
    {
        $p = new TravailleurHoraire('Duchmol', 'Robert', 2000.00);
        $p->setTaux(9.75);
        $p->setQuantite(10);
        $this->assertEquals(2097.5, $p->traitement());
    }

    function test_toString_TravailleurHoraire()
    {
        $p = new TravailleurHoraire('Duchmol', 'Robert', 2000.00);
        $p->setTaux(9.75);
        $p->setQuantite(10);
        $this->assertEquals('id: 9, Robert Duchmol, traitement: 2097.50', "" . $p);
    }


}
