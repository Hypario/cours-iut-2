<?php

//global $fruits;

$fruits = [];
$recettes = [];

/**
 * @param $nom
 * @param $protides
 * @param $glucides
 * @param $lipides
 * @param $calories
 */
function ajouterFruit($nom, float $protides = 0.0, float $glucides = 0.0, float $lipides = 0.0, float $calories = 0.0)
{
    global $fruits;
    if (!array_key_exists($nom, $fruits)) {
        $fruits[$nom]['protides'] = $protides;
        $fruits[$nom]['glucides'] = $glucides;
        $fruits[$nom]['lipides'] = $lipides;
        $fruits[$nom]['calories'] = $calories;
        /*
        echo sprintf("nom: %s lipide:%6.3f glucide:%6.3f protide:%6.3f calorie:%6.3f",
                $nom,
                $protides,
                $glucides,
                $lipides,
                $calories)
            . PHP_EOL;
        */
    } else {
        echo "Le fruit \"$nom\" est déjà répertorié." . PHP_EOL;
    }
}

/**
 * @param $nom
 * @param array $lesfruits
 */
function ajouterRecette($nom, array $lesfruits)
{
    global $fruits;
    global $recettes;
    foreach ($lesfruits as $fruit) {
        if (!array_key_exists($fruit, $fruits)) {
            echo "Le fruit \"$fruit\" n'est pas répertorié." . PHP_EOL;
            return;
        }
    }
    $recettes[$nom] = [];
    $recettes[$nom]['nom'] = $nom;
    $recettes[$nom]['lesfruits'] = $lesfruits;
}

/**
 * @param $nom
 */
function valNutRecette($nom)
{
    global $fruits;
    global $recettes;
    if (array_key_exists($nom, $recettes)) {
        $val = ['protides' => 0, 'glucides' => 0, 'lipides' => 0, 'calories' => 0];
        foreach ($recettes[$nom]['lesfruits'] as $fruit) {
            if (array_key_exists($fruit, $fruits)) {
                foreach ($fruits[$fruit] as $nut => $value) {
                    if ($nut !== "nom") {
                        $val[$nut] += $value;
                    }
                }
            }
        }
        echo sprintf("val nut de la recette \"$nom\" contient %6.3f protides, %6.3f lipides, %6.3f glucides, %6.3f calories." . PHP_EOL,
            $val['protides'], $val['glucides'], $val['lipides'], $val['calories']);
    } else {
        echo "La recette \"$nom\" n'est pas répertoriée." . PHP_EOL;
    }
}

ajouterFruit("Pomme", 0.3, 12.6, 0.3, 54);
ajouterFruit("Banane", 1.2, 20.5, 0.3, 90);
ajouterFruit("Orange", 1, 9, 0.2, 45);
ajouterRecette("Salade de fruits", ["Pomme", "Banane", "Orange" ]);
