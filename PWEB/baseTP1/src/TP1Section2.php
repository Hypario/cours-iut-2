<?php


namespace TP1;


use Exception;

class TP1Section2 {

    /**
     * @param mixed ...$elts
     * @return array
     */
    function addElementInTab(...$elts) {
        return $elts;
    }

    /**
     * @param mixed ...$elts
     * @return array
     */
    function addElementEnTeteTab(...$elts) {
        return array_reverse($elts);
    }

    /**
     * @param mixed ...$elts
     * @return array
     * @throws Exception
     */
    function stockTriValeurTableauAsso(...$elts): array {
        if (count($elts) % 2 != 0) {
            throw new \Exception("Nombre pas pair");
        } else {
            $ret = [];
            $i = 0;
            while ($i < count($elts) - 1) {
                $ret[$elts[$i]] = $elts[$i + 1];
                $i += 2;
            }
            asort($ret);
            return $ret;
        }
    }

    /**
     * @param mixed ...$elts
     * @return array
     * @throws Exception
     */
    function stockTriCleTableauAsso(...$elts): array {
        if (count($elts) % 2 != 0) {
            throw new \Exception("Nombre pas pair");
        } else {
            $ret = [];
            $i = 0;
            while ($i < count($elts) - 1) {
                $ret[$elts[$i]] = $elts[$i + 1];
                $i += 2;
            }
            ksort($ret);
            return $ret;
        }
    }

    /**
     * @param int $limite
     * @return array
     */
    function cribleEratosthene($limite = 100): array {
        $premiers = [];
        $tab = [];
        for ($i = 2; $i < $limite + 1; $i++) {
            $tab[] = $i;
        }

        while (count($tab) > 0) {
            $premiers[] = array_shift($tab);
            for($i = 1; $i < count($tab); $i++) {
                if ($tab[$i] % $tab[0] === 0) {
                    unset($tab[$i]);
                }
            }
        }
        return $premiers;
    }

    /**
     * @param int $max
     * @return array
     */
    function tableDeMultiplication($max = 10): array {
        $table = [];
        for($i = 0; $i < $max; $i++) {
            for ($j = 0; $j < $max; $j++) {
                $table[$i][] = ($i + 1) * ($j + 1);
            }
        }
        return $table;
    }

    /**
     * @param $matrice
     */
    function afficherTableDeMultiplication($matrice) {
        foreach($matrice as $key => $table) {
            foreach ($table as $value) {
                echo sprintf("%3d ", $value);
            }
            echo PHP_EOL;
        }
    }
}
