<?php

namespace TP1;

use phpDocumentor\Reflection\Types\Boolean;

/**
 * Déclarez ici la constante PI
 */

class TP1Section1 {

    /**
     * Ecrire la fonction estMajeur qui renvoie vrai si l'age donné en paramètre est supérieur ou égal à 18
     * et faux sinon
     * @param $age
     * @return bool
     */
    function estMajeur($age): bool {
        return $age >= 18;
    }


    /**
     * Ecrire la fonction sommeMultipleDeTrois qui renvoie la somme des entiers multiple de trois jusqu'à la limite donnée en paramètre
     * @param $limite
     * @return int
     */
    function sommeMultipleDeTrois($limite): int {
        $sum = 0;
        for ($i = 0; $i <= $limite / 3; $i++) {
            $sum += 3 * $i;
        }
        return $sum;
    }

    /**
     * Ecrire la fonction stockChocolat avec un paramètre stockDepart qui repond à la question
     * @param $stockDepart
     * @return float|int
     * Josette reçoit chaque jour un kilo de chocolat supérieur.
     * En un jour, avec la quantité de chocolat en stock, Josette fabrique 2 fois la quantité de chocolat en stock.
     * A la fin de la journée la quantité de chocolat en stock correspond à la somme de la quantité de chocolat reçue plus la quantité produite.
     * Calculez la quantité de chocolat produite  après 10 jours en fonction du stock de chocolat au départ.
     */
    function stockChocolat($stockDepart): int {
        for ($i = 1; $i <= 10; $i++) {
            $stockDepart = 1 + 2 * $stockDepart;
        }
        return $stockDepart;
    }

    /**
     * Sachant que f1 = 1, f2 = 1 et fi+2 = fi = fi+1
     * Ecrire un algorithme qui calcule le ième terme de la suite
     */
    function fibonacci($i): int {
        $suite = [1, 1];
        for ($j = 1; $j < $i - 1; $j++) {
            $suite[] = $suite[$j] + $suite[$j - 1];
        }
        return $suite[$j];
    }

    /**
     * Ecrire la fonction evolution qui renvoie le nombre d'années pour que la population des lemmongs de la Laponie soit supérieure à la population des lemmings de Toundra
     * @param $toundra
     * @param $laponie
     * @return int
     */
    function evolution($toundra, $laponie) {
        $i = 0;
        while ($toundra <= $laponie) {
            $toundra = 0.2 * $toundra + $toundra;
            $laponie += 50;
            $i++;
        }
        return $i;
    }

    /**
     * La fonction typeParametre renvoie une chaîne de caractères qui indique le type du paramètre
     * @param $var
     * @return string
     */
    function typeParametre($var) {
        if (is_array($var)) return "Tableau";
        if (is_bool($var)) return "Booléen";
        if (is_float($var)) return "Réel";
        if (is_int($var)) return "Entier";
        if (is_string($var)) return "String";
    }


    /**
     * Renvoie Vrai ou Faux en fonction de la valeur du paramètre
     * @param $var
     * @return string
     */
    function valbool($var) {
        if ($var)
            return "Vrai";
        return "Faux";
    }

    /**
     * Renvoie la valeur de PI avec une certaine précision indiqué par la valeur du paramètre epsilon
     * @param $epsilon
     * @return float|int
     */
    function decimalDePi($epsilon) {
        $piOld = 3;
        $signe = 1;
        $n = 3;
        $delta = 1000000;
        while ($delta > $epsilon) {
            $piNew = $piOld + $signe * (4 / ($n ** 3 - $n));
            $delta = abs($piNew - $piOld);
            $piOld = $piNew;
            $signe *= -1;
            $n += 2;
        }
        return $piNew;
    }

    /**
     * Ecrire les fonctions qui calcilent le périmètre, la surface du disqu, la surface de la sphère et le volume de la sphère avec un rayon donné en paramètre
     * @param $rayon
     * @return float|int
     */

    function perimetre($rayon) {
        return 2 * pi() * $rayon;
    }

    function surfaceDisque($rayon) {
        return 3.141592653 * $rayon ** 2;
    }

    function surfaceSphere($rayon) {
        return 4 * pi() * $rayon ** 2;
    }

    function volumeSphere($rayon) {
        return (4* pi() * $rayon ** 3) / 3;
    }
}
