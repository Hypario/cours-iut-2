const path = require('path');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const ManifestPlugin = require('webpack-manifest-plugin');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const dev = process.env.NODE_ENV || "development";

let cssLoaders = [
  {loader: MiniCssExtractPlugin.loader, options: {hmr: dev === "development"}},
  {loader: 'css-loader', options: {import: true}}];

if (dev === "production") {
  cssLoaders.push({
    loader: 'postcss-loader', options: {
      plugins: (loader) => {
        require('autoprefixer')()
      }
    }
  })
}

let config = {
  entry: {app: ["./src/ts/app.ts", "./src/scss/app.scss"]},
  watch: dev === "development",
  mode: dev,
  devtool: dev === "development" ? "cheap-module-eval-source-map" : "source-map",
  output: {
    path: path.resolve("./public/dist"),
    filename: "[name].bundle.js", // dev === "development" ? "[name].bundle.js" : "[name].[hash].js" for cache in prod
    publicPath: "/dist/"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
    alias: {
      '@css': path.resolve('./src/scss'),
      '@js': path.resolve('./src/ts')
    }
  },
  devServer: {
    contentBase: path.resolve('./public'),
    port: 8000
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /(node_modules)/
      },
      {
        test: /\.s[ac]ss$/,
        exclude: /(node_modules)/,
        use: [
          ...cssLoaders,
          'sass-loader'
        ]
      },
      {
        test: /\.(woff2?|eot|ttf|otf|wav)(\?.*)?$/,
        loader: 'file-loader',
        exclude: /(node_modules)/
      },
      {
        test: /\.(png|jpe?g|gif|svg|)$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[name].[ext]', // dev === "dev" ? "[name].[ext]" : "[name].[hash].[ext]" for cache in prod
              outputPath: 'images'
            }
          }, {
            loader: "image-webpack-loader",
            options: {
              disable: dev === "development"
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: '[name].css', // dev === "development" ? '[name].css' : '[name].[hash].css' for cache in prod
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    })
  ],
};

if (dev === "production") {
  config.plugins.push(new MinifyPlugin({removeConsole: true}, {comments: false}));
  /* for cache purpose
  config.plugins.push(new ManifestPlugin());
  config.plugins.push(new CleanWebpackPlugin({
    root: path.resolve('./'),
    verbose: true,
    dry: true
  }));
   */
}

module.exports = config;
