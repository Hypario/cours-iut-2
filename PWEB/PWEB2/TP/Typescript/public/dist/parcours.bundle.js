/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ts/parcours.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/ts/parcours.ts":
/*!****************************!*\
  !*** ./src/ts/parcours.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("let node;\nif ('undefined' == typeof Node) {\n    node = { ELEMENT_NODE: 1, TEXT_NODE: 3 };\n}\nfunction getInnerText(element) {\n    let result = '';\n    if (node.TEXT_NODE == element.nodeType) {\n        return node.nodeValue;\n    }\n    if (node.ELEMENT_NODE !== element.nodeType) {\n        return '';\n    }\n    for (let i = 0; i < element.childNodes.length; i++) {\n        result += getInnerText(element.childNodes.item(i));\n    }\n}\nfunction parcours() {\n    let text = getInnerText(document.getElementById(\"fragmentRoot\"));\n    let results = document.getElementById(\"results\");\n    if (results.hasChildNodes()) {\n        results.firstChild.nodeValue = text;\n    }\n    else {\n        let txt = document.createTextNode(text);\n        results.appendChild(txt);\n    }\n    results.style.display = \"\";\n}\nfunction initButton() {\n    console.log(\"Configuration du bouton afficher ...\");\n    let btn = document.getElementById('afficher');\n    btn.addEventListener('click', parcours);\n}\ninitButton();\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvdHMvcGFyY291cnMudHMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvdHMvcGFyY291cnMudHM/Y2RmZiJdLCJzb3VyY2VzQ29udGVudCI6WyJsZXQgbm9kZTtcbmlmICgndW5kZWZpbmVkJyA9PSB0eXBlb2YgTm9kZSkge1xuICAgIG5vZGUgPSB7IEVMRU1FTlRfTk9ERTogMSwgVEVYVF9OT0RFOiAzIH07XG59XG5mdW5jdGlvbiBnZXRJbm5lclRleHQoZWxlbWVudCkge1xuICAgIGxldCByZXN1bHQgPSAnJztcbiAgICBpZiAobm9kZS5URVhUX05PREUgPT0gZWxlbWVudC5ub2RlVHlwZSkge1xuICAgICAgICByZXR1cm4gbm9kZS5ub2RlVmFsdWU7XG4gICAgfVxuICAgIGlmIChub2RlLkVMRU1FTlRfTk9ERSAhPT0gZWxlbWVudC5ub2RlVHlwZSkge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgfVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZWxlbWVudC5jaGlsZE5vZGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHJlc3VsdCArPSBnZXRJbm5lclRleHQoZWxlbWVudC5jaGlsZE5vZGVzLml0ZW0oaSkpO1xuICAgIH1cbn1cbmZ1bmN0aW9uIHBhcmNvdXJzKCkge1xuICAgIGxldCB0ZXh0ID0gZ2V0SW5uZXJUZXh0KGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZnJhZ21lbnRSb290XCIpKTtcbiAgICBsZXQgcmVzdWx0cyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmVzdWx0c1wiKTtcbiAgICBpZiAocmVzdWx0cy5oYXNDaGlsZE5vZGVzKCkpIHtcbiAgICAgICAgcmVzdWx0cy5maXJzdENoaWxkLm5vZGVWYWx1ZSA9IHRleHQ7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBsZXQgdHh0ID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodGV4dCk7XG4gICAgICAgIHJlc3VsdHMuYXBwZW5kQ2hpbGQodHh0KTtcbiAgICB9XG4gICAgcmVzdWx0cy5zdHlsZS5kaXNwbGF5ID0gXCJcIjtcbn1cbmZ1bmN0aW9uIGluaXRCdXR0b24oKSB7XG4gICAgY29uc29sZS5sb2coXCJDb25maWd1cmF0aW9uIGR1IGJvdXRvbiBhZmZpY2hlciAuLi5cIik7XG4gICAgbGV0IGJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhZmZpY2hlcicpO1xuICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHBhcmNvdXJzKTtcbn1cbmluaXRCdXR0b24oKTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/ts/parcours.ts\n");

/***/ })

/******/ });