const MAX: number = 100;

function creerTableau(nbElements: number): number[] {
    let tab = [];
    for (let i = 0; i < nbElements; i++) {
        tab.push(Math.floor(Math.random() * Math.floor(MAX)));
    }
    return tab;
}

console.log("==Création d'un tableau d'entiers : ");
let tab = creerTableau(10);
console.log(tab);

function rechercheClassique(tab: number[], element: number): boolean {
    for (let value of tab) {
        if (value == element) {
            return true;
        }
    }
    return false;
}

function recherche(element): boolean {
    return this == element;
}

console.log("========= Test si un élément appartient au tableau =========");
console.log(tab[7]);
console.log(rechercheClassique(tab, tab[7])); // methode classique
console.log(tab.some(recherche, tab[7]));   // utilisation de some

console.log("========= Utilisation de some et any =========");
console.log(tab.every((element) => element % 2 === 0));
console.log(tab.some((element) => element % 2 === 0));

console.log("========= Test tous les éléments sont triés ? avec every =========");
let sorted = function (value: number, index: number, array: Array<number>) {
    if (index < array.length - 1 && array.length > 1) {
        return value < array[index + 1]
    }
    return true;
};
console.log("Avant le tri : ", tab.every(sorted));
tab.sort((a, b) => a - b);
console.log("Après le tri : ", tab.every(sorted));

console.log("========= tous les éléments pair =========");
console.log(tab.filter((value) => value % 2 === 0));
console.log("========= tous les éléments inférieur à une valeur =========");
// ici c'est inférieur à 30
console.log(tab.filter((value) => value < 30));


console.log("========= tab * 2 =========");
console.log(tab.map((value) => value * 2));

console.log("======== moyenne =========");
console.log(tab.reduce((somme, current) => somme + current) / tab.length);

console.log("======== moyenne coefficienté =========");
let sum = 0;
let sumC = 0;
let tab2: Array<Array<number>> = [[1, 10], [2, 15], [1, 5]];
tab2.forEach((value) => {
    sum += value[1] * value[0];
    sumC += value[0];
});
console.log(sum);
if (sumC > 0) {
    console.log(sum / sumC)
}
