let expr: string = "";
let sqrt: boolean = false;

function modeCalculatrice(scientifique: boolean = false) {
    buildDOM();
    addEvents();
    let science = document.getElementById("scientifique");
    let sciBtn = (document.getElementById("modeScientifique") as HTMLInputElement);
    let classicBtn = (document.getElementById("modeClassique") as HTMLInputElement);
    if (scientifique) {
        let calc = document.getElementById('calculatrice');
        calc.style.width = '310px';

        sciBtn.setAttribute('disabled', "true");
        classicBtn.removeAttribute('disabled')
    } else {
        science.style.display = "none";

        classicBtn.setAttribute('disabled', "true");
        sciBtn.removeAttribute('disabled')
    }
}

function buildDOM() {
    let dom = document.getElementById('calc');
    dom.innerHTML = `<div id="calculatrice">
    <div id="affichage">
        <input class="element" type='text' id='resultat'/>
    </div>
    <div id="scientifique">
      <div class="ligne">     
        <input class="element" type='button' value='\u221a'>
        <input class="element" type='button' value='log'>
        <input class="element" type='button' value='x²'>
        <input class="element" type='button' value='x!'>
        <input class="element" type='button' value='...'>
      </div> 
  </div>
    <div id="classique">
        <div class="ligne">
            <input class="element" type='button' value='7'>
            <input class="element" type='button' value='8'>
            <input class="element" type='button' value='9'>
            <input class="element" type='button' value='/'>
        </div>
        <div class="ligne">
            <input class="element" type='button' value='4'>
            <input class="element" type='button' value='5'>
            <input class="element" type='button' value='6'>
            <input class="element" type='button' value='*'>
        </div>
        <div class="ligne">
            <input class="element" type='button' value='1'>
            <input class="element" type='button' value='2'>
            <input class="element" type='button' value='3'>
            <input class="element" type='button' value='-'>
        </div>
        <div class="ligne">
            <input class="element" type='button' value='0'>
            <input class="element" type='button' value='.'>
            <input class="element" type='button' value='+'>
        </div>
        <div class="ligne">
            <input class="element" type='button' value='='>
        </div>
    </div>
</div>`;
    return dom;
}

function initialisation(): void {
    let classicBtn = document.getElementById('modeClassique') as HTMLInputElement;
    classicBtn.addEventListener('click', () => {
        modeCalculatrice();
    });

    let ScienceBtn = document.getElementById('modeScientifique') as HTMLInputElement;
    ScienceBtn.addEventListener('click', () => {
        modeCalculatrice(true);
    });
}

function addEvents() {
    Array.from(document.querySelectorAll('input[value]')).filter((input: HTMLInputElement) => {
        return (parseInt(input.value) >= 0 && parseInt(input.value) <= 9) || input.value === "+" || input.value === "-" || input.value === "/" || input.value === "*";
    }).forEach((input: HTMLElement) => {
        input.addEventListener("click", (e) => {
            e.preventDefault();
            let screen = document.getElementById("resultat") as HTMLInputElement;
            expr += (e.target as HTMLInputElement).value.toString();
            screen.value = expr
        })
    });

    document.querySelector('input[value="√"]').addEventListener("click", (e) => {
        e.preventDefault();
        let screen = document.getElementById("resultat") as HTMLInputElement;
        expr += !sqrt ? "√(" : ")";
        sqrt = !sqrt;
        screen.value = expr;
    });

    document.querySelector('input[value="="]').addEventListener("click", (e) => {
        e.preventDefault();
        let calc = expr.replace("√", "Math.sqrt");
        let screen = document.getElementById("resultat") as HTMLInputElement;
        screen.value = eval(calc);
        expr = "";
    });
}

initialisation();
