let originalNode: HTMLDivElement; // div original
let initialText; // text initial

let oldSearch: string; // ancienne recherche
let index = 0; // index courant de la liste d'index du mot cherché

function mettreEnGras(): void {
    originalNode.style.fontWeight = 'bold';
}

function colorize(e): void {
    originalNode.style.color = (e.target as HTMLInputElement).value;
}

function highlight(index: number): string {
    let text: string[] = initialText.split(" ");

    text[index] = text[index].replace(new RegExp(".*"),`<span style='background-color:yellow'>${text[index]}</span>`);

    return text.join(" ");
}

function search(motif: string): Array<number> {
    let text = initialText.split(" ");
    let indexes = [];
    // search motif
    for (let i = 0; i < text.length; i++) {
        if (text[i].search(new RegExp(motif, 'i')) > -1) {
            indexes.push(i);
        }
    }
    return indexes;
}

function next(): void {
    let motif = (document.getElementById('motif') as HTMLInputElement).value;
    let indexes = search(motif);
    if (motif === oldSearch) {
        index = index >= indexes.length ? 0 : index + 1;
    } else {
        index = 0;
        oldSearch = motif;
    }
    format(highlight(indexes[index]));
}

function prev(): void {
    let motif = (document.getElementById('motif') as HTMLInputElement).value;
    let indexes = search(motif);
    if (motif === oldSearch) {
        index = index <= 0 ? indexes.length - 1 : index - 1;
    } else {
        index = indexes.length;
    }
    format(highlight(indexes[index]));
}

function setCommandes(): void {
    let btnGras: HTMLInputElement = document.getElementById('gras') as HTMLInputElement;
    btnGras.addEventListener("click", mettreEnGras);

    let btnColor: HTMLInputElement = document.getElementById("couleur") as HTMLInputElement;
    btnColor.addEventListener("change", colorize);

    let btnPrev: HTMLButtonElement = document.getElementById('arriere') as HTMLButtonElement;
    let btnNext: HTMLButtonElement = document.getElementById('avant') as HTMLButtonElement;

    btnPrev.addEventListener('click', () => prev());
    btnNext.addEventListener('click', () => next());
}

function format(element: HTMLElement|string = originalNode): void {
    let expr = /(Article)(\s\w+)?/g;
    if (element instanceof HTMLElement) {
        element.innerHTML = element.innerHTML.replace(expr, '<h3>$1 $2</h3>');
    } else {
        originalNode.innerHTML = element.replace(expr, '<h3>$1 $2</h3>')
    }
}

originalNode = document.getElementById('original') as HTMLDivElement;
initialText = originalNode.innerHTML;
setCommandes();
format();
