export class Person {
    public id: number;
    public firstName: string;
    public lastName: string;

    static counter: number = 0;

    constructor(firstName: string, lastName: string) {
        this.id = Person.counter++;
        this.firstName = firstName;
        this.lastName = lastName
    }

    toString(): string {
        return this.firstName + " " + this.lastName;
    }
}

export class Movie {
    public id: number;
    public title: string;
    public year: number;
    public audience: number;

    public actors: Array<Actor> = [];

    public static counter = 0;

    constructor(title: string, year: number, audience: number) {
        this.id = Movie.counter++;
        this.title = title;
        this.year = year;
        this.audience = audience;
    }

    toString(complete: boolean = false): string {
        let string = "film " + this.id + ", " + this.title + " sorti en " + this.year + " a pour audience : " + this.audience;
        if (complete) {
            string += " est a pour  acteur";
            for (let actor of this.actors) {
                string += " " + actor.firstName + " " + actor.lastName
            }
        }
        return string
    }

    toHTML(complete: boolean = false) {
        let dom = document.createElement('li');
        dom.innerText = `${this.title} est sorti en ${this.year}, a pour audience ${this.audience}`;
        if (complete) {
            let ul = document.createElement('ul');
            for (let actor of this.actors) {
                let li = document.createElement('li');
                li.innerText = `${actor.firstName} ${actor.lastName}`;
                ul.appendChild(li);
            }
            dom.appendChild(ul);
        }
        return dom;
    }

    addActor(actor: Actor) {
        this.actors.push(actor);
    }
}

export class Actor extends Person {
    public movies: Array<Movie> = [];

    addMovie(movie: Movie) {
        this.movies.push(movie);
    }

    toString(complete: boolean = false): string {
        let string = super.toString();
        if (complete) {
            this.movies.forEach((movie) => {
                string += "\n Il apparaît dans le " + movie.toString();
            });
        }
        return string;
    }

    toHTML(complete: boolean = false) {
        let dom = document.createElement('li');
        dom.innerText = `${this.firstName} ${this.lastName}`;
        if (complete) {
            dom.innerText += " a joué dans :";
            let ul = document.createElement('ul');
            for (let movie of this.movies) {
                let li = document.createElement('li');
                li.innerText = `${movie.title} sortie en ${movie.year}`;
                ul.appendChild(li);
            }
            dom.appendChild(ul);
        }
        return dom;
    }
}
