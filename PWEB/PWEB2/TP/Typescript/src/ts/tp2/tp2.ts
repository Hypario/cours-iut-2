import {myMovies, myPersons} from "./data";
import {Person, Movie, Actor} from "./cinema";

let persons: Array<Person> = [];
let movies: Array<Movie> = [];
let actors: Array<Actor> = [];

myPersons.forEach((person) => {
    persons.push(new Person(person.firstName, person.lastName));
    actors.push(new Actor(person.firstName, person.lastName));
});

myMovies.forEach((movie) => {
    let current = new Movie(movie.title, movie.year, movie.audience);
    movie.actors.forEach(actor => {
        // actor est un id, ils sont triés par id
        // on rajoute l'acteur au film
        current.addActor(actors[actor]);
        // on rajoute le film à l'acteurs
        actors[actor].addMovie(current)
    });
    movies.push(current)
});

// on affiche la liste des films
let listMovie = document.createElement('h1');
listMovie.innerText = "Liste des films : ";
document.body.appendChild(listMovie);

let ul = document.createElement('ul');
movies.forEach((movie) => {
    ul.appendChild(movie.toHTML(true));
});
document.body.appendChild(ul);

// on affiche la liste des acteurs
let listActor = document.createElement('h1');
listActor.innerText = "Liste des acteurs :";
document.body.appendChild(listActor);

let ul2 = document.createElement('ul');
actors.forEach((actor) => {
    ul2.appendChild(actor.toHTML(true));
});
document.body.appendChild(ul2);
