import helper  from './helper'

document.title = "Calculatrice";

let element = helper.createElement("div", {id: 'content'},
    helper.createElement("input", {type: "text", name: "zone", id: "affichage", value: ""}),
    helper.createElement("input", {type: "button", value: "1" }),
    helper.createElement("input", {type: "button", value: "2"}),
    helper.createElement("input", {type: "button", value: "3"}),
    helper.createElement("input", {type: "button", value: "4"}),
    helper.createElement("input", {type: "button", value: "5"}),
    helper.createElement("input", {type: "button", value: "6"}),
    helper.createElement("input", {type: "button", value: "7"}),
    helper.createElement("input", {type: "button", value: "8"}),
    helper.createElement("input", {type: "button", value: "9"}),
    helper.createElement("input", {type: "button", value: "+"}),
    helper.createElement("input", {type: "button", value: "√"}),
    helper.createElement("input", {type: "button", value: "="}),
);

helper.render(element, document.body);

let screen = document.getElementById("affichage") as HTMLInputElement;
let expr: string = "";
let sqrt = false;

Array.from(document.querySelectorAll('input[value]')).filter((input: HTMLInputElement) => {
    return (parseInt(input.value) >= 1 && parseInt(input.value) <= 9) || input.value == "+";
}).forEach((input: HTMLElement) => {
    input.addEventListener("click", (e) => {
        e.preventDefault();
        expr += (e.target as HTMLInputElement).value.toString();
        screen.value = expr;
    })
});

document.querySelector('input[value="√"]').addEventListener("click", (e) => {
    e.preventDefault();
    expr += !sqrt ? "√(" : ")";
    sqrt = !sqrt;
    screen.value = expr;
});

document.querySelector('input[value="="]').addEventListener("click", (e) => {
    e.preventDefault();
    let result = expr.replace("√", "Math.sqrt");
    screen.value = eval(result);
    expr = "";
});
