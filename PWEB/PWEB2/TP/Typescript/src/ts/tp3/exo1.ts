// on change le title
import helper from "./helper";

document.title = "surface";

// on génère le html
let container: HTMLElement = document.body;
let element = helper.createElement('div', null,
    helper.createElement('h1', null, "Calculer la surface d'un rectangle :"),
    helper.createElement('p', null,
        "Cliquez sur le bouton pour lancer la fonction : ",
        helper.createElement('button', {id: "calculate"}, "Calculer")
    ),
    helper.createElement('div', {id: "resultat"})
);
helper.render(element, container);

document.getElementById('calculate').addEventListener('click', () => {
    let width: number = parseFloat(prompt("Rectangle de quel longueur ?"));
    let height: number = parseFloat(prompt("Rectangle de quel largeur ?"));

    let result = document.getElementById('resultat');

    result.innerText = `La surface est de : ${width * height}`
});
