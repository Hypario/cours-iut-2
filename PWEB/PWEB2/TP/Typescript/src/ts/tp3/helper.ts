function createElement(type: string, props: object, ...children) {
    return {
        type,
        props: {
            ...props,
            children: children.map(child => {
                return typeof child === 'object' ? child : createTextElement(child)
            })
        }
    }
}

function createTextElement(text) {
    return {
        type: 'TEXT_ELEMENT',
        props: {
            nodeValue: text,
            children: []
        }
    }
}

function render(element, container) {

    const dom =
        element.type === "TEXT_ELEMENT"
            ? document.createTextNode(element.props.nodeValue)
            : document.createElement(element.type);

    Object.keys(element.props).forEach(name => {
        if (name !== 'children') {
            dom[name] = element.props[name]
        }
    });

    element.props.children.forEach(child => {
        render(child, dom)
    });

    container.appendChild(dom)
}

export default {
    createElement,
    render
}
