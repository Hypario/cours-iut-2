import helper from './helper'

document.title = "Exemple timer simple";

let element = helper.createElement('div', null,
    helper.createElement('h2', {id: "message"}, "Cliquer sur le bouton pour démarrer le compte à rebours"),
    helper.createElement('input', {id: "toggle", type: "button", value: "Démarrer"})
);

helper.render(element, document.body);

let timer;
let toggle: boolean = false;

let button = document.getElementById('toggle');
button.addEventListener('click', () => {
    let message = document.getElementById('message');
    message.innerText = "Le compte à rebours démarre";
    if (!toggle) {
        timer = setTimeout(() => {
            message.innerText = ">> Le compte à rebours s'arrête";
            button.setAttribute('value', "Démarrer");
            toggle = false;
        }, 3000);
        button.setAttribute('value', "Arrêter");
        toggle = true;
    } else {
        clearTimeout(timer);
        message.innerText = "Cliquer sur le bouton pour démarrer le compte à rebours";
        button.setAttribute('value', "Démarrer");
        toggle = false;
    }
});
