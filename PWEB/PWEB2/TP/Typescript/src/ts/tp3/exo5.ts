let node;

if (node === undefined) {
    node = {ELEMENT_NODE: 1, TEXT_NODE: 3}
}

function getInnerText(element) {
    if (node.TEXT_NODE == element.nodeType) {
        return element.nodeValue;
    }

    if (node.ELEMENT_NODE !== element.nodeType) {
        return '';
    }

    let result = '';

    for (let i = 0; i < element.childNodes.length; i++) {
        result += getInnerText(element.childNodes.item(i));
    }

    return result;
}

function exo5() {
    let text = getInnerText(document.getElementById("fragmentRoot"));
    let results = document.getElementById("results");
    if (results.hasChildNodes()) {
        results.firstChild.nodeValue = text;
    } else {
        let txt = document.createTextNode(text);
        results.appendChild(txt);
    }
    let br = document.createElement('br');
    let h1 = document.createElement('h1');
    h1.innerText = "Un gros titre ajouté";
    let nodeRemarque = document.createTextNode("mais le contenu du bouton ne s'affiche pas .... pourquoi ?");
    results.appendChild(h1);
    results.appendChild(br.cloneNode());
    results.appendChild(nodeRemarque.cloneNode());
    results.appendChild(br.cloneNode());
    results.appendChild(nodeRemarque.cloneNode());
    results.appendChild(br.cloneNode());
    results.appendChild(nodeRemarque.cloneNode());

    results.style.display = ""
}

function initButton() {
    console.log("Configuration du bouton afficher ...");
    let btn = document.getElementById('afficher');
    btn.addEventListener('click', exo5);
}

initButton();
