import {from, fromEvent, Observable, Observer, range} from "rxjs";
import {filter, reduce} from "rxjs/operators";
import {myPersons} from "./data";

export interface Enonce {
    hello(nom: string): void;
}

export class TPObservable implements Enonce {

    private doc: Document;

    constructor(doc: Document) {
        this.doc = doc;
    }

    hello(nom: string): void {
        console.log(`Bienvenue ${nom}`)
    } ;

    creerHTMLElement(type: string, contenu: string | HTMLElement = ""): HTMLElement {
        const dom = document.createElement(type);

        if (contenu instanceof HTMLElement)
            dom.appendChild(contenu);
        else
            dom.innerText = contenu;

        return dom
    }

    addElementToBloc(elt: HTMLElement, bloc: HTMLElement): void {
        bloc.appendChild(elt);
    }

    creerDivWithId(id: string): HTMLDivElement {
        let div = document.createElement("div");
        div.id = id;

        let child = document.createElement("h3");
        child.innerText = id;

        div.appendChild(child);

        return div;
    }

    sourisVerte(conteneur: HTMLElement): HTMLElement {

        let source$ = from([
            "Une souris verte",
            "Qui courait dans l’herbe.",
            "Je l’attrape par la queue,",
            "Je la montre à ces messieurs.",
            "Ces messieurs me disent :",
            "Trempez là dans l’huile,",
            "Trempez là dans l’eau,",
            "Ça fera un escargot tout chaud.",
            "Je la mets dans mon chapeau",
            "Elle me dit qu’il fait trop chaud.",
            "Je la mets dans un tiroir",
            "Elle me dit qu’il fait trop noir.",
            "Je la mets dans ma culotte",
            "Elle me fait trois petites crottes.",
        ]);

        source$.subscribe((str: string) => {
                let li = this.creerHTMLElement('li', str);
                this.addElementToBloc(li, conteneur);
            }, (err) => console.log(err.message),
            () => console.log("Fin de la chanson")
        );
        return conteneur;
    }

    /* la fonction ne fais pas de console.log
    car je m'en sert pour les autres fonctions
     */
    rangeValeurs(lb: number, hb: number) {
        return range(lb, hb).subscribe(console.log)
    }

    rangeValeursPaires(lb: number, hb: number) {
        range(lb, hb).pipe(
            filter(x => x % 2 === 0),
        ).subscribe(console.log);
    }

    rangeValeursImpairesPlus(lb: number, hb: number) {
        range(lb, hb).pipe(
            filter(x => x % 2 === 1)
        ).subscribe(console.log);
    }

    fibonacci(n: number) {
        let a = 0;
        let b = 1;
        let tmp;
        new Observable((observer: Observer<number>) => {
            observer.next(a);
            observer.next(b);
            for (let i = 0; i < n - 1; i++) {
                observer.next(a + b);
                tmp = a;
                a = b;
                b = b + tmp;
            }
        }).subscribe(console.log)
    }

    moyenne(notes: number[]) {
        from(notes).pipe(
            reduce((acc, current) => acc + current)
        ).subscribe(x => console.log(x / notes.length))
    }

    zoneDessin(id: string, largeur: number, hauteur: number, style: string): HTMLElement {
        let div = this.creerDivWithId(id);
        let canvas: HTMLCanvasElement = document.createElement("canvas");
        canvas.height = hauteur;
        canvas.width = largeur;
        canvas.innerText = "Votre navigateur ne connait pas les balises canvas (html 5)";
        canvas.getContext("2d").fillStyle = style;

        div.appendChild(canvas);
        return div;
    }

    progress(canvas: HTMLCanvasElement) {
        return new Observable((observer: Observer<any>) => {
            let current = 0;
            let ctx = canvas.getContext("2d");
            ctx.fillStyle = 'blue';
            setInterval(() => {
                if (current < canvas.width)
                    ctx.fillRect(0, 0, current, 10);
                current += canvas.width / 100
            }, 50)
        })
    }

    exo4(id: string, idSearch: string, idResult: string): HTMLElement {
        let div = this.creerDivWithId(id);

        let input = document.createElement("input");
        input.type = "text";
        input.id = idSearch;
        input.placeholder = "Recherche";

        let ul = document.createElement("ul");
        ul.id = idResult;

        div.appendChild(input);
        div.appendChild(ul);

        return div;
    }

    recherche(search: HTMLInputElement, results: HTMLElement) {
        let look = function () {
            results.innerHTML = "";
            myPersons.map((personne) => {
                if (
                    personne.nom === search.value ||
                    personne.prenom === search.value ||
                    personne.prenom + " " + personne.nom === search.value ||
                    personne.nom + " " + personne.prenom === search.value ||
                    search.value.length === 0
                ) {
                    let li = document.createElement("li");
                    li.innerText = `${personne.nom} ${personne.prenom}`;
                    results.appendChild(li)
                }
            })
        };

        look();
        fromEvent(search, "keyup").subscribe(look)
    }

}

export default TPObservable;
