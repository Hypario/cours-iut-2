import TPObservable from "./TPObservable";

console.log('Bonjour, Trace de l\'exécution du fichier index.ts');

const tpObservable = new TPObservable(document);

function exos(): void {
    console.log(`Début d'exécution à ${new Date()}`);

    tpObservable.hello("Fabien");

    let answers = document.getElementById("answers");

    let div = tpObservable.creerDivWithId("Exo1");
    div.appendChild(tpObservable.creerHTMLElement("h2", "Robert Duchmol"));
    answers.appendChild(div);

    tpObservable.sourisVerte(document.getElementById("chanson"));

    tpObservable.rangeValeurs(1, 5);
    tpObservable.rangeValeursPaires(1, 5);
    tpObservable.rangeValeursImpairesPlus(1, 5);

    tpObservable.fibonacci(5);
    tpObservable.moyenne([12, 15, 12, 20, 11]);

    document.body.appendChild(tpObservable.zoneDessin("exo3", 100, 10, "1px solid #d3d3d3"));
    let canvas: HTMLCanvasElement = document.querySelector("#exo3 canvas");
    tpObservable.progress(canvas).subscribe();

    document.body.appendChild(tpObservable.exo4("exo4", "search", "results"));

    tpObservable.recherche(document.getElementById("search") as HTMLInputElement, document.getElementById("results"));

    console.log(`Fin d'exécution à ${new Date()}`);
}

exos();
