export class FullName {
  firstName: string;
  lastName: string;
  score: number;

  constructor(firstName: string, lastName: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.score = 0;
  }

  getName(): string {
    return `${this.firstName} ${this.lastName}`;
  }
}
