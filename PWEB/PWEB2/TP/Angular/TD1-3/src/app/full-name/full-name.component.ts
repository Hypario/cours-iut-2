import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FullName} from './full-name.model';

@Component({
  selector: 'app-full-name',
  templateUrl: './full-name.component.html',
  styleUrls: ['./full-name.component.scss']
})
export class FullNameComponent implements OnInit {

  @Input() fullNames: Array<FullName>;

  upvote(fullName: FullName): boolean {
    fullName.score += 1;
    return false;
  }

  downvote(fullName: FullName): boolean {
    fullName.score -= 1;
    return false;
  }

  ngOnInit() {
  }

}
