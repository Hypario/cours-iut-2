import {Component} from '@angular/core';
import {FullName} from './full-name/full-name.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  fullNames: Array<FullName> = [];

  addName(firstName: HTMLInputElement, lastName: HTMLInputElement): boolean {
    this.fullNames.push(new FullName(firstName.value, lastName.value));
    return false;
  }

  sortedNames() {
    return this.fullNames.sort((a: FullName, b: FullName) => {
      if (a.lastName > b.lastName) {
        return 1;
      } else if (a.lastName === b.lastName) {
        return a.firstName >= b.firstName ? 1 : -1;
      }
      return -1;
    });
  }

  sortedScores() {
    return this.fullNames.sort((a: FullName, b: FullName) => {
      if (a.score > b.score) {
        return -1;
      } else if (a.score === b.score) {
        // sort by name
        if (a.lastName > b.lastName) {
          return 1;
        } else if (a.lastName === b.lastName) {
          return a.firstName >= b.firstName ? 1 : -1;
        }
        return -1;
      }
      return 1;
    });
  }

}
