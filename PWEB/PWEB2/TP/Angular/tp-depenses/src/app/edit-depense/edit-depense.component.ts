import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonnesService} from '../personnes.service';
import {Depense} from '../datas.model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-edit-depense',
  templateUrl: './edit-depense.component.html',
  styleUrls: ['./edit-depense.component.css']
})
export class EditDepenseComponent implements OnInit {

  idP: number;
  depense: Depense;

  constructor(private route: ActivatedRoute, private service: PersonnesService) {
  }

  ngOnInit() {
    this.idP = +this.route.snapshot.paramMap.get('idP');
    const idD = +this.route.snapshot.paramMap.get('idD');

    this.service.getPersonne(this.idP).subscribe(rep => {
      this.depense = rep.getDepenses().find((current) => {
        return current.ident === idD;
      });
    });
  }

}
