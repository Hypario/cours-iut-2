import {Component, Input, OnInit} from '@angular/core';
import {Depense} from '../datas.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonnesService} from '../personnes.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-form-depense',
  templateUrl: './form-depense.component.html',
  styleUrls: ['./form-depense.component.css']
})
export class FormDepenseComponent implements OnInit {

  @Input() idP: number;
  @Input() depense: Depense;
  @Input() doUpdate = false;

  form: FormGroup;
  natures = ['Alimentaire', 'Loisirs', 'Voiture', 'Habitat', 'Sport', 'Vacances'];

  constructor(private route: ActivatedRoute, private service: PersonnesService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      nature: new FormControl(this.depense.nature ? this.depense.nature : null),
      libelle: new FormControl(this.depense.libelle ? this.depense.libelle : null, [Validators.required]),
      date: new FormControl(this.depense.date ? this.depense.date : null, [Validators.required]),
      montant: new FormControl(this.depense.montant ? this.depense.montant : null, [Validators.required, Validators.min(0)])
    });
  }

  get nature() {
    return this.form.get('nature');
  }

  get libelle() {
    return this.form.get('libelle');
  }

  get date() {
    return this.form.get('date');
  }

  get montant() {
    return this.form.get('montant');
  }

  onSubmit() {
    this.depense.nature = this.form.get('nature').value;
    this.depense.libelle = this.form.get('libelle').value;
    this.depense.date = new Date(this.form.get('date').value);
    this.depense.montant = this.form.get('montant').value;

    if (this.doUpdate) {
      this.service.updateDepense(this.idP, this.depense);
    }
  }

}
