import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonnesService} from '../personnes.service';
import {Depense, Personne} from '../datas.model';

@Component({
  selector: 'app-details-personne',
  templateUrl: './details-personne.component.html',
  styleUrls: ['./details-personne.component.css']
})
export class DetailsPersonneComponent implements OnInit {
  loading: boolean = false;
  personne: Personne;


  constructor(private route: ActivatedRoute,
              private service: PersonnesService) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.loading = true;
    this.service.getPersonne(id).subscribe(rep => {
      console.log(rep);
      this.personne = rep;
      console.log(`Dépenses globales = ${this.personne.montantDepenses}`);
      this.loading = false;
    });
  }

  edit(depense: Depense) {
    // TODO modifie une dépense
    console.log('Edit : ', depense);
  }

  suppression(depense: Depense) {
    // TODO supprime une dépense
    console.log('Suppression : ', depense);
  }

  ajoute() {
    // TODO ajoute une dépense
    console.log('Crée une nouvelle dépense');
  }
}
