import {Injectable} from '@angular/core';
import {MessageService} from './message.service';
import {Personne} from './datas-model';
import {Data} from './mock-datas';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  personnes: Personne[] = Data.getInstance().datas;

  constructor(private messagesService: MessageService) {
  }

  getPersonnes(): Personne[] {
    return this.personnes;
  }

  getPersonne(id): Personne {
    this.messagesService.add(`getPersonne id = ${id}`);

    return this.personnes[id - 1];
  }
}
