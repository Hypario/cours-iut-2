import {Component, OnInit} from '@angular/core';
import {PersonneService} from '../personne.service';
import {Personne} from '../datas-model';

@Component({
  selector: 'app-liste-personnes',
  templateUrl: './liste-personnes.component.html',
  styleUrls: ['./liste-personnes.component.css']
})
export class ListePersonnesComponent implements OnInit {

  personnes: Personne[];

  constructor(private personneService: PersonneService) {
  }

  ngOnInit() {
    this.personnes = this.personneService.getPersonnes();
  }

}
