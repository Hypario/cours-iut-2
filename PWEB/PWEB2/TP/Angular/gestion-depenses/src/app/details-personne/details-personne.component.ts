import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonneService} from '../personne.service';
import {Personne} from '../datas-model';

@Component({
  selector: 'app-details-personne',
  templateUrl: './details-personne.component.html',
  styleUrls: ['./details-personne.component.css']
})
export class DetailsPersonneComponent implements OnInit {

  personne: Personne;

  constructor(private route: ActivatedRoute, private service: PersonneService) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.personne = this.service.getPersonne(id);
  }

}
