import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ListePersonnesComponent} from './liste-personnes/liste-personnes.component';
import {DetailsPersonneComponent} from './details-personne/details-personne.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';


const routes: Routes = [
  // todo : route vers le composant home
  {path: '', component: HomeComponent},
  {path: 'personnes', component: ListePersonnesComponent},
  {path: 'details/:id', component: DetailsPersonneComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
