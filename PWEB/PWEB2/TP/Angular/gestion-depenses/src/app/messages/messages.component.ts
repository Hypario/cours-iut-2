import {Component, OnInit} from '@angular/core';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-messages',
  template: `
    <div *ngIf="messageService.messages.length">
      <h2>Messages</h2>
      <button class=" clear" (click)="messageService.clear()">
        <mat-icon aria-hidden="false" aria-label="suppression des messages">clear</mat-icon>
      </button>
      <mat-list>
        <mat-list-item *ngFor="let message of messageService.messages">{{ message }}</mat-list-item>
      </mat-list>
    </div>
  `,
  styles: []
})
export class MessagesComponent implements OnInit {

  constructor(public messageService: MessageService) {
  }

  ngOnInit() {
  }

}
