import { Component } from '@angular/core';
import {Personne} from './datas-model';
import {Data} from './mock-datas';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gestion-depenses';
  personnes: Personne[];

  panelOpenState: boolean;

  constructor() {
    this.personnes = Data.getInstance().datas;
    console.log(this.personnes);
  }
}
