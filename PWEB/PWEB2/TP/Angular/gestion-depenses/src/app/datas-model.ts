export interface Deserializable {
  deserialize(input: any): this;
}

export class Personne implements Deserializable {
  static cpt = 1;
  private id: number;
  private nom: string;
  private prenom: string;
  private depenses: Depense[];

  /*
   constructor(nom: string, prenom: string) {
    this.id = Personne.cpt++;
    this.nom = nom;
    this.prenom = prenom;
    this.depenses = [];
  }
  */

  deserialize(input: any): this {
    const obj = Object.assign(this, input);
    obj.id = Personne.cpt++;
    obj.depenses = input.depenses.map(depense => new Depense().deserialize(depense));
    return obj;
  }

  montantDepense(): number {
    let sum = 0;
    this.depenses.forEach((current: Depense) => sum += parseFloat(current.getMontant().toString()));
    return sum;
  }

  montantNature(): Map<string, number> {
    const sum = new Map();
    this.depenses.forEach((current: Depense) => {
      const nature = current.getNature();
      const montant = parseFloat(current.getMontant().toString());
      if (sum.has(nature)) {
        sum.set(nature, sum.get(nature) + montant);
      } else {
        sum.set(nature, montant);
      }
    });
    return sum;
  }

  listeNature(): Map<string, Array<Depense>> {
    const liste = new Map();
    this.depenses.forEach((current: Depense) => {
      const nature = current.getNature();
      if (liste.has(nature)) {
        const arr: Array<Depense> = liste.get(nature);
        arr.push(current);
        liste.set(nature, arr);
      } else {
        liste.set(nature, [current]);
      }
    });
    return liste;
  }
}

export class Depense implements Deserializable {
  static cpt = 1;
  private id: number;
  private dd: Date;
  private nature: string;
  private libelle: string;
  private montant: number;

  deserialize(input: any): this {
    const obj = Object.assign(this, input);
    obj.id = Depense.cpt++;
    return obj;
  }

  getDate() {
    return this.dd;
  }

  getNature(): string {
    return this.nature;
  }

  getLibelle(): string {
    return this.libelle;
  }

  getMontant(): number {
    return this.montant;
  }
}
