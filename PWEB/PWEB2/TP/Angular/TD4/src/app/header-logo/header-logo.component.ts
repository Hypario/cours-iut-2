import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-logo',
  template: `<img class="logo" src="../../assets/img/logo.png" alt="logo">`,
  styleUrls: ['./header-logo.component.css']
})
export class HeaderLogoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
