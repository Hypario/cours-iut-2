import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieYearComponent } from './movie-year.component';

describe('MovieYearComponent', () => {
  let component: MovieYearComponent;
  let fixture: ComponentFixture<MovieYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieYearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
