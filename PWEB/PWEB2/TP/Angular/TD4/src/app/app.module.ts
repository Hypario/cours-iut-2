import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderRowComponent } from './header-row/header-row.component';
import { HeaderLogoComponent } from './header-logo/header-logo.component';
import { HeaderTitleComponent } from './header-title/header-title.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { MovieRowComponent } from './movie-row/movie-row.component';
import { MoviePosterComponent } from './movie-poster/movie-poster.component';
import { MovieTitleComponent } from './movie-title/movie-title.component';
import { MovieYearComponent } from './movie-year/movie-year.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderRowComponent,
    HeaderLogoComponent,
    HeaderTitleComponent,
    MoviesListComponent,
    MovieRowComponent,
    MoviePosterComponent,
    MovieTitleComponent,
    MovieYearComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
