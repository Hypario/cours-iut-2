import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularMaterialModule} from './shared/angular-material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from './home/home.component';
import { IllustrationUnComponent } from './illustration-un/illustration-un.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormValEmailComponent } from './form-val-email/form-val-email.component';
import { EditDepenseComponent } from './edit-depense/edit-depense.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IllustrationUnComponent,
    FormValEmailComponent,
    EditDepenseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
