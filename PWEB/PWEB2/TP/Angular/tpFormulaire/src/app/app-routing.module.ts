import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {IllustrationUnComponent} from './illustration-un/illustration-un.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'test1', component: IllustrationUnComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
