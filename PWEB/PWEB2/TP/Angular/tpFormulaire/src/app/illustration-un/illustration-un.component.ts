import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-illustration-un',
  templateUrl: './illustration-un.component.html',
  styleUrls: ['./illustration-un.component.css']
})
export class IllustrationUnComponent implements OnInit {

  editForm: FormGroup;
  specialites: string[] = [
    'Polyvalent', 'Charpentier', 'Chauffagiste', 'Couvreur', 'Electricien', 'Grutier', 'Maçon', 'Plaquiste', 'Plombier'
  ];

  constructor() {
  }

  onSubmit() {
    console.log(this.editForm.value);
  }

  ngOnInit(): void {
    this.editForm = new FormGroup({
      nom: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      prenom: new FormControl(null, [Validators.required]),
      tel: new FormControl(null, [Validators.required, Validators.pattern('([0-9]{2}\\.){4}[0-9]{2}')]),
      specialite: new FormControl('Polyvalent')
    });
  }

  get nom() {
    return this.editForm.get('nom');
  }

  get prenom() {
    return this.editForm.get('prenom');
  }

  get tel() {
    return this.editForm.get('tel');
  }

}
