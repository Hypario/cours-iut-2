-- =======================
-- III: FONCTIONS SQL
-- =======================

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1: Fonctions qui retournent une valeur unique
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
create or replace function f1(idT team.idTeam%type)
returns team.name%type as
$$
  select name from team where idTeam=idT;
$$ language sql;

-- 2
 select f1('GAG');

-- 3
select surname, f1(idTeam)
from competitor;

-- 4
create or replace function f2(idC competitor.idCompetitor%type)
returns bigint as
$$
  select count(*) from performance
  where idCompetitor=idC;
$$ language sql;

-- 5
select surname
from competitor
where f2(idCompetitor) < 15;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 2: Fonctions qui retournent plusieurs valeurs
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

-- 1
create or replace function f3(idT team.idTeam%type)
returns setof competitor as
$$
  select * from competitor where idTeam=idT;
$$ language sql;

-- 2
select *
from f3('GAG');

-- 3
select givenname, surname, f1(idTeam)
from f3('GAG');

-- 4
create or replace function f4(n int)
returns table(
 id competitor.idCompetitor%type,
 prenom competitor.givenname%type,
 nom competitor.surname%type) as
$$
  select idCompetitor,givenname,surname
  from competitor join performance using(idCompetitor)
  group by competitor.idCompetitor
  having count(*) >= n;
$$ language sql;

-- 5
select count(*)
from f4(15);

