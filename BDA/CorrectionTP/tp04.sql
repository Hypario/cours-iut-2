-- =======================
-- IV: FONCTIONS PL/SQL
-- =======================

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1: Structures de contrôle
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

-- 1
create or replace function estFrancais(idC competitor.idCompetitor%type)
returns boolean as
$$
begin
  perform 1 from competitor
  where idCompetitor=idC and idCountry='FRA';
  return found;
end
$$ language plpgsql;

-- 2
select *
from competitor
where estFrancais(idCompetitor);

-- 3
create or replace function listePoids(poidsTotal decimal)
returns setof competitor as
$$
declare
 p decimal =0;
 c competitor;
begin
  for c in select * from competitor order by weight loop
    --if(p + c.weight) > poidsTotal then return; end if;
    exit when (p + c.weight)> poidsTotal;
    p = p+ c.weight;
    return next c;
  end loop;
end
$$ language plpgsql;

select * from listePoids(300);

-- 4
drop function classement(int);
create or replace function classement(
  n int,
  out rank performance.rank%type,
  out idCompetitor competitor.idCompetitor%type,
  out surname competitor.surname%type,
  out givenname competitor.givenname%type,
  out duration performance.duration%type
)
returns setof record as
$$
declare
 p record;
 r int =0;
begin
  for rank,idCompetitor,surname,givenname,duration in select performance.rank,performance.idCompetitor,competitor.surname,competitor.givenname,performance.duration
           from competitor join performance using(idCompetitor)
           where idStage = n
           order by rank, surname, givenname
  loop
    if (rank = r) then
      rank=null;
    else
      r = rank;
    end if;
    return next;
  end loop;
end
$$ language plpgsql;

select * from classement(5) limit 10;

--5
create or replace function perf(idC competitor.idCompetitor%type, idS stage.idStage%type, d performance.duration%type)
returns boolean as
$$
begin
  perform 1 from competitor
  where idCompetitor=idC;
  if not found then return false; end if;
  
  perform 1 from stage
  where idStage=idS;
  if not found then return false; end if;
  
  perform 1 from performance
  where idCompetitor=idC and idStage=idS;
  if found then
    update performance
    set duration = d
    where idCompetitor=idC and idStage=idS;
  else
    if (idS >1) then
      perform 1 from performance where idCompetitor=idC and idStage=idS-1;
      if not found then return false; end if;
    end if;
    insert into performance values(null,idC,idS,d);
  end if;
  update performance p1
  set rank = (select count(*)+1 from performance p2 where p2.duration < p1.duration and p1.idStage=p2.idStage);
  return true;
end
$$ language plpgsql;

-- 6

-- select idCompetitor, max(idStage) from performance group by idCompetitor order by 2;
-- 6.1
select count(*) from performance;
-- 6.2
select perf(512,5,'06:07:00');
-- 6.3
select perf(5,512,'06:07:00');
-- 6.4
select count(*) from performance where idStage=5;
-- 6.5
select perf(8,5,'12:00:00');
select count(*) from performance where idStage=5;
-- 6.6
select * from classement(5) limit 10;
-- 6.7
select perf(97,5,'06:15:00');
select * from classement(5) limit 10;
-- 6.8
select perf(8,4,'12:00:00');
select perf(8,5,'12:00:00');
select count(*) from performance where idStage=5;