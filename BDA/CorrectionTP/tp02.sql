-- =======================
-- II: GESTION DES DONNEES
-- =======================

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1: Insertion
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
insert into competitor(...) values (...);

-- 2
create table topFive(
  idStage int,
  idCompetitor int,
  givenname varchar,
  surname varchar
);

insert into topFive
  select idStage, idCompetitor, givenname,surname
  from competitor join performance using(idCompetitor)
  where rank <= 5;

-- 3
insert into topFive
  select idStage, idCompetitor, givenname,surname
  from competitor join performance p1 using(idCompetitor)
  where rank > (select max(rank)-5 
                from performance p2 where p2.idStage=p1.idStage);

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 2: Modification
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
update competitor
set givenname = 'toto'
where idCountry = (select idCountry from country where name='France');

-- 2
update competitor
set height = height+5,
    weight = weight * 0.9
where idTeam = (select idTeam from team where name = 'Belgicom');

-- 3
update competitor
set birthday = birthday + interval '1 month'
where idTeam != (select idTeam from team where name = 'Belgicom');


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 3: Suppression
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
delete from performance
where idCompetitor=50 and idStage>8;

-- 2
delete from performance
where idCompetitor in (
  select idCompetitor
  from competitor
  where weight > 100);

delete from competitor
where weight > 100;

-- 3
delete from country
where idCountry not in (
  select idCountry from competitor
  union
  select idCountry from city
  union
  select idCountry from team)

