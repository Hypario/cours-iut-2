-- ===========
-- I: REQUETES
-- ===========

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1: Jointures
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select *
from competitor join country using(idCountry)
where country.name='France';

-- 2
select idStage, dateStage, idCompetitor, surname, name
from competitor join performance using (idCompetitor)
                join team using (idTeam)
                join stage using(idStage)
where rank=1
order by dateStage;

-- 3
select idStage, dep.name, c1.name, arr.name, c2.name, distance
from stage join city dep on stage.idStart=dep.idCity
           join city arr on stage.idEnd=arr.idCity
           join country c1 on c1.idCountry=dep.idCountry
           join country c2 on c2.idCountry=arr.idCountry
order by idStage;
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 2 : Sous requêtes
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select *
from competitor
where idCompetitor not in (
  select idCompetitor
  from performance
  where idStage =10);

-- 2
select idStage, dateStage, distance
from stage
where distance = (select max(distance) from stage);

-- 3
select count(*)
from country
where idCountry not in (select idCountry from competitor);

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 3 : Groupements
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select bloodtype, count(*)
from competitor
group by bloodtype;

-- 2
select team.name, count(*), sum(weight)
from competitor join team using(idTeam)
group by team.idTeam;

-- 3
select competitor.surname, min(rank), sum(duration),count(*)
from competitor join performance using(idCompetitor)
group by competitor.idCompetitor;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 4: Groupements et sélections
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select surname, count(*)
from competitor join performance using(idCompetitor)
where rank=1
group by competitor.idCompetitor
having count(*) > 1;

-- 2
select team.name, count(*)
from team join competitor using(idTeam)
join performance using(idCompetitor)
where idStage=(select max(idStage) from stage)
group by team.idTeam
having count(*) = 9;

-- 3
select competitor.surname, competitor.givenname, sum(duration) as temps
from competitor join performance using(idCompetitor)
group by competitor.idCompetitor
having count(*) = (select count(*) from stage)
order by temps;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 5: Jointures externes
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select name,count(idCompetitor)
from team left join competitor on team.idTeam=competitor.idTeam 
                                   and weight>100
group by team.idTeam;

-- 2
select class.*, count(idStage)
from class left join stage on class.idClass=stage.idClass and
                              distance < 600
group by class.idClass;

-- 3
select name, count(performance.idCompetitor)
from team left join competitor using(idTeam)
          left join performance on 
                     performance.idCompetitor=competitor.idCompetitor 
                     and performance.rank=1
group by team.idTeam;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 6: distinct
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select distinct country.name
from competitor join country using(idCountry);

-- 2
select count(distinct idCountry) from competitor;

-- 3
select team.name, count(distinct competitor.idCountry)
from team join competitor using(idTeam)
group by team.idTeam;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 7: Combinaisons de requêtes
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1
select idCountry from competitor
union
select idCountry from city
union
select idCountry from team;

-- 2
select bloodtype from competitor where idTeam = 'INC'
except
select bloodtype from competitor where idTeam = 'DYN';

-- 3
select idStart from stage
intersect
select idEnd from stage
order by idStart;
