SET search_path TO eurotour;

CREATE OR REPLACE FUNCTION checkTemp() returns TRIGGER AS
$$
    BEGIN
    END
$$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tg_temps ON performance;
CREATE TRIGGER tg_temps
    before insert or update
    on performance
    for each row
execute function checkTemp();
