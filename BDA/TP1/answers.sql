/**
-- 1
SELECT COUNT(*) FROM team;

-- 2
SELECT COUNT(*)
FROM competitor as c
JOIN team as t ON c.idTeam = t.idTeam
GROUP BY c.idTeam;

-- 3
SELECT COUNT(*)
FROM stage;

-- 4
SELECT COUNT(*), name 
FROM stage as s
JOIN class as c ON s.idClass = c.idClass
GROUP BY name;
-- 3.1)
-- 1
SELECT COUNT(*)
FROM competitor as c
JOIN country as co ON c.idCountry = co.idCountry
WHERE co.name = 'France';

-- 2
SELECT s.idStage, s.dateStage, c.idCompetitor, c.surname
FROM performance as p
JOIN stage as s ON p.idStage = s.idStage
JOIN competitor as c ON p.idCompetitor = c.idCompetitor
WHERE p.rank = 1
ORDER BY s.dateStage;

-- 3
SELECT idStage, c1.name as villeDep, c2.name as villeArr, co1.name as PaysDep, co2.name as PaysArr, s.distance
FROM stage as s
JOIN city as c1 ON s.idStart = c1.idCity
JOIN city as c2 ON s.idEnd = c2.idCity
JOIN country as co1 ON c1.idCountry = co1.idCountry
JOIN country as co2 ON c2.idCountry = co2.idCountry
ORDER BY idStage;

-- 3.2)
-- 1
SELECT *
FROM competitor
WHERE idCompetitor NOT IN 
    (SELECT idCompetitor FROM performance WHERE idStage = 10);

-- 2
SELECT idStage, dateStage, distance
FROM stage
WHERE distance = 
    (SELECT MAX(distance) FROM stage);

-- 3
SELECT COUNT(*)
FROM country
WHERE idCountry NOT IN 
    (SELECT idCountry 
    FROM competitor)

-- 3.3)
-- 1
SELECT bloodtype, COUNT(*)
FROM competitor
GROUP BY bloodtype;

-- 2
SELECT t.name, count(*), sum(c.weight)
FROM team as t
JOIN competitor c ON t.idTeam = c.idTeam
GROUP BY t.idTeam;

-- 3
SELECT c.givenName, c.surname, min(p.rank), sum(p.duration), count(*)
FROM competitor c
JOIN performance p ON c.idCompetitor = p.idCompetitor
GROUP BY c.givenName, c.surName

-- 3.4)
-- 1
SELECT c.givenName, c.surname
FROM competitor c
JOIN performance p ON c.idCompetitor = p.idCompetitor
WHERE rank = 1
GROUP BY c.givenName, c.surname
HAVING count(*) >= 2;

-- 2
SELECT t.name
FROM team t
JOIN competitor c ON t.idTeam = c.idTeam
JOIN performance p on c.idCompetitor = p.idCompetitor
WHERE idStage = (SELECT MAX(idStage) FROM stage)
GROUP BY t.name
HAVING count(*) = 9;

-- 3
SELECT c.givenName, c.surname, sum(duration) as time
FROM competitor c
JOIN performance p ON c.idCompetitor = p.idCompetitor
GROUP BY c.idCompetitor
HAVING count(*) = (SELECT MAX(idStage) FROM stage)
ORDER BY time ASC;

-- 3.5)
-- 1
SELECT t.name, count(c.weight)
FROM team t
LEFT JOIN competitor c ON t.idTeam = c.idTeam
AND c.weight >= 100
GROUP BY t.idTeam;

-- 2
SELECT c.name, count(s.*)
FROM class c
LEFT JOIN stage s ON c.idClass = s.idClass
AND distance < 600
GROUP BY c.idClass;

-- 3
SELECT t.name, count(rank)
FROM competitor c
LEFT JOIN performance p ON p.idCompetitor = c.idCompetitor 
AND rank = 1
JOIN team t ON t.idTeam = c.idTeam
GROUP BY t.idTeam;

-- 3.6
-- 1
SELECT DISTINCT co.name
FROM competitor c
JOIN country co ON c.idCountry = co.idCountry
GROUP BY co.idCountry; 

-- 2 
SELECT COUNT(DISTINCT idCountry) FROM competitor;

-- 3

SELECT name, COUNT(DISTINCT c.idCountry)
FROM team as t
JOIN competitor as c ON c.idTeam = t.idTeam
GROUP BY t.name;

*/

-- 3.7
-- 1