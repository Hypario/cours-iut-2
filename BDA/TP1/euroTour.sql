--
-- PostgreSQL database dump
--

DROP schema if exists eurotour cascade;
CREATE SCHEMA eurotour;
SET search_path = eurotour;

--
-- Name: city; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE city (
    idcity integer NOT NULL,
    name character varying NOT NULL,
    latitud numeric,
    longitud numeric,
    idcountry character(3)
);


--
-- Name: city_idcity_seq; Type: SEQUENCE; Schema: eurotour; Owner: -
--

CREATE SEQUENCE city_idcity_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: city_idcity_seq; Type: SEQUENCE OWNED BY; Schema: eurotour; Owner: -
--

ALTER SEQUENCE city_idcity_seq OWNED BY city.idcity;


--
-- Name: class; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE class (
    idclass integer NOT NULL,
    name character varying NOT NULL
);


--
-- Name: class_idclass_seq; Type: SEQUENCE; Schema: eurotour; Owner: -
--

CREATE SEQUENCE class_idclass_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: class_idclass_seq; Type: SEQUENCE OWNED BY; Schema: eurotour; Owner: -
--

ALTER SEQUENCE class_idclass_seq OWNED BY class.idclass;


--
-- Name: competitor; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE competitor (
    idcompetitor integer NOT NULL,
    givenname character varying(20) NOT NULL,
    surname character varying(23) NOT NULL,
    gender character varying(6) NOT NULL,
    city character varying(100) NOT NULL,
    zipcode character varying(15) NOT NULL,
    idcountry character varying(3) NOT NULL,
    email character varying(100) NOT NULL,
    birthday date NOT NULL,
    bloodtype character varying(3) NOT NULL,
    weight numeric,
    height integer,
    idteam character(3)
);


--
-- Name: competitor_idcompetitor_seq; Type: SEQUENCE; Schema: eurotour; Owner: -
--

CREATE SEQUENCE competitor_idcompetitor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: competitor_idcompetitor_seq; Type: SEQUENCE OWNED BY; Schema: eurotour; Owner: -
--

ALTER SEQUENCE competitor_idcompetitor_seq OWNED BY competitor.idcompetitor;


--
-- Name: country; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE country (
    idcountry character(3) NOT NULL,
    name text
);


--
-- Name: performance; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE performance (
    rank integer,
    idcompetitor integer NOT NULL,
    idstage integer NOT NULL,
    duration interval
);


--
-- Name: stage; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE stage (
    idstage integer NOT NULL,
    datestage date,
    idclass integer,
    distance integer,
    idstart integer,
    idend integer
);


--
-- Name: stage_idstage_seq; Type: SEQUENCE; Schema: eurotour; Owner: -
--

CREATE SEQUENCE stage_idstage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stage_idstage_seq; Type: SEQUENCE OWNED BY; Schema: eurotour; Owner: -
--

ALTER SEQUENCE stage_idstage_seq OWNED BY stage.idstage;


--
-- Name: team; Type: TABLE; Schema: eurotour; Owner: -; Tablespace: 
--

CREATE TABLE team (
    idteam text NOT NULL,
    name text,
    idcountry character(3) NOT NULL
);


--
-- Name: idcity; Type: DEFAULT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY city ALTER COLUMN idcity SET DEFAULT nextval('city_idcity_seq'::regclass);


--
-- Name: idclass; Type: DEFAULT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY class ALTER COLUMN idclass SET DEFAULT nextval('class_idclass_seq'::regclass);


--
-- Name: idcompetitor; Type: DEFAULT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY competitor ALTER COLUMN idcompetitor SET DEFAULT nextval('competitor_idcompetitor_seq'::regclass);


--
-- Name: idstage; Type: DEFAULT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY stage ALTER COLUMN idstage SET DEFAULT nextval('stage_idstage_seq'::regclass);


--
-- Data for Name: city; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY city (idcity, name, latitud, longitud, idcountry) FROM stdin;
1	Dublin	53.33306	-6.24889	IRL
2	Belfast	54.58333	-5.93333	GBR
3	Edimburg	55.95206	-3.19648	GBR
4	Oslo	59.91273	10.74609	NOR
5	Stockholm	59.33258	18.0649	SWE
6	Copenhague	55.67594	12.56553	DEN
7	Amsterdam	52.37403	4.88969	NED
8	Berlin	52.52437	13.41053	GER
9	Bruxelles	50.85045	4.34878	BEL
10	Paris	48.85341	2.3488	FRA
11	Varsovie	52.22977	21.01178	POL
12	Lisbonne	38.71667	-9.13333	POR
13	Prague	50.08804	14.42076	CZE
14	Madrid	40.4165	-3.70256	ESP
15	Budapest	47.49801	19.03991	HUN
16	Vienne	48.20849	16.37208	AUT
17	Rome	41.89474	12.4839	ITA
18	Bucarest	44.43225	26.10626	ROU
19	Barcelone	41.38879	2.15899	ESP
20	Athènes	37.97945	23.71622	GRE
21	Sofia	42.69751	23.32415	BUL
\.


--
-- Name: city_idcity_seq; Type: SEQUENCE SET; Schema: eurotour; Owner: -
--

SELECT pg_catalog.setval('city_idcity_seq', 22, false);


--
-- Data for Name: class; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY class (idclass, name) FROM stdin;
1	overbike
2	aéromob
3	multicoptère
\.


--
-- Name: class_idclass_seq; Type: SEQUENCE SET; Schema: eurotour; Owner: -
--

SELECT pg_catalog.setval('class_idclass_seq', 4, true);


--
-- Data for Name: competitor; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY competitor (idcompetitor, givenname, surname, gender, city, zipcode, idcountry, email, birthday, bloodtype, weight, height, idteam) FROM stdin;
7	Virginie	Lebrun	female	Schelten	2827	SUI	VirginieLebrun@teleworm.us	2029-05-13	A+	78.8	175	DYN
67	Marko	Fruehauf	male	OSCHENITZEN	9111	AUT	MarkoFruehauf@armyspy.com	2026-10-07	O+	75.9	171	DYN
32	Arnou	Monrency	male	Grissenberg	3266	SUI	ArnouMonrency@gustr.com	1983-06-14	B+	72.5	183	DYN
55	Karolin	Amsel	female	Aarbergen	65326	GER	KarolinAmsel@dayrep.com	2022-04-13	AB+	82.9	162	DYN
6	Didier	Coulombe	male	GARGES-LÈS-GONESSE	95140	FRA	DidierCoulombe@superrito.com	1996-02-20	O+	99.2	174	DYN
66	Tom	Burger	male	Kalenborn	53505	GER	TomBurger@gustr.com	2009-07-19	AB-	100.9	174	DYN
1	Tristan	Bouchard	male	Gippingen	5316	SUI	TristanBouchard@armyspy.com	2034-06-29	O+	93.3	180	DYN
117	Harrison	Gibbs	male	Taradale	4112	NZL	HarrisonGibbs@teleworm.us	2002-09-19	A+	105.9	181	DYN
54	Leonie	Eisenhower	female	MÖNCHEGG	8742	AUT	LeonieEisenhower@teleworm.us	1989-04-21	B+	99.4	155	DYN
20	Vedette	de Chateaub	female	ANTONY	92160	FRA	VedettedeChateaub@fleckens.hu	1985-11-12	A+	95.6	164	EXS
82	Juliane	Achen	female	ZWATZHOF	9362	AUT	JulianeAchen@gustr.com	2026-09-16	A+	101.7	166	EXS
17	Afrodille	Gervais	female	Aywaille	4920	BEL	AfrodilleGervais@gustr.com	2026-05-01	O+	91.5	164	EXS
126	Louis	Summers	male	HENLEY PARK	GU3 6DZ	GBR	LouisSummers@rhyta.com	2028-02-08	O+	81.2	178	EXS
96	Randall	Romo	male	El Bosque	15004	URU	RandallRomoResendez@jourrapide.com	2012-11-19	B+	88.7	175	EXS
108	Natalí	Coronado	female	El Dorado	90201	URU	NataliCoronadoSedillo@armyspy.com	2026-08-03	A+	95.6	160	EXS
75	Nadine	Möller	female	Vodskov	9310	DEN	NadineMoller@einrot.com	2023-12-07	O+	76.6	165	EXS
104	Arnulfo	Lozano	male	Paraje San Juan	90601	URU	ArnulfoLozanoLovato@teleworm.us	2002-04-13	O+	77.2	183	EXS
90	Ralph	Junker	male	PAIERDORF	9422	AUT	RalphJunker@dayrep.com	2019-08-22	B+	101.8	174	EXS
84	Jan	Grunwald	male	Freiburg Waltershofen	79112	GER	JanGrunwald@jourrapide.com	2011-12-23	A+	72.8	185	SIN
26	Monique	Ruais	female	LYON	69007	FRA	MoniqueRuais@gustr.com	2027-11-16	B+	62.7	154	SIN
73	Vanessa	Barth	female	København V	1639	DEN	VanessaBarth@jourrapide.com	2020-03-05	A+	82.2	158	SIN
114	Joel	Kay	male	WATTLE HILL	3237	AUS	JoelKay@einrot.com	2007-04-21	B+	99.4	173	SIN
85	Sophie	Eisenhauer	female	Waldegg	8142	SUI	SophieEisenhauer@einrot.com	2037-09-02	O+	78.2	155	SIN
80	Ulrich	Beich	male	Halden	9223	SUI	UlrichBeich@superrito.com	1973-01-11	B+	96.7	167	SIN
36	Sibyla	Mazuret	female	Dickihof	8252	SUI	SibylaMazuret@gustr.com	2015-05-09	B+	98.3	163	SIN
124	Phoebe	Leach	female	AULICH	PH17 9GS	GBR	PhoebeLeach@dayrep.com	2034-07-10	O+	96.0	158	SIN
60	Dominik	Beckenbauer	male	Bremen Neue Vahr Südwest	28329	GER	DominikBeckenbauer@dayrep.com	1985-08-16	O+	83.8	168	SIN
45	Daniela	Eisenhower	female	Frederiksberg C	1865	DEN	DanielaEisenhower@teleworm.us	1985-10-17	O+	89.5	162	GAG
63	Claudia	Fuhrmann	female	København K	1160	DEN	ClaudiaFuhrmann@superrito.com	1981-12-21	A+	85.6	152	GAG
77	Jessika	Reiniger	female	Frederikshavn	9900	DEN	JessikaReiniger@gustr.com	2028-04-04	B+	57.6	175	GAG
61	Julia	Bohm	female	ASCHBACH BEI FÜRSTENFELD	8362	AUT	JuliaBohm@dayrep.com	2026-06-30	O+	90.3	163	GAG
110	Hollie	Hodgson	female	Huntsville	35802	USA	HollieHodgson@fleckens.hu	2028-06-01	O+	60.3	169	GAG
59	Mike	Dreher	male	Bad Schandau	01814	GER	MikeDreher@jourrapide.com	1983-03-17	O+	76.1	176	GAG
47	Benjamin	Amsel	male	GERLING	5760	AUT	BenjaminAmsel@dayrep.com	2016-07-27	O+	83.7	182	GAG
89	Andreas	Sankt	male	HILPRIGEN	4873	AUT	AndreasSankt@superrito.com	1986-07-23	O+	66.0	171	GAG
57	Vanessa	Aachen	female	Bäriswil	3186	SUI	VanessaAachen@superrito.com	2025-07-31	B+	90.8	155	GAG
31	Margaux	Bernard	female	St-Pierre-de-Clages	1955	SUI	MargauxBernard@fleckens.hu	2013-04-03	B+	76.6	170	EMI
102	Suray	Badillo	female	Santa Lucía	90700	URU	SurayBadilloVela@gustr.com	2006-12-14	B+	75.0	165	EMI
112	Joel	Harvey	male	NOWERGUP	6032	AUS	JoelHarvey@gustr.com	2025-02-08	A+	107.1	182	EMI
95	Alfonsina	Avilés	female	Plácido Rosas	37101	URU	AlfonsinaAvilesAlaniz@superrito.com	1988-12-06	O+	88.7	151	EMI
98	Jair	Soliz	male	La Estanzuela	70006	URU	JairSolizBaez@rhyta.com	2034-11-21	B+	74.2	180	EMI
48	Tobias	Bachmeier	male	Schongau	6288	SUI	TobiasBachmeier@rhyta.com	1988-02-17	O+	95.0	171	EMI
81	Christine	Baumgartner	female	EMBERG	9771	AUT	ChristineBaumgartner@superrito.com	1986-07-06	A+	86.7	157	EMI
111	Jessica	Noble	female	UPTON MAGNA	SY4 6QX	GBR	JessicaNoble@cuvox.de	2015-10-08	B+	63.0	154	EMI
40	Adorlee	Laforest	female	SAINT-CHAMOND	42400	FRA	AdorleeLaforest@fleckens.hu	2022-07-24	B+	86.4	166	EMI
27	Varden	Caron	male	Heusberg	8607	SUI	VardenCaron@rhyta.com	1975-03-17	O+	78.7	166	COP
120	Maisie	Skinner	female	Poike	3112	NZL	MaisieSkinner@einrot.com	2034-09-11	A+	64.6	159	COP
113	Jasmine	Roberts	female	Martinborough	5711	NZL	JasmineRoberts@teleworm.us	2033-06-21	AB-	90.6	157	COP
18	Lorraine	Gladu	female	CHÂTEAUROUX	36000	FRA	LorraineGladu@dayrep.com	1975-09-06	A+	77.0	164	COP
118	Eleanor	Lloyd	female	ARDNAGRASK	IV4 2LW	GBR	EleanorLloyd@cuvox.de	2026-05-27	B+	71.6	164	COP
24	Amarante	Lévesque	female	Zarren	8610	BEL	AmaranteLevesque@cuvox.de	1974-01-17	A+	88.6	156	COP
9	Fitz	Desilets	male	Sulz	6284	SUI	FitzDesilets@einrot.com	2000-11-23	A+	92.5	175	COP
58	Ute	Schneider	female	Vaulruz	1627	SUI	UteSchneider@fleckens.hu	2000-03-08	A+	70.1	169	COP
78	Kevin	Schäfer	male	Brunnadern	9125	SUI	KevinSchafer@teleworm.us	2007-12-28	A+	100.8	182	COP
76	Stefanie	Reinhard	female	Muralto	6600	SUI	StefanieReinhard@jourrapide.com	2034-03-04	B+	85.2	169	LAV
51	Leon	Wexler	male	Niederroßbach	56479	GER	LeonWexler@cuvox.de	2038-07-04	O+	73.2	188	LAV
68	Antje	Baer	female	Dürrenbühl	4954	SUI	AntjeBaer@einrot.com	2031-07-27	O+	74.0	157	LAV
65	Mathias	Kaiser	male	Frederiksberg C	1966	DEN	MathiasKaiser@dayrep.com	1980-09-28	B+	98.8	183	LAV
88	Heike	Kalb	female	Frederiksberg C	1815	DEN	HeikeKalb@armyspy.com	2014-03-19	AB+	52.9	167	LAV
115	Ella	Sims	female	Gleniti	7910	NZL	EllaSims@fleckens.hu	2029-02-16	A+	62.0	156	LAV
103	Vella	Olivares	female	Manantiales	20002	URU	VellaOlivaresAlonso@dayrep.com	2022-07-20	O-	80.4	171	LAV
8	Yolande	Fortier	female	SÈVRES	92310	FRA	YolandeFortier@teleworm.us	1997-05-04	O+	56.6	157	LAV
92	Klaudia	Roth	female	København K	1016	DEN	KlaudiaRoth@superrito.com	2015-04-06	B+	81.8	173	LAV
125	Scott	Pugh	male	BALTERLEY	CW2 2FH	GBR	ScottPugh@cuvox.de	2006-08-27	O+	73.9	173	BEL
121	Libby	Lee	female	Seaview	7910	NZL	LibbyLee@armyspy.com	1997-03-24	B+	65.4	156	BEL
28	Byron	Beaudouin	male	Nollevaux	6851	BEL	ByronBeaudouin@cuvox.de	2000-09-15	B+	106.2	180	BEL
44	Dirk	Freytag	male	IPFDORF	4481	AUT	DirkFreytag@gustr.com	2033-05-28	A+	96.6	187	BEL
42	Travers	Saurel	male	CREIL	60100	FRA	TraversSaurel@cuvox.de	2014-09-18	A+	111.5	181	BEL
62	Lisa	Schroeder	female	RABENBERG	4910	AUT	LisaSchroeder@dayrep.com	2015-07-08	O-	89.8	157	BEL
116	Alfie	Nicholls	male	BENEREMBAH	2680	AUS	AlfieNicholls@dayrep.com	2016-05-12	B+	96.5	174	BEL
107	Ona	Barrios	female	Sarandí de Navarro	65100	URU	OnaBarriosValencia@einrot.com	2029-03-05	B+	58.0	174	BEL
99	Mariu	Cordova	female	Cuchilla de Peralta	45021	URU	MariuCordovaLimon@rhyta.com	2023-07-11	A+	98.2	156	BEL
12	Ninette	Boutot	female	Basel	4041	SUI	NinetteBoutot@teleworm.us	1980-09-03	O+	48.9	152	LAC
33	Hilaire	Gousse	female	Hegi	8400	SUI	HilaireGousse@teleworm.us	2015-11-10	A+	66.0	158	LAC
106	Ageo	Casas	male	Treinta y Tres	33000	URU	AgeoCasasHernadez@jourrapide.com	2013-09-09	O+	98.0	185	LAC
72	Dennis	Dresdner	male	Fårvang	8882	DEN	DennisDresdner@gustr.com	1981-01-18	A+	71.2	171	LAC
46	Sophie	Mueller	female	SCHMIEDVIERTEL	8253	AUT	SophieMueller@fleckens.hu	1973-04-07	A-	48.3	154	LAC
5	Toussaint	Poissonnier	male	Frauenfeld	8501	SUI	ToussaintPoissonnier@gustr.com	2020-07-24	A+	91.1	185	LAC
74	Katja	Konig	female	Århus C	8000	DEN	KatjaKonig@teleworm.us	2025-04-21	B+	65.5	155	LAC
122	Luke	Ford	male	Watlington	7910	NZL	LukeFord@rhyta.com	2003-12-23	O+	71.8	171	LAC
3	Sibyla	Jacques	female	Soy	6997	BEL	SibylaJacques@teleworm.us	1996-06-15	A+	90.9	171	LAC
91	Wolfgang	Kalb	male	Hergiswil	6052	SUI	WolfgangKalb@armyspy.com	2015-12-14	O-	91.4	184	POT
39	Dominique	Bonsaint	male	GRIGNY	91350	FRA	DominiqueBonsaint@einrot.com	1983-09-20	B+	81.4	176	POT
70	Patrick	Neudorf	male	Dedelstorf	29386	GER	PatrickNeudorf@jourrapide.com	1994-12-20	A+	68.8	166	POT
52	Dennis	Kohler	male	HOFKIRCHEN BEI HARTBERG	8224	AUT	DennisKohler@cuvox.de	1982-10-27	A+	60.7	166	POT
23	Claude	Turgeon	male	Vaulx-lez-Chimay	6462	BEL	ClaudeTurgeon@armyspy.com	2014-02-17	AB+	75.4	181	POT
19	Brunella	Vertefeuille	female	LE PUY-EN-VELAY	43000	FRA	BrunellaVertefeuille@teleworm.us	2010-12-21	B+	88.8	174	POT
14	Clovis	Camus	male	Mont-Gauthier	5580	BEL	ClovisCamus@einrot.com	1991-07-03	O+	89.4	175	POT
93	Frank	Kruger	male	München	80915	GER	FrankKruger@cuvox.de	2006-01-23	B+	73.2	182	POT
41	Harcourt	Gour	male	Les Clées	1356	SUI	HarcourtGour@einrot.com	1979-03-19	AB+	89.5	166	POT
79	Nadine	Huber	female	Schwerin	19028	GER	NadineHuber@einrot.com	2021-02-11	B-	76.7	170	INC
2	Brice	Blondlot	male	Schuiferskapelle	8700	BEL	BriceBlondlot@rhyta.com	1998-05-25	A+	102.0	185	INC
21	Auda	Marleau	female	Izegem	8870	BEL	AudaMarleau@fleckens.hu	1998-09-26	O-	96.2	172	INC
16	Pauline	Grivois	female	Cornol	2952	SUI	PaulineGrivois@einrot.com	2009-08-06	O+	101.6	168	INC
15	Yseult	Rouze	female	ROMAINVILLE	93230	FRA	YseultRouze@gustr.com	2007-03-30	O+	71.8	167	INC
71	Ute	Sankt	female	København V	1506	DEN	UteSankt@jourrapide.com	2039-06-19	O+	65.9	173	INC
56	Philipp	Roth	male	Hadlikon	8340	SUI	PhilippRoth@dayrep.com	1986-07-14	O-	67.6	175	INC
64	Alexander	Koehler	male	Lierfeld	54597	GER	AlexanderKoehler@gustr.com	1996-03-04	O+	98.9	172	INC
4	Bruce	Echeverri	male	NANTES	44100	FRA	BruceEcheverri@cuvox.de	2035-04-02	O+	59.1	170	INC
123	Tilly	Craig	female	Quyon	J0X 2V0	CAN	TillyCraig@superrito.com	2004-05-15	O+	77.4	156	DBX
13	Clarimunda	Lagueux	female	LE PLESSIS-ROBINSON	92350	FRA	ClarimundaLagueux@gustr.com	2027-03-29	A+	81.7	156	DBX
35	Valentine	Paradis	female	Bidogno	6958	SUI	ValentineParadis@cuvox.de	1988-04-16	O+	70.1	159	DBX
10	Olivier	Lamarre	male	VERDUN	55100	FRA	OlivierLamarre@superrito.com	2016-04-07	B+	74.1	184	DBX
53	Katja	Weber	female	Helsingør	3000	DEN	KatjaWeber@einrot.com	1985-03-21	O-	90.9	161	DBX
38	Joy	Franchet	female	Ricken	8726	SUI	JoyFranchet@einrot.com	2005-10-03	B+	80.9	165	DBX
100	Dunstano	Mascarenas	male	Tala	91400	URU	DunstanoMascarenasMiramontes@teleworm.us	2011-10-12	B+	72.4	175	DBX
11	Pinabel	Lachance	male	Boechout	2530	BEL	PinabelLachance@armyspy.com	2000-03-26	B+	99.7	186	DBX
83	Katharina	Amsel	female	København K	1371	DEN	KatharinaAmsel@cuvox.de	2038-12-08	O+	47.2	166	DBX
30	Nathalie	Briard	female	Itzikon	8627	SUI	NathalieBriard@gustr.com	2034-04-17	A+	80.8	160	SHK
94	Stefan	Möller	male	Møldrup	9632	DEN	StefanMoller@jourrapide.com	2026-12-05	B+	66.2	184	SHK
43	Fanchon	Roussel	female	Jauchelette	1370	BEL	FanchonRoussel@einrot.com	1993-03-29	B+	67.9	168	SHK
105	Eliel	Carranza	male	Ombúes de Lavalle	70003	URU	ElielCarranzaMarcos@einrot.com	2011-05-29	O+	74.0	188	SHK
86	Heike	Daecher	female	Odogno	6951	SUI	HeikeDaecher@superrito.com	1987-04-15	O+	53.7	162	SHK
25	Auda	Aupry	female	Bussy-sur-Moudon	1514	SUI	AudaAupry@gustr.com	2002-07-18	A-	92.9	170	SHK
87	Eric	Cole	male	Memmingen	87681	GER	EricCole@rhyta.com	1990-10-07	B+	84.3	166	SHK
69	Maximilian	Schultheiss	male	Attisholz	4533	SUI	MaximilianSchultheiss@superrito.com	1988-12-04	B+	108.7	176	SHK
22	Hilaire	Beaulieu	female	SAINT-MAUR-DES-FOSSÈS	94100	FRA	HilaireBeaulieu@dayrep.com	2014-07-26	B+	68.2	155	SHK
34	Dreux	St-Jacques	male	Prêles	2515	SUI	DreuxSt-Jacques@gustr.com	2011-08-14	O+	85.7	187	NOV
50	Janina	Osterhagen	female	Konstanz Allmannsdorf	78464	GER	JaninaOsterhagen@fleckens.hu	1979-10-17	B+	83.1	168	NOV
97	Esmeralda	Padilla	female	Pirarajá	30002	URU	EsmeraldaPadillaValdivia@rhyta.com	2014-03-03	A+	90.4	163	NOV
29	Cammile	Tanguay	female	Lauperswil	3438	SUI	CammileTanguay@fleckens.hu	1992-03-20	A+	59.5	166	NOV
101	Athina	Tapia	female	Santa Catalina	75201	URU	AthinaTapiaAvila@jourrapide.com	1977-11-12	AB+	73.3	153	NOV
49	Katja	Saenger	female	FREIDORF AN DER LASSNITZ	8523	AUT	KatjaSaenger@teleworm.us	1993-01-22	B+	69.3	154	NOV
37	Arienne	Théberge	female	Saint-Aubin	5620	BEL	ArienneTheberge@cuvox.de	2030-06-29	B-	51.1	167	NOV
109	Clare	Gallegos	female	Costa Azul	16202	URU	ClareGallegosArmendariz@dayrep.com	2030-06-24	A+	66.6	157	NOV
119	Isabella	Brady	female	WILLOUGHBY	2068	AUS	IsabellaBrady@dayrep.com	2034-04-25	AB+	71.5	156	NOV
\.


--
-- Name: competitor_idcompetitor_seq; Type: SEQUENCE SET; Schema: eurotour; Owner: -
--

SELECT pg_catalog.setval('competitor_idcompetitor_seq', 126, true);


--
-- Data for Name: country; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY country (idcountry, name) FROM stdin;
AFG	Afghanistan
AHO	Antilles néerlandaises
ALB	Albanie
ALG	Algérie
AND	Andorre
ANG	Angola
ANT	Antigua-et-Barbuda
ARG	Argentine
ARM	Arménie
ARU	Aruba
ASA	Samoa américaines
AUS	Australie
AUT	Autriche
AZE	Azerbaïdjan
BAH	Bahamas
BAN	Bangladesh
BAR	Barbade
BDI	Burundi
BEL	Belgique
BEN	Bénin
BER	Bermudes
BHU	Bhoutan
BIH	Bosnie-Herzégovine
BIZ	Belize
BLR	Biélorussie
BOL	Bolivie
BOT	Botswana
BRA	Brésil
BRN	Bahreïn
BRU	Brunei
BUL	Bulgarie
BUR	Burkina Faso
CAF	République centrafricaine
CAM	Cambodge
CAN	Canada
CAY	Iles Caïmans
CGO	République du Congo
CHA	Tchad
CHI	Chili
CHN	République populaire de Chine
CIV	Côte d'Ivoire
CMR	Cameroun
COD	République démocratique du Congo
COK	Iles Cook
COL	Colombie
COM	Comores
CPV	Cap-Vert
CRC	Costa Rica
CRO	Croatie
CUB	Cuba
CYP	Chypre
CZE	République tchèque
DEN	Danemark
DJI	Djibouti
DMA	Ile de la Dominique
DOM	République dominicaine
ECU	Équateur
EGY	Égypte
ERI	Érythrée
ESA	Salvador
ESP	Espagne
EST	Estonie
ETH	Éthiopie
FIJ	Fidji
FIN	Finlande
FRA	France
FSM	Micronésie
GAB	Gabon
GAM	Gambie
GBR	Royaume-Uni
GBS	Guinée-Bissau
GEO	Géorgie
GEQ	Guinée équatoriale
GER	Allemagne
GHA	Ghana
GRE	Grèce
GRN	Grenade
GUA	Guatemala
GUI	Guinée
GUM	Guam
GUY	Guyana
HAI	Haïti
HKG	Hong Kong
HON	Honduras
HUN	Hongrie
INA	Indonésie
IND	Inde
IRI	Iran
IRL	Irlande
IRQ	Irak
ISL	Islande
ISR	Israël
ISV	Iles Vierges des États Unis
ITA	Italie
IVB	Iles Vierges britanniques
JAM	Jamaïque
JOR	Jordanie
JPN	Japon
KAZ	Kazakhstan
KEN	Kenya
KGZ	Kirghizistan
KIR	Kiribati
KOR	Corée du sud
KSA	Arabie saoudite
KUW	Koweït
LAO	Laos
LAT	Lettonie
LBA	Libye
LBR	Liberia
LCA	Sainte-Lucie
LES	Lesotho
LIB	Liban
LIE	Liechtenstein
LTU	Lituanie
LUX	Luxembourg
MAD	Madagascar
MAR	Maroc
MAS	Malaisie
MAW	Malawi
MDA	Moldavie
MDV	Maldives
MEX	Mexique
MGL	Mongolie
MHL	Marshall
MKD	Macédoine
MLI	Mali
MLT	Malte
MNE	Monténégro
MON	Monaco
MOZ	Mozambique
MRI	Maurice
MTN	Mauritanie
MYA	Birmanie
NAM	Namibie
NCA	Nicaragua
NED	Pays-Bas
NEP	Népal
NGR	Nigeria
NIG	Niger
NOR	Norvège
NRU	Nauru
NZL	Nouvelle-Zélande
OMA	Oman
PAK	Pakistan
PAN	Panamá
PAR	Paraguay
PER	Pérou
PHI	Philippines
PLE	Palestine
PLW	Palaos
PNG	Papouasie-Nouvelle-Guinée
POL	Pologne
POR	Portugal
PRK	Corée du nord
PUR	Porto Rico
QAT	Qatar
ROU	Roumanie
RSA	Afrique du Sud
RUS	Russie
RWA	Rwanda
SAM	Samoa
SEN	Sénégal
SEY	Seychelles
SIN	Singapour
SKN	Saint-Kitts-et-Nevis
SLE	Sierra Leone
SLO	Slovénie
SMR	Saint-Marin
SOL	Iles Salomon
SOM	Somalie
SRB	Serbie
SRI	Sri Lanka
STP	Sao Tomé-et-Principe
SUD	Soudan
SUI	Suisse
SUR	Suriname
SVK	Slovaquie
SWE	Suède
SWZ	Swaziland
SYR	Syrie
TAN	Tanzanie
TGA	Tonga
THA	Thaïlande
TJK	Tadjikistan
TKM	Turkménistan
TLS	Timor oriental
TOG	Togo
TPE	Taipei
TRI	Trinité-et-Tobago
TUN	Tunisie
TUR	Turquie
TUV	Tuvalu
UAE	Emirats arabes unis
UGA	Ouganda
UKR	Ukraine
URU	Uruguay
USA	États-Unis
UZB	Ouzbékistan
VAN	Vanuatu
VEN	Venezuela
VIE	Viêt Nam
VIN	Saint-Vincent-et-les-Grenadines
YEM	Yémen
ZAM	Zambie
ZIM	Zimbabwe
\.


--
-- Data for Name: performance; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY performance (rank, idcompetitor, idstage, duration) FROM stdin;
60	1	1	03:43:00
59	2	1	03:41:00
119	3	1	05:34:00
4	4	1	02:53:00
34	5	1	03:13:00
124	6	1	05:54:00
68	7	1	03:49:00
60	8	1	03:43:00
98	9	1	04:27:00
26	10	1	03:07:00
49	11	1	03:32:00
80	12	1	03:59:00
45	13	1	03:27:00
34	14	1	03:13:00
19	15	1	03:04:00
68	16	1	03:49:00
126	17	1	05:59:00
87	18	1	04:09:00
105	19	1	04:44:00
4	20	1	02:53:00
26	21	1	03:07:00
15	22	1	03:02:00
93	23	1	04:17:00
26	24	1	03:07:00
125	25	1	05:57:00
13	26	1	03:00:00
22	27	1	03:05:00
19	28	1	03:04:00
89	29	1	04:12:00
49	30	1	03:32:00
56	31	1	03:36:00
65	32	1	03:46:00
111	33	1	05:03:00
76	34	1	03:57:00
79	35	1	03:58:00
52	36	1	03:33:00
76	37	1	03:57:00
23	38	1	03:06:00
1	39	1	02:49:00
53	40	1	03:34:00
117	41	1	05:21:00
6	42	1	02:54:00
82	43	1	04:05:00
74	44	1	03:56:00
43	45	1	03:25:00
32	46	1	03:12:00
47	47	1	03:29:00
116	48	1	05:20:00
60	49	1	03:43:00
3	50	1	02:51:00
84	51	1	04:08:00
81	52	1	04:00:00
8	53	1	02:56:00
72	54	1	03:54:00
70	55	1	03:53:00
88	56	1	04:11:00
37	57	1	03:15:00
64	58	1	03:45:00
60	59	1	03:43:00
109	60	1	04:53:00
118	61	1	05:26:00
30	62	1	03:08:00
112	63	1	05:06:00
31	64	1	03:11:00
70	65	1	03:53:00
93	66	1	04:17:00
102	67	1	04:36:00
76	68	1	03:57:00
11	69	1	02:58:00
57	70	1	03:38:00
105	71	1	04:44:00
17	72	1	03:03:00
108	73	1	04:50:00
32	74	1	03:12:00
101	75	1	04:33:00
45	76	1	03:27:00
104	77	1	04:39:00
43	78	1	03:25:00
42	79	1	03:23:00
89	80	1	04:12:00
113	81	1	05:07:00
84	82	1	04:08:00
23	83	1	03:06:00
15	84	1	03:02:00
99	85	1	04:28:00
84	86	1	04:08:00
36	87	1	03:14:00
40	88	1	03:20:00
7	89	1	02:55:00
38	90	1	03:16:00
72	91	1	03:54:00
107	92	1	04:46:00
8	93	1	02:56:00
83	94	1	04:07:00
26	95	1	03:07:00
100	96	1	04:31:00
110	97	1	04:54:00
55	98	1	03:35:00
114	99	1	05:08:00
67	100	1	03:47:00
74	101	1	03:56:00
12	102	1	02:59:00
49	103	1	03:32:00
122	104	1	05:52:00
8	105	1	02:56:00
89	106	1	04:12:00
121	107	1	05:41:00
93	108	1	04:17:00
1	109	1	02:49:00
92	110	1	04:16:00
23	111	1	03:06:00
19	112	1	03:04:00
93	113	1	04:17:00
97	114	1	04:25:00
48	115	1	03:30:00
58	116	1	03:39:00
40	117	1	03:20:00
53	118	1	03:34:00
14	119	1	03:01:00
65	120	1	03:46:00
122	121	1	05:52:00
115	122	1	05:17:00
17	123	1	03:03:00
103	124	1	04:38:00
38	125	1	03:16:00
120	126	1	05:40:00
67	1	2	06:57:00
52	2	2	06:19:00
114	3	2	09:09:00
16	4	2	05:25:00
50	5	2	06:12:00
119	6	2	09:33:00
68	7	2	06:58:00
55	8	2	06:24:00
90	9	2	07:33:00
3	10	2	05:02:00
46	11	2	06:09:00
82	12	2	07:20:00
55	13	2	06:24:00
12	14	2	05:23:00
33	15	2	05:47:00
61	16	2	06:43:00
121	17	2	09:45:00
76	18	2	07:13:00
116	19	2	09:16:00
18	20	2	05:26:00
26	21	2	05:35:00
18	22	2	05:26:00
86	23	2	07:29:00
36	24	2	05:52:00
118	25	2	09:29:00
31	26	2	05:45:00
14	27	2	05:24:00
1	28	2	04:50:00
83	29	2	07:28:00
51	30	2	06:16:00
48	31	2	06:11:00
58	32	2	06:30:00
113	33	2	09:01:00
92	34	2	07:38:00
83	35	2	07:28:00
39	36	2	05:59:00
68	37	2	06:58:00
9	38	2	05:17:00
5	39	2	05:06:00
29	40	2	05:38:00
112	41	2	08:58:00
6	42	2	05:10:00
77	43	2	07:15:00
81	44	2	07:18:00
60	45	2	06:38:00
22	46	2	05:32:00
41	47	2	06:00:00
109	48	2	08:44:00
70	49	2	07:00:00
21	50	2	05:31:00
101	51	2	08:00:00
93	52	2	07:44:00
2	53	2	04:54:00
88	54	2	07:30:00
72	55	2	07:02:00
95	56	2	07:48:00
43	57	2	06:02:00
77	58	2	07:15:00
54	59	2	06:22:00
114	60	2	09:09:00
111	61	2	08:56:00
20	62	2	05:28:00
120	63	2	09:41:00
14	64	2	05:24:00
73	65	2	07:03:00
94	66	2	07:47:00
99	67	2	07:58:00
90	68	2	07:33:00
28	69	2	05:37:00
57	70	2	06:25:00
103	71	2	08:17:00
38	72	2	05:56:00
99	73	2	07:58:00
32	74	2	05:46:00
107	75	2	08:35:00
52	76	2	06:19:00
98	77	2	07:57:00
63	78	2	06:45:00
42	79	2	06:01:00
97	80	2	07:55:00
122	81	2	09:51:00
61	82	2	06:43:00
27	83	2	05:36:00
24	84	2	05:34:00
89	85	2	07:31:00
74	86	2	07:07:00
47	87	2	06:10:00
44	88	2	06:07:00
8	89	2	05:12:00
45	90	2	06:08:00
59	91	2	06:33:00
102	92	2	08:09:00
10	93	2	05:19:00
80	94	2	07:17:00
24	95	2	05:34:00
95	96	2	07:48:00
110	97	2	08:45:00
37	98	2	05:53:00
108	99	2	08:37:00
66	100	2	06:54:00
77	101	2	07:15:00
22	102	2	05:32:00
33	103	2	05:47:00
126	104	2	10:19:00
16	105	2	05:25:00
105	106	2	08:27:00
123	107	2	09:56:00
75	108	2	07:09:00
4	109	2	05:03:00
83	110	2	07:28:00
12	111	2	05:23:00
35	112	2	05:50:00
86	113	2	07:29:00
106	114	2	08:32:00
48	115	2	06:11:00
65	116	2	06:51:00
39	117	2	05:59:00
64	118	2	06:48:00
6	119	2	05:10:00
70	120	2	07:00:00
124	121	2	09:59:00
117	122	2	09:24:00
11	123	2	05:22:00
103	124	2	08:17:00
30	125	2	05:41:00
125	126	2	10:13:00
23	1	3	06:15:00
119	2	3	07:25:00
97	3	3	06:59:00
5	4	3	06:00:00
26	5	3	06:16:00
31	6	3	06:20:00
52	7	3	06:29:00
1	8	3	05:55:00
44	9	3	06:26:00
61	10	3	06:32:00
87	11	3	06:50:00
100	12	3	07:00:00
92	13	3	06:54:00
108	14	3	07:09:00
57	15	3	06:31:00
7	16	3	06:02:00
73	17	3	06:42:00
93	18	3	06:55:00
111	19	3	07:11:00
4	20	3	05:59:00
31	21	3	06:20:00
83	22	3	06:49:00
65	23	3	06:35:00
68	24	3	06:36:00
88	25	3	06:51:00
57	26	3	06:31:00
79	27	3	06:46:00
123	28	3	07:49:00
55	29	3	06:30:00
114	30	3	07:15:00
8	31	3	06:03:00
41	32	3	06:25:00
21	33	3	06:14:00
117	34	3	07:20:00
44	36	3	06:26:00
64	37	3	06:34:00
52	38	3	06:29:00
57	39	3	06:31:00
39	40	3	06:24:00
121	41	3	07:35:00
44	42	3	06:26:00
37	43	3	06:23:00
121	44	3	07:35:00
73	45	3	06:42:00
116	46	3	07:17:00
23	47	3	06:15:00
100	48	3	07:00:00
18	49	3	06:11:00
10	50	3	06:04:00
79	52	3	06:46:00
37	53	3	06:23:00
77	54	3	06:45:00
14	55	3	06:09:00
14	56	3	06:09:00
96	57	3	06:57:00
104	58	3	07:03:00
91	59	3	06:53:00
26	60	3	06:16:00
81	61	3	06:48:00
113	62	3	07:12:00
35	63	3	06:21:00
118	64	3	07:22:00
48	65	3	06:27:00
48	66	3	06:27:00
75	67	3	06:43:00
111	68	3	07:11:00
89	69	3	06:52:00
26	70	3	06:16:00
63	71	3	06:33:00
83	72	3	06:49:00
100	73	3	07:00:00
44	74	3	06:26:00
77	75	3	06:45:00
89	76	3	06:52:00
10	77	3	06:04:00
120	78	3	07:29:00
23	79	3	06:15:00
20	80	3	06:13:00
14	81	3	06:09:00
14	82	3	06:09:00
21	83	3	06:14:00
70	84	3	06:38:00
30	85	3	06:19:00
1	86	3	05:55:00
65	87	3	06:35:00
97	88	3	06:59:00
97	89	3	06:59:00
12	90	3	06:05:00
41	91	3	06:25:00
57	92	3	06:31:00
109	93	3	07:10:00
18	94	3	06:11:00
83	95	3	06:49:00
12	96	3	06:05:00
68	97	3	06:36:00
51	98	3	06:28:00
105	99	3	07:04:00
35	100	3	06:21:00
8	101	3	06:03:00
31	102	3	06:20:00
41	103	3	06:25:00
83	104	3	06:49:00
52	105	3	06:29:00
94	106	3	06:56:00
94	108	3	06:56:00
76	109	3	06:44:00
61	110	3	06:32:00
115	111	3	07:16:00
5	112	3	06:00:00
105	113	3	07:04:00
39	114	3	06:24:00
109	115	3	07:10:00
65	116	3	06:35:00
48	117	3	06:27:00
55	118	3	06:30:00
26	119	3	06:16:00
103	120	3	07:02:00
81	121	3	06:48:00
72	122	3	06:41:00
3	123	3	05:57:00
107	124	3	07:08:00
70	125	3	06:38:00
31	126	3	06:20:00
53	1	4	04:21:00
42	2	4	04:10:00
115	3	4	06:31:00
3	4	4	03:29:00
46	5	4	04:13:00
118	6	4	06:56:00
62	7	4	04:31:00
78	9	4	05:03:00
7	10	4	03:36:00
26	11	4	03:52:00
82	12	4	05:08:00
64	13	4	04:33:00
6	14	4	03:34:00
18	15	4	03:47:00
53	16	4	04:21:00
117	17	4	06:54:00
76	18	4	04:59:00
108	19	4	06:08:00
8	20	4	03:37:00
35	21	4	04:02:00
33	22	4	03:59:00
88	23	4	05:15:00
39	24	4	04:04:00
120	25	4	07:03:00
8	26	4	03:37:00
11	27	4	03:40:00
16	28	4	03:46:00
98	29	4	05:42:00
51	30	4	04:17:00
58	31	4	04:24:00
66	32	4	04:39:00
105	33	4	05:56:00
86	34	4	05:10:00
30	36	4	03:57:00
89	37	4	05:16:00
3	38	4	03:29:00
12	39	4	03:43:00
40	40	4	04:05:00
114	41	4	06:25:00
5	42	4	03:31:00
87	43	4	05:13:00
73	44	4	04:55:00
44	45	4	04:12:00
12	46	4	03:43:00
51	47	4	04:17:00
102	48	4	05:47:00
71	49	4	04:49:00
10	50	4	03:39:00
82	52	4	05:08:00
25	53	4	03:51:00
72	54	4	04:54:00
59	55	4	04:26:00
74	56	4	04:58:00
50	57	4	04:16:00
65	58	4	04:36:00
70	59	4	04:48:00
103	60	4	05:51:00
108	61	4	06:08:00
1	62	4	03:27:00
116	63	4	06:36:00
35	64	4	04:02:00
67	65	4	04:41:00
95	66	4	05:33:00
106	67	4	06:01:00
77	68	4	05:01:00
23	69	4	03:50:00
55	70	4	04:23:00
111	71	4	06:23:00
43	72	4	04:11:00
100	73	4	05:44:00
30	74	4	03:57:00
96	75	4	05:36:00
47	76	4	04:14:00
85	77	4	05:09:00
47	78	4	04:14:00
26	79	4	03:52:00
101	80	4	05:46:00
113	81	4	06:24:00
82	82	4	05:08:00
44	83	4	04:12:00
21	84	4	03:49:00
80	85	4	05:04:00
69	86	4	04:47:00
55	87	4	04:23:00
35	88	4	04:02:00
20	89	4	03:48:00
33	90	4	03:59:00
61	91	4	04:30:00
91	92	4	05:19:00
28	93	4	03:55:00
74	94	4	04:58:00
12	95	4	03:43:00
93	96	4	05:30:00
106	97	4	06:01:00
55	98	4	04:23:00
110	99	4	06:18:00
49	100	4	04:15:00
90	101	4	05:18:00
16	102	4	03:46:00
29	103	4	03:56:00
111	104	4	06:23:00
21	105	4	03:49:00
92	106	4	05:23:00
78	108	4	05:03:00
18	109	4	03:47:00
94	110	4	05:31:00
1	111	4	03:27:00
23	112	4	03:50:00
99	113	4	05:43:00
97	114	4	05:38:00
68	115	4	04:43:00
62	116	4	04:31:00
35	117	4	04:02:00
60	118	4	04:29:00
30	119	4	03:57:00
81	120	4	05:05:00
119	121	4	06:57:00
121	122	4	07:04:00
15	123	4	03:45:00
103	124	4	05:51:00
41	125	4	04:09:00
122	126	4	07:12:00
109	1	5	08:13:00
73	2	5	07:28:00
12	3	5	06:28:00
12	4	5	06:28:00
24	5	5	06:43:00
83	6	5	07:38:00
97	7	5	07:54:00
105	9	5	08:09:00
29	10	5	06:46:00
121	11	5	08:55:00
50	12	5	07:08:00
65	13	5	07:18:00
45	14	5	07:02:00
80	15	5	07:37:00
102	16	5	08:02:00
33	17	5	06:48:00
78	18	5	07:35:00
42	19	5	07:00:00
65	20	5	07:18:00
87	21	5	07:43:00
44	22	5	07:01:00
83	23	5	07:38:00
107	24	5	08:11:00
117	25	5	08:33:00
60	26	5	07:15:00
50	27	5	07:08:00
26	28	5	06:45:00
101	29	5	07:58:00
4	30	5	06:15:00
22	31	5	06:41:00
90	32	5	07:45:00
17	33	5	06:34:00
38	34	5	06:59:00
50	36	5	07:08:00
94	37	5	07:50:00
96	38	5	07:53:00
35	39	5	06:51:00
80	40	5	07:37:00
25	41	5	06:44:00
89	42	5	07:44:00
72	43	5	07:25:00
37	44	5	06:56:00
68	45	5	07:21:00
3	46	5	06:13:00
83	47	5	07:38:00
104	48	5	08:07:00
111	49	5	08:25:00
38	50	5	06:59:00
15	52	5	06:32:00
73	53	5	07:28:00
56	54	5	07:10:00
91	55	5	07:47:00
120	56	5	08:53:00
18	57	5	06:35:00
7	58	5	06:18:00
87	59	5	07:43:00
86	60	5	07:40:00
32	61	5	06:47:00
26	62	5	06:45:00
5	63	5	06:16:00
103	64	5	08:05:00
11	65	5	06:27:00
110	66	5	08:14:00
22	67	5	06:41:00
113	68	5	08:27:00
50	69	5	07:08:00
6	70	5	06:17:00
1	71	5	06:10:00
16	72	5	06:33:00
36	73	5	06:53:00
46	74	5	07:04:00
107	75	5	08:11:00
48	76	5	07:07:00
64	77	5	07:16:00
115	78	5	08:30:00
2	79	5	06:12:00
54	80	5	07:09:00
26	81	5	06:45:00
60	82	5	07:15:00
38	83	5	06:59:00
99	84	5	07:56:00
60	85	5	07:15:00
59	86	5	07:13:00
21	87	5	06:40:00
92	88	5	07:48:00
42	89	5	07:00:00
105	90	5	08:09:00
113	91	5	08:27:00
18	92	5	06:35:00
78	93	5	07:35:00
73	94	5	07:28:00
48	95	5	07:07:00
115	96	5	08:30:00
10	97	5	06:24:00
56	98	5	07:10:00
73	99	5	07:28:00
8	100	5	06:22:00
47	101	5	07:06:00
38	102	5	06:59:00
70	103	5	07:24:00
98	104	5	07:55:00
34	105	5	06:49:00
95	106	5	07:52:00
8	108	5	06:22:00
92	109	5	07:48:00
65	110	5	07:18:00
112	111	5	08:26:00
20	112	5	06:39:00
54	113	5	07:09:00
56	114	5	07:10:00
80	115	5	07:37:00
29	116	5	06:46:00
60	117	5	07:15:00
121	118	5	08:55:00
14	119	5	06:29:00
70	120	5	07:24:00
100	121	5	07:57:00
118	122	5	08:37:00
77	123	5	07:34:00
68	124	5	07:21:00
119	125	5	08:38:00
29	126	5	06:46:00
73	1	6	10:01:00
52	2	6	09:05:00
109	3	6	13:03:00
14	4	6	07:50:00
39	5	6	08:33:00
122	6	6	15:28:00
65	7	6	09:46:00
80	9	6	10:28:00
8	10	6	07:32:00
27	11	6	08:09:00
60	12	6	09:34:00
54	13	6	09:10:00
4	14	6	07:23:00
31	15	6	08:18:00
49	16	6	08:58:00
117	17	6	14:04:00
88	18	6	10:47:00
110	19	6	13:06:00
8	20	6	07:32:00
46	21	6	08:52:00
15	22	6	07:52:00
91	23	6	11:09:00
12	24	6	07:47:00
121	25	6	15:15:00
23	26	6	08:03:00
34	27	6	08:25:00
2	28	6	07:14:00
92	29	6	11:17:00
36	30	6	08:27:00
66	31	6	09:49:00
51	32	6	09:04:00
115	33	6	13:44:00
77	34	6	10:19:00
47	36	6	08:56:00
86	37	6	10:44:00
10	38	6	07:46:00
19	39	6	08:01:00
45	40	6	08:50:00
116	41	6	13:48:00
18	42	6	08:00:00
67	43	6	09:50:00
74	44	6	10:03:00
64	45	6	09:45:00
17	46	6	07:55:00
33	47	6	08:21:00
105	48	6	12:23:00
79	49	6	10:25:00
21	50	6	08:02:00
83	52	6	10:39:00
1	53	6	07:08:00
89	54	6	10:56:00
59	55	6	09:33:00
84	56	6	10:40:00
38	57	6	08:30:00
63	58	6	09:42:00
70	59	6	09:54:00
102	60	6	11:59:00
108	61	6	12:52:00
12	62	6	07:47:00
119	63	6	14:25:00
31	64	6	08:18:00
76	65	6	10:09:00
84	66	6	10:40:00
106	67	6	12:37:00
90	68	6	11:00:00
3	69	6	07:20:00
55	70	6	09:13:00
104	71	6	12:03:00
41	72	6	08:38:00
101	73	6	11:51:00
24	74	6	08:07:00
102	75	6	11:59:00
53	76	6	09:07:00
95	77	6	11:35:00
42	78	6	08:44:00
16	79	6	07:54:00
99	80	6	11:50:00
118	81	6	14:14:00
69	82	6	09:53:00
26	83	6	08:08:00
6	84	6	07:26:00
93	85	6	11:19:00
78	86	6	10:20:00
44	87	6	08:48:00
50	88	6	09:03:00
21	89	6	08:02:00
43	90	6	08:45:00
58	91	6	09:31:00
98	92	6	11:44:00
7	93	6	07:28:00
62	94	6	09:41:00
10	95	6	07:46:00
97	96	6	11:43:00
112	97	6	13:23:00
48	98	6	08:57:00
99	99	6	11:50:00
61	100	6	09:36:00
86	101	6	10:44:00
30	102	6	08:16:00
56	103	6	09:19:00
113	104	6	13:25:00
29	105	6	08:14:00
94	106	6	11:31:00
71	108	6	09:56:00
24	109	6	08:07:00
82	110	6	10:36:00
19	111	6	08:01:00
37	112	6	08:29:00
81	113	6	10:34:00
96	114	6	11:42:00
71	115	6	09:56:00
75	116	6	10:06:00
35	117	6	08:26:00
57	118	6	09:24:00
5	119	6	07:24:00
67	120	6	09:50:00
120	121	6	14:42:00
113	122	6	13:25:00
40	123	6	08:36:00
107	124	6	12:42:00
28	125	6	08:13:00
111	126	6	13:20:00
67	1	7	05:50:00
96	2	7	06:14:00
20	3	7	05:19:00
13	4	7	05:14:00
61	5	7	05:42:00
83	6	7	06:03:00
94	7	7	06:12:00
112	9	7	06:33:00
45	10	7	05:35:00
120	11	7	07:00:00
18	12	7	05:17:00
43	13	7	05:33:00
15	14	7	05:15:00
81	16	7	06:02:00
4	17	7	04:56:00
48	18	7	05:37:00
11	19	7	05:07:00
48	20	7	05:37:00
51	21	7	05:38:00
31	22	7	05:24:00
51	23	7	05:38:00
102	24	7	06:19:00
111	25	7	06:32:00
51	26	7	05:38:00
69	27	7	05:51:00
24	28	7	05:20:00
118	29	7	06:52:00
24	30	7	05:20:00
67	31	7	05:50:00
64	32	7	05:48:00
31	33	7	05:24:00
24	34	7	05:20:00
87	36	7	06:06:00
61	37	7	05:42:00
58	38	7	05:41:00
69	39	7	05:51:00
89	40	7	06:08:00
16	41	7	05:16:00
90	42	7	06:10:00
43	43	7	05:33:00
13	44	7	05:14:00
72	45	7	05:54:00
5	46	7	05:01:00
79	47	7	06:01:00
113	48	7	06:36:00
105	49	7	06:22:00
31	50	7	05:24:00
38	52	7	05:28:00
109	53	7	06:28:00
66	54	7	05:49:00
96	55	7	06:14:00
99	56	7	06:17:00
37	57	7	05:26:00
12	58	7	05:13:00
91	59	7	06:11:00
64	60	7	05:48:00
24	61	7	05:20:00
16	62	7	05:16:00
46	63	7	05:36:00
95	64	7	06:13:00
8	65	7	05:05:00
106	66	7	06:24:00
46	67	7	05:36:00
86	68	7	06:04:00
40	69	7	05:31:00
7	70	7	05:04:00
1	71	7	04:49:00
29	72	7	05:21:00
24	73	7	05:20:00
63	74	7	05:47:00
83	75	7	06:03:00
91	76	7	06:11:00
51	77	7	05:38:00
115	78	7	06:46:00
2	79	7	04:51:00
100	80	7	06:18:00
20	81	7	05:19:00
77	82	7	05:59:00
40	83	7	05:31:00
75	84	7	05:56:00
69	85	7	05:51:00
103	86	7	06:20:00
30	87	7	05:22:00
107	88	7	06:25:00
20	89	7	05:19:00
108	90	7	06:27:00
96	91	7	06:14:00
58	92	7	05:41:00
100	93	7	06:18:00
20	95	7	05:19:00
83	96	7	06:03:00
40	97	7	05:31:00
56	98	7	05:40:00
81	99	7	06:02:00
35	100	7	05:25:00
73	101	7	05:55:00
9	102	7	05:06:00
31	103	7	05:24:00
114	104	7	06:45:00
3	105	7	04:55:00
110	106	7	06:30:00
6	108	7	05:02:00
115	109	7	06:46:00
18	110	7	05:17:00
119	111	7	06:59:00
35	112	7	05:25:00
51	113	7	05:38:00
48	114	7	05:37:00
78	115	7	06:00:00
39	116	7	05:29:00
58	117	7	05:41:00
117	118	7	06:48:00
9	119	7	05:06:00
75	120	7	05:56:00
88	121	7	06:07:00
104	122	7	06:21:00
79	123	7	06:01:00
56	124	7	05:40:00
91	125	7	06:11:00
73	126	7	05:55:00
71	1	8	05:08:00
102	2	8	05:32:00
38	3	8	04:49:00
5	4	8	04:23:00
47	5	8	04:52:00
77	6	8	05:13:00
108	7	8	05:40:00
98	9	8	05:30:00
79	10	8	05:15:00
105	11	8	05:36:00
47	12	8	04:52:00
61	13	8	05:02:00
51	14	8	04:54:00
66	16	8	05:05:00
23	17	8	04:40:00
75	18	8	05:12:00
10	19	8	04:29:00
18	20	8	04:33:00
103	21	8	05:34:00
85	23	8	05:24:00
98	24	8	05:30:00
111	25	8	05:43:00
54	26	8	04:56:00
73	27	8	05:10:00
10	28	8	04:29:00
118	29	8	06:04:00
18	30	8	04:33:00
30	31	8	04:43:00
30	32	8	04:43:00
13	33	8	04:30:00
61	34	8	05:02:00
24	36	8	04:41:00
59	37	8	04:59:00
82	38	8	05:20:00
36	39	8	04:47:00
105	40	8	05:36:00
13	41	8	04:30:00
72	42	8	05:09:00
44	43	8	04:51:00
32	44	8	04:44:00
83	45	8	05:21:00
28	46	8	04:42:00
93	47	8	05:28:00
91	48	8	05:27:00
89	49	8	05:26:00
32	50	8	04:44:00
41	52	8	04:50:00
87	53	8	05:25:00
41	54	8	04:50:00
115	55	8	05:50:00
112	56	8	05:46:00
57	57	8	04:58:00
7	58	8	04:27:00
56	59	8	04:57:00
74	60	8	05:11:00
47	61	8	04:52:00
2	62	8	04:20:00
8	63	8	04:28:00
98	64	8	05:30:00
34	65	8	04:45:00
96	66	8	05:29:00
13	67	8	04:30:00
93	68	8	05:28:00
35	69	8	04:46:00
16	70	8	04:31:00
6	71	8	04:26:00
8	72	8	04:28:00
21	73	8	04:36:00
41	74	8	04:50:00
110	75	8	05:41:00
53	76	8	04:55:00
50	77	8	04:53:00
119	78	8	06:16:00
20	79	8	04:35:00
51	80	8	04:54:00
38	81	8	04:49:00
91	82	8	05:27:00
44	83	8	04:51:00
63	84	8	05:03:00
36	85	8	04:47:00
75	86	8	05:12:00
65	87	8	05:04:00
89	88	8	05:26:00
10	89	8	04:29:00
96	90	8	05:29:00
112	91	8	05:46:00
16	92	8	04:31:00
44	93	8	04:51:00
54	95	8	04:56:00
104	96	8	05:35:00
4	97	8	04:21:00
81	98	8	05:17:00
77	99	8	05:13:00
1	100	8	04:11:00
66	101	8	05:05:00
28	102	8	04:42:00
57	103	8	04:58:00
116	104	8	05:54:00
22	105	8	04:37:00
108	106	8	05:40:00
2	108	8	04:20:00
87	109	8	05:25:00
66	110	8	05:05:00
93	111	8	05:28:00
38	112	8	04:49:00
59	113	8	04:59:00
70	114	8	05:06:00
101	115	8	05:31:00
24	116	8	04:41:00
24	117	8	04:41:00
114	118	8	05:48:00
24	119	8	04:41:00
85	120	8	05:24:00
84	121	8	05:23:00
117	122	8	06:03:00
66	123	8	05:05:00
80	124	8	05:16:00
107	125	8	05:37:00
63	126	8	05:03:00
15	1	9	08:53:00
90	2	9	09:52:00
88	3	9	09:45:00
1	4	9	08:16:00
77	5	9	09:34:00
52	6	9	09:16:00
82	7	9	09:39:00
17	9	9	08:54:00
31	10	9	09:04:00
67	11	9	09:28:00
17	12	9	08:54:00
41	13	9	09:12:00
74	14	9	09:32:00
57	16	9	09:20:00
114	17	9	10:36:00
110	18	9	10:19:00
49	19	9	09:15:00
9	20	9	08:40:00
23	21	9	08:58:00
41	23	9	09:12:00
46	24	9	09:14:00
84	25	9	09:41:00
35	26	9	09:07:00
64	27	9	09:24:00
87	28	9	09:44:00
46	29	9	09:14:00
100	30	9	10:05:00
49	31	9	09:15:00
84	32	9	09:41:00
3	33	9	08:28:00
112	34	9	10:26:00
7	36	9	08:34:00
67	37	9	09:28:00
37	38	9	09:10:00
98	39	9	10:03:00
95	40	9	09:58:00
118	41	9	10:48:00
20	42	9	08:56:00
80	43	9	09:37:00
116	44	9	10:45:00
8	45	9	08:36:00
39	46	9	09:11:00
73	47	9	09:31:00
114	48	9	10:36:00
65	49	9	09:26:00
17	50	9	08:54:00
111	52	9	10:23:00
35	53	9	09:07:00
13	54	9	08:51:00
11	55	9	08:48:00
3	56	9	08:28:00
98	57	9	10:03:00
55	58	9	09:18:00
53	59	9	09:17:00
46	60	9	09:14:00
12	61	9	08:50:00
106	62	9	10:12:00
31	63	9	09:04:00
106	64	9	10:12:00
26	65	9	08:59:00
86	66	9	09:42:00
97	67	9	10:01:00
92	68	9	09:55:00
20	69	9	08:56:00
29	70	9	09:00:00
57	71	9	09:20:00
102	72	9	10:08:00
109	73	9	10:18:00
57	74	9	09:20:00
67	75	9	09:28:00
75	76	9	09:33:00
6	77	9	08:33:00
77	78	9	09:34:00
81	80	9	09:38:00
39	81	9	09:11:00
91	82	9	09:53:00
26	83	9	08:59:00
83	84	9	09:40:00
103	85	9	10:09:00
65	86	9	09:26:00
75	87	9	09:33:00
101	88	9	10:07:00
96	89	9	09:59:00
30	90	9	09:02:00
104	91	9	10:10:00
71	92	9	09:29:00
106	93	9	10:12:00
71	95	9	09:29:00
5	96	9	08:31:00
93	97	9	09:56:00
15	98	9	08:53:00
23	99	9	08:58:00
45	100	9	09:13:00
14	101	9	08:52:00
41	102	9	09:12:00
61	103	9	09:22:00
93	104	9	09:56:00
31	105	9	09:04:00
113	106	9	10:29:00
89	108	9	09:46:00
22	109	9	08:57:00
57	110	9	09:20:00
117	111	9	10:46:00
10	112	9	08:47:00
61	113	9	09:22:00
105	114	9	10:11:00
31	115	9	09:04:00
79	116	9	09:35:00
63	117	9	09:23:00
49	118	9	09:15:00
2	119	9	08:18:00
23	120	9	08:58:00
53	121	9	09:17:00
26	122	9	08:59:00
41	123	9	09:12:00
56	124	9	09:19:00
67	125	9	09:28:00
37	126	9	09:10:00
80	1	10	06:47:00
84	2	10	06:50:00
24	3	10	05:54:00
49	4	10	06:19:00
33	5	10	06:01:00
74	6	10	06:43:00
108	7	10	07:18:00
116	9	10	07:39:00
63	10	10	06:32:00
117	11	10	07:52:00
64	12	10	06:36:00
43	13	10	06:11:00
29	14	10	05:58:00
56	16	10	06:24:00
8	17	10	05:35:00
61	18	10	06:30:00
22	19	10	05:52:00
48	20	10	06:18:00
81	21	10	06:48:00
79	23	10	06:46:00
92	24	10	06:56:00
111	25	10	07:22:00
66	26	10	06:38:00
34	27	10	06:02:00
40	28	10	06:07:00
113	29	10	07:25:00
14	30	10	05:44:00
24	31	10	05:54:00
38	32	10	06:04:00
7	33	10	05:34:00
38	34	10	06:04:00
51	36	10	06:21:00
52	37	10	06:22:00
89	38	10	06:55:00
55	39	10	06:23:00
86	40	10	06:52:00
4	41	10	05:32:00
97	42	10	07:04:00
77	43	10	06:44:00
9	44	10	05:36:00
72	45	10	06:42:00
12	46	10	05:39:00
56	47	10	06:24:00
72	48	10	06:42:00
104	49	10	07:11:00
21	50	10	05:51:00
4	52	10	05:32:00
107	53	10	07:15:00
34	54	10	06:02:00
110	55	10	07:20:00
105	56	10	07:12:00
60	57	10	06:26:00
40	58	10	06:07:00
89	59	10	06:55:00
94	60	10	07:00:00
71	61	10	06:40:00
1	62	10	05:26:00
22	63	10	05:52:00
86	64	10	06:52:00
17	65	10	05:47:00
100	66	10	07:06:00
10	67	10	05:37:00
95	68	10	07:02:00
31	69	10	05:59:00
29	70	10	05:58:00
19	71	10	05:50:00
2	72	10	05:29:00
27	73	10	05:57:00
56	74	10	06:24:00
101	75	10	07:07:00
49	76	10	06:19:00
74	77	10	06:43:00
113	78	10	07:25:00
82	80	10	06:49:00
68	81	10	06:39:00
65	82	10	06:37:00
27	83	10	05:57:00
61	84	10	06:30:00
19	85	10	05:50:00
82	86	10	06:49:00
68	87	10	06:39:00
84	88	10	06:50:00
10	89	10	05:37:00
101	90	10	07:07:00
18	92	10	05:48:00
95	93	10	07:02:00
42	95	10	06:10:00
115	96	10	07:36:00
52	97	10	06:22:00
46	98	10	06:14:00
112	99	10	07:24:00
4	100	10	05:32:00
78	101	10	06:45:00
26	102	10	05:56:00
43	103	10	06:11:00
88	104	10	06:54:00
3	105	10	05:30:00
103	106	10	07:08:00
14	108	10	05:44:00
98	109	10	07:05:00
34	110	10	06:02:00
93	111	10	06:59:00
13	112	10	05:41:00
47	113	10	06:16:00
66	114	10	06:38:00
52	115	10	06:22:00
45	116	10	06:13:00
32	117	10	06:00:00
106	118	10	07:14:00
14	119	10	05:44:00
68	120	10	06:39:00
74	121	10	06:43:00
98	122	10	07:05:00
108	123	10	07:18:00
34	124	10	06:02:00
89	125	10	06:55:00
56	126	10	06:24:00
67	1	11	05:49:00
52	2	11	05:25:00
105	3	11	07:32:00
3	4	11	04:11:00
23	5	11	04:40:00
113	6	11	08:04:00
64	7	11	05:47:00
83	9	11	06:12:00
21	10	11	04:38:00
45	11	11	05:08:00
84	12	11	06:13:00
40	13	11	05:04:00
4	14	11	04:15:00
56	16	11	05:28:00
115	17	11	08:25:00
73	18	11	05:58:00
98	19	11	07:05:00
21	20	11	04:38:00
37	21	11	04:55:00
80	23	11	06:07:00
19	24	11	04:36:00
116	25	11	08:34:00
24	26	11	04:41:00
29	27	11	04:46:00
7	28	11	04:23:00
86	29	11	06:19:00
28	30	11	04:44:00
49	31	11	05:16:00
62	32	11	05:44:00
111	33	11	07:55:00
75	34	11	06:01:00
48	36	11	05:12:00
69	37	11	05:50:00
9	38	11	04:26:00
13	39	11	04:31:00
50	40	11	05:17:00
104	41	11	07:28:00
11	42	11	04:30:00
71	43	11	05:55:00
80	44	11	06:07:00
46	45	11	05:09:00
31	46	11	04:47:00
40	47	11	05:04:00
103	48	11	07:18:00
59	49	11	05:39:00
11	50	11	04:30:00
76	52	11	06:02:00
18	53	11	04:35:00
67	54	11	05:49:00
54	55	11	05:26:00
85	56	11	06:16:00
24	57	11	04:41:00
71	58	11	05:55:00
59	59	11	05:39:00
97	60	11	07:00:00
109	61	11	07:46:00
7	62	11	04:23:00
107	63	11	07:40:00
29	64	11	04:46:00
64	65	11	05:47:00
77	66	11	06:05:00
102	67	11	07:14:00
63	68	11	05:46:00
26	69	11	04:43:00
42	70	11	05:05:00
108	71	11	07:41:00
16	72	11	04:33:00
100	73	11	07:12:00
26	74	11	04:43:00
91	75	11	06:41:00
43	76	11	05:06:00
93	77	11	06:50:00
61	78	11	05:40:00
90	80	11	06:34:00
106	81	11	07:34:00
64	82	11	05:47:00
10	83	11	04:29:00
2	84	11	04:08:00
87	85	11	06:22:00
70	86	11	05:53:00
35	87	11	04:54:00
51	88	11	05:21:00
32	89	11	04:50:00
20	90	11	04:37:00
88	92	11	06:25:00
6	93	11	04:21:00
13	95	11	04:31:00
98	96	11	07:05:00
101	97	11	07:13:00
57	98	11	05:29:00
92	99	11	06:49:00
52	100	11	05:25:00
78	101	11	06:06:00
16	102	11	04:33:00
43	103	11	05:06:00
114	104	11	08:07:00
35	105	11	04:54:00
95	106	11	06:58:00
74	108	11	05:59:00
5	109	11	04:19:00
78	110	11	06:06:00
1	111	11	04:07:00
13	112	11	04:31:00
88	113	11	06:25:00
95	114	11	06:58:00
55	115	11	05:27:00
58	116	11	05:35:00
38	117	11	04:59:00
47	118	11	05:10:00
32	119	11	04:50:00
82	120	11	06:08:00
117	121	11	08:40:00
110	122	11	07:51:00
34	123	11	04:51:00
94	124	11	06:52:00
39	125	11	05:03:00
112	126	11	08:00:00
59	1	12	04:44:00
40	2	12	04:18:00
114	3	12	07:09:00
17	4	12	03:52:00
36	5	12	04:12:00
108	6	12	06:41:00
66	7	12	04:53:00
89	9	12	05:37:00
2	10	12	03:37:00
35	11	12	04:10:00
67	12	12	04:56:00
42	13	12	04:21:00
11	14	12	03:45:00
62	16	12	04:47:00
109	17	12	06:48:00
79	18	12	05:15:00
93	19	12	05:49:00
9	20	12	03:42:00
42	21	12	04:21:00
80	23	12	05:18:00
19	24	12	03:55:00
116	25	12	07:26:00
20	26	12	03:56:00
24	27	12	03:58:00
7	28	12	03:41:00
82	29	12	05:22:00
26	30	12	04:03:00
46	31	12	04:28:00
53	32	12	04:36:00
105	33	12	06:35:00
76	34	12	05:06:00
48	36	12	04:31:00
87	37	12	05:29:00
1	38	12	03:35:00
7	39	12	03:41:00
44	40	12	04:24:00
106	41	12	06:36:00
26	42	12	04:03:00
78	43	12	05:13:00
75	44	12	05:05:00
45	45	12	04:27:00
20	46	12	03:56:00
39	47	12	04:16:00
101	48	12	06:18:00
58	49	12	04:42:00
5	50	12	03:40:00
70	52	12	05:02:00
15	53	12	03:50:00
71	54	12	05:03:00
56	55	12	04:41:00
71	56	12	05:03:00
41	57	12	04:19:00
73	58	12	05:04:00
69	59	12	05:01:00
102	60	12	06:19:00
98	61	12	06:05:00
4	62	12	03:39:00
110	63	12	06:50:00
29	64	12	04:05:00
60	65	12	04:46:00
90	66	12	05:42:00
97	67	12	06:04:00
73	68	12	05:04:00
26	69	12	04:03:00
55	70	12	04:38:00
104	71	12	06:22:00
14	72	12	03:48:00
102	73	12	06:19:00
29	74	12	04:05:00
94	75	12	05:50:00
34	76	12	04:09:00
94	77	12	05:50:00
48	78	12	04:31:00
80	80	12	05:18:00
107	81	12	06:39:00
76	82	12	05:06:00
25	83	12	04:00:00
3	84	12	03:38:00
86	85	12	05:28:00
51	87	12	04:33:00
33	88	12	04:07:00
10	89	12	03:43:00
32	90	12	04:06:00
83	92	12	05:25:00
11	93	12	03:45:00
37	95	12	04:13:00
91	96	12	05:45:00
99	97	12	06:08:00
50	98	12	04:32:00
92	99	12	05:47:00
64	100	12	04:48:00
65	101	12	04:49:00
23	102	12	03:57:00
37	103	12	04:13:00
115	104	12	07:13:00
5	105	12	03:40:00
83	106	12	05:25:00
68	108	12	04:59:00
11	109	12	03:45:00
88	110	12	05:31:00
16	111	12	03:51:00
18	112	12	03:53:00
85	113	12	05:26:00
96	114	12	05:52:00
56	115	12	04:41:00
52	116	12	04:34:00
54	117	12	04:37:00
62	118	12	04:47:00
29	119	12	04:05:00
60	120	12	04:46:00
111	121	12	07:00:00
112	122	12	07:03:00
20	123	12	03:56:00
100	124	12	06:15:00
47	125	12	04:30:00
113	126	12	07:04:00
89	1	13	06:32:00
24	3	13	05:33:00
27	4	13	05:36:00
26	5	13	05:35:00
85	6	13	06:28:00
91	7	13	06:33:00
105	9	13	06:50:00
72	10	13	06:15:00
111	11	13	07:05:00
35	12	13	05:48:00
37	13	13	05:53:00
24	14	13	05:33:00
79	16	13	06:23:00
2	17	13	05:09:00
59	18	13	06:04:00
13	19	13	05:26:00
54	20	13	06:01:00
101	21	13	06:44:00
47	23	13	05:58:00
88	24	13	06:31:00
96	25	13	06:37:00
34	26	13	05:43:00
45	27	13	05:57:00
1	28	13	05:08:00
89	29	13	06:32:00
3	30	13	05:12:00
29	31	13	05:40:00
75	32	13	06:19:00
18	33	13	05:29:00
58	34	13	06:03:00
69	36	13	06:11:00
74	37	13	06:18:00
81	38	13	06:26:00
54	39	13	06:01:00
70	40	13	06:13:00
11	41	13	05:22:00
86	42	13	06:29:00
41	43	13	05:55:00
4	44	13	05:14:00
81	45	13	06:26:00
6	46	13	05:15:00
65	47	13	06:08:00
83	48	13	06:27:00
87	49	13	06:30:00
18	50	13	05:29:00
9	52	13	05:19:00
91	53	13	06:33:00
50	54	13	06:00:00
95	55	13	06:34:00
104	56	13	06:48:00
12	57	13	05:25:00
29	58	13	05:40:00
68	59	13	06:10:00
83	60	13	06:27:00
50	61	13	06:00:00
22	62	13	05:32:00
17	63	13	05:28:00
98	64	13	06:38:00
37	65	13	05:53:00
110	66	13	07:02:00
37	67	13	05:53:00
75	68	13	06:19:00
32	69	13	05:42:00
15	70	13	05:27:00
15	71	13	05:27:00
31	72	13	05:41:00
18	73	13	05:29:00
50	74	13	06:00:00
103	75	13	06:46:00
54	76	13	06:01:00
43	77	13	05:56:00
101	78	13	06:44:00
73	80	13	06:17:00
77	81	13	06:20:00
78	82	13	06:22:00
28	83	13	05:38:00
45	84	13	05:57:00
47	85	13	05:58:00
36	87	13	05:49:00
91	88	13	06:33:00
4	89	13	05:14:00
99	90	13	06:41:00
63	92	13	06:07:00
99	93	13	06:41:00
65	95	13	06:08:00
106	96	13	06:54:00
22	97	13	05:32:00
41	98	13	05:55:00
71	99	13	06:14:00
21	100	13	05:30:00
43	101	13	05:56:00
50	102	13	06:00:00
47	103	13	05:58:00
109	104	13	07:01:00
8	105	13	05:17:00
108	106	13	06:58:00
13	108	13	05:26:00
113	109	13	07:20:00
32	110	13	05:42:00
107	111	13	06:55:00
10	112	13	05:20:00
54	113	13	06:01:00
62	114	13	06:06:00
96	115	13	06:37:00
59	116	13	06:04:00
63	117	13	06:07:00
114	118	13	07:25:00
6	119	13	05:15:00
91	120	13	06:33:00
80	121	13	06:24:00
114	122	13	07:25:00
65	123	13	06:08:00
37	124	13	05:53:00
112	125	13	07:14:00
59	126	13	06:04:00
67	1	14	06:43:00
103	3	14	08:47:00
1	4	14	04:47:00
35	5	14	05:43:00
115	6	14	10:24:00
60	7	14	06:31:00
81	9	14	07:27:00
10	10	14	05:05:00
43	11	14	06:03:00
72	12	14	06:51:00
49	13	14	06:08:00
31	14	14	05:34:00
62	16	14	06:33:00
109	17	14	09:27:00
74	18	14	07:01:00
95	19	14	08:16:00
18	20	14	05:23:00
34	21	14	05:42:00
71	23	14	06:50:00
36	24	14	05:45:00
113	25	14	10:15:00
19	26	14	05:24:00
27	27	14	05:31:00
14	28	14	05:09:00
92	29	14	08:05:00
41	30	14	05:57:00
38	31	14	05:54:00
55	32	14	06:25:00
96	33	14	08:26:00
76	34	14	07:03:00
46	36	14	06:06:00
69	37	14	06:48:00
24	38	14	05:27:00
4	39	14	05:00:00
29	40	14	05:32:00
102	41	14	08:41:00
22	42	14	05:25:00
66	43	14	06:42:00
72	44	14	06:51:00
38	45	14	05:54:00
22	46	14	05:25:00
33	47	14	05:38:00
100	48	14	08:32:00
54	49	14	06:21:00
6	50	14	05:01:00
84	52	14	07:41:00
3	53	14	04:56:00
74	54	14	07:01:00
60	55	14	06:31:00
83	56	14	07:35:00
25	57	14	05:28:00
65	58	14	06:41:00
57	59	14	06:27:00
98	60	14	08:30:00
103	61	14	08:47:00
4	62	14	05:00:00
106	63	14	08:49:00
32	64	14	05:37:00
64	65	14	06:39:00
79	66	14	07:11:00
90	67	14	08:03:00
80	68	14	07:15:00
6	69	14	05:01:00
42	70	14	06:00:00
103	71	14	08:47:00
17	72	14	05:19:00
85	73	14	07:46:00
11	74	14	05:07:00
99	75	14	08:31:00
51	76	14	06:10:00
88	77	14	07:56:00
50	78	14	06:09:00
93	80	14	08:12:00
108	81	14	08:55:00
78	82	14	07:05:00
27	83	14	05:31:00
6	84	14	05:01:00
87	85	14	07:48:00
37	87	14	05:52:00
56	88	14	06:26:00
9	89	14	05:02:00
45	90	14	06:05:00
90	92	14	08:03:00
12	93	14	05:08:00
30	95	14	05:33:00
86	96	14	07:47:00
94	97	14	08:15:00
58	98	14	06:28:00
107	99	14	08:53:00
58	100	14	06:28:00
63	101	14	06:37:00
19	102	14	05:24:00
44	103	14	06:04:00
111	104	14	09:39:00
19	105	14	05:24:00
89	106	14	07:59:00
68	108	14	06:44:00
26	109	14	05:29:00
77	110	14	07:04:00
2	111	14	04:53:00
16	112	14	05:13:00
82	113	14	07:31:00
97	114	14	08:29:00
47	115	14	06:07:00
53	116	14	06:16:00
47	117	14	06:07:00
52	118	14	06:15:00
15	119	14	05:12:00
69	120	14	06:48:00
113	121	14	10:15:00
112	122	14	09:52:00
12	123	14	05:08:00
101	124	14	08:33:00
40	125	14	05:55:00
110	126	14	09:36:00
59	1	15	07:35:00
21	3	15	06:51:00
28	4	15	07:00:00
22	5	15	06:56:00
84	6	15	08:04:00
103	7	15	08:38:00
112	9	15	09:01:00
46	10	15	07:18:00
102	11	15	08:35:00
45	12	15	07:17:00
37	13	15	07:08:00
17	14	15	06:48:00
75	16	15	07:54:00
3	17	15	06:20:00
89	18	15	08:09:00
57	19	15	07:27:00
50	20	15	07:20:00
78	21	15	07:55:00
88	23	15	08:07:00
80	24	15	07:57:00
110	25	15	08:52:00
32	26	15	07:03:00
55	27	15	07:24:00
7	28	15	06:33:00
107	29	15	08:43:00
11	30	15	06:41:00
52	32	15	07:21:00
3	33	15	06:20:00
47	34	15	07:19:00
39	36	15	07:09:00
95	37	15	08:17:00
58	38	15	07:28:00
52	39	15	07:21:00
101	40	15	08:32:00
13	41	15	06:42:00
70	42	15	07:46:00
59	43	15	07:35:00
28	44	15	07:00:00
92	45	15	08:13:00
2	46	15	06:19:00
82	47	15	08:00:00
99	48	15	08:22:00
72	49	15	07:51:00
5	50	15	06:22:00
9	52	15	06:38:00
97	53	15	08:18:00
25	54	15	06:59:00
98	55	15	08:19:00
95	56	15	08:17:00
17	57	15	06:48:00
14	58	15	06:47:00
85	59	15	08:05:00
87	60	15	08:06:00
35	61	15	07:07:00
9	62	15	06:38:00
34	63	15	07:04:00
85	64	15	08:05:00
42	65	15	07:12:00
92	66	15	08:13:00
32	67	15	07:03:00
94	68	15	08:16:00
25	69	15	06:59:00
17	70	15	06:48:00
14	71	15	06:47:00
40	72	15	07:10:00
22	73	15	06:56:00
37	74	15	07:08:00
109	75	15	08:48:00
83	76	15	08:02:00
35	77	15	07:07:00
112	78	15	09:01:00
47	80	15	07:19:00
43	81	15	07:15:00
63	82	15	07:37:00
50	83	15	07:20:00
66	84	15	07:40:00
70	85	15	07:46:00
69	87	15	07:45:00
105	88	15	08:40:00
30	89	15	07:02:00
78	90	15	07:55:00
63	92	15	07:37:00
91	93	15	08:11:00
22	95	15	06:56:00
108	96	15	08:44:00
41	97	15	07:11:00
59	98	15	07:35:00
72	99	15	07:51:00
1	100	15	06:18:00
75	101	15	07:54:00
55	102	15	07:24:00
68	103	15	07:43:00
81	104	15	07:59:00
14	105	15	06:47:00
89	106	15	08:09:00
11	108	15	06:41:00
104	109	15	08:39:00
44	110	15	07:16:00
110	111	15	08:52:00
6	112	15	06:30:00
75	113	15	07:54:00
30	114	15	07:02:00
59	115	15	07:35:00
54	116	15	07:23:00
20	117	15	06:50:00
114	118	15	09:15:00
8	119	15	06:34:00
66	120	15	07:40:00
74	121	15	07:53:00
106	122	15	08:42:00
65	123	15	07:38:00
47	124	15	07:19:00
100	125	15	08:25:00
25	126	15	06:59:00
9	1	16	05:28:00
44	3	16	05:51:00
31	4	16	05:43:00
39	5	16	05:49:00
33	6	16	05:45:00
48	7	16	05:53:00
20	9	16	05:39:00
12	10	16	05:33:00
69	11	16	06:01:00
53	12	16	05:54:00
75	13	16	06:04:00
83	14	16	06:12:00
24	16	16	05:40:00
97	17	16	06:22:00
105	18	16	06:32:00
62	19	16	05:58:00
4	20	16	05:19:00
11	21	16	05:32:00
94	23	16	06:20:00
2	24	16	05:17:00
107	25	16	06:36:00
16	26	16	05:35:00
87	27	16	06:14:00
112	28	16	06:53:00
48	29	16	05:53:00
104	30	16	06:31:00
39	32	16	05:49:00
59	33	16	05:57:00
83	34	16	06:12:00
36	36	16	05:48:00
88	37	16	06:15:00
62	38	16	05:58:00
77	39	16	06:09:00
88	40	16	06:15:00
83	41	16	06:12:00
24	42	16	05:40:00
100	43	16	06:25:00
101	44	16	06:26:00
17	45	16	05:37:00
36	46	16	05:48:00
12	47	16	05:33:00
110	48	16	06:45:00
18	49	16	05:38:00
7	50	16	05:25:00
109	52	16	06:44:00
80	53	16	06:11:00
24	54	16	05:40:00
5	55	16	05:22:00
3	56	16	05:18:00
80	57	16	06:11:00
24	58	16	05:40:00
42	59	16	05:50:00
29	60	16	05:41:00
48	61	16	05:53:00
62	62	16	05:58:00
53	63	16	05:54:00
94	64	16	06:20:00
92	65	16	06:18:00
78	66	16	06:10:00
88	67	16	06:15:00
111	68	16	06:47:00
34	69	16	05:46:00
67	70	16	05:59:00
71	71	16	06:02:00
71	72	16	06:02:00
96	73	16	06:21:00
68	74	16	06:00:00
92	75	16	06:18:00
80	76	16	06:11:00
1	77	16	05:13:00
105	78	16	06:32:00
20	80	16	05:39:00
10	81	16	05:29:00
88	82	16	06:15:00
29	83	16	05:41:00
53	84	16	05:54:00
99	85	16	06:24:00
46	87	16	05:52:00
57	88	16	05:56:00
71	89	16	06:02:00
69	90	16	06:01:00
12	92	16	05:33:00
59	93	16	05:57:00
102	95	16	06:27:00
6	96	16	05:23:00
62	97	16	05:58:00
48	98	16	05:53:00
36	99	16	05:48:00
20	100	16	05:39:00
44	101	16	05:51:00
83	102	16	06:12:00
12	103	16	05:33:00
103	104	16	06:28:00
31	105	16	05:43:00
108	106	16	06:40:00
98	108	16	06:23:00
18	110	16	05:38:00
113	111	16	06:55:00
57	112	16	05:56:00
62	113	16	05:58:00
76	114	16	06:06:00
35	115	16	05:47:00
46	116	16	05:52:00
59	117	16	05:57:00
39	118	16	05:49:00
42	119	16	05:50:00
78	120	16	06:10:00
48	121	16	05:53:00
74	122	16	06:03:00
56	123	16	05:55:00
20	124	16	05:39:00
8	125	16	05:26:00
24	126	16	05:40:00
56	1	17	08:16:00
14	3	17	07:20:00
44	4	17	07:58:00
60	5	17	08:19:00
81	6	17	08:36:00
108	7	17	09:38:00
110	9	17	09:59:00
23	10	17	07:37:00
109	11	17	09:58:00
47	12	17	08:01:00
41	13	17	07:53:00
42	14	17	07:55:00
92	16	17	08:53:00
34	17	17	07:49:00
92	18	17	08:53:00
17	19	17	07:29:00
34	20	17	07:49:00
49	21	17	08:03:00
51	23	17	08:06:00
74	24	17	08:30:00
100	25	17	09:17:00
71	26	17	08:27:00
69	27	17	08:26:00
3	28	17	06:56:00
90	29	17	08:51:00
1	30	17	06:50:00
84	32	17	08:45:00
6	33	17	07:07:00
37	34	17	07:50:00
80	36	17	08:34:00
53	37	17	08:13:00
90	38	17	08:51:00
37	39	17	07:50:00
95	40	17	08:59:00
17	41	17	07:29:00
94	42	17	08:56:00
66	43	17	08:25:00
4	44	17	07:02:00
69	45	17	08:26:00
14	46	17	07:20:00
56	47	17	08:16:00
82	48	17	08:37:00
86	49	17	08:47:00
12	50	17	07:17:00
13	52	17	07:19:00
79	53	17	08:33:00
65	54	17	08:23:00
104	55	17	09:32:00
105	56	17	09:34:00
25	57	17	07:38:00
19	58	17	07:32:00
72	59	17	08:28:00
66	60	17	08:25:00
56	61	17	08:16:00
27	62	17	07:44:00
28	63	17	07:45:00
88	64	17	08:50:00
21	65	17	07:34:00
45	67	17	08:00:00
77	68	17	08:32:00
56	69	17	08:16:00
4	70	17	07:02:00
1	71	17	06:50:00
20	72	17	07:33:00
7	73	17	07:11:00
26	74	17	07:40:00
101	75	17	09:23:00
50	76	17	08:05:00
28	77	17	07:45:00
106	78	17	09:35:00
34	80	17	07:49:00
52	81	17	08:07:00
97	82	17	09:02:00
55	83	17	08:15:00
87	84	17	08:48:00
30	85	17	07:46:00
33	87	17	07:47:00
21	89	17	07:34:00
85	90	17	08:46:00
60	92	17	08:19:00
54	93	17	08:14:00
43	95	17	07:57:00
96	96	17	09:01:00
45	97	17	08:00:00
66	98	17	08:25:00
74	99	17	08:30:00
7	100	17	07:11:00
40	101	17	07:52:00
9	102	17	07:12:00
30	103	17	07:46:00
99	104	17	09:05:00
9	105	17	07:12:00
88	106	17	08:50:00
11	108	17	07:15:00
103	111	17	09:29:00
37	112	17	07:50:00
48	113	17	08:02:00
30	114	17	07:46:00
83	115	17	08:40:00
23	116	17	07:37:00
63	117	17	08:21:00
102	118	17	09:26:00
16	119	17	07:22:00
77	120	17	08:32:00
60	121	17	08:19:00
97	122	17	09:02:00
76	123	17	08:31:00
64	124	17	08:22:00
107	125	17	09:36:00
72	126	17	08:28:00
66	1	18	04:55:00
24	3	18	04:27:00
17	4	18	04:21:00
42	5	18	04:36:00
101	6	18	05:39:00
96	7	18	05:29:00
105	9	18	05:44:00
71	10	18	04:59:00
109	11	18	05:51:00
61	12	18	04:52:00
19	13	18	04:24:00
36	14	18	04:32:00
54	16	18	04:46:00
3	17	18	04:08:00
42	18	18	04:36:00
38	19	18	04:33:00
69	21	18	04:56:00
61	23	18	04:52:00
80	24	18	05:08:00
102	25	18	05:40:00
59	26	18	04:49:00
49	27	18	04:39:00
16	28	18	04:20:00
96	29	18	05:29:00
13	30	18	04:16:00
71	32	18	04:59:00
30	33	18	04:30:00
42	34	18	04:36:00
40	36	18	04:34:00
88	37	18	05:15:00
53	38	18	04:45:00
66	39	18	04:55:00
92	40	18	05:23:00
5	41	18	04:09:00
79	42	18	05:06:00
50	43	18	04:41:00
10	44	18	04:14:00
75	45	18	05:04:00
22	46	18	04:26:00
54	47	18	04:46:00
91	48	18	05:22:00
92	49	18	05:23:00
32	50	18	04:31:00
3	52	18	04:08:00
89	53	18	05:18:00
24	54	18	04:27:00
74	55	18	05:03:00
104	56	18	05:41:00
51	57	18	04:42:00
15	58	18	04:18:00
75	59	18	05:04:00
63	60	18	04:53:00
22	61	18	04:26:00
9	62	18	04:13:00
6	63	18	04:11:00
96	64	18	05:29:00
59	65	18	04:49:00
30	67	18	04:30:00
80	68	18	05:08:00
19	69	18	04:24:00
46	70	18	04:37:00
28	71	18	04:29:00
32	72	18	04:31:00
8	73	18	04:12:00
10	74	18	04:14:00
99	75	18	05:32:00
52	76	18	04:43:00
54	77	18	04:46:00
108	78	18	05:47:00
46	80	18	04:37:00
24	81	18	04:27:00
63	82	18	04:53:00
57	83	18	04:47:00
84	84	18	05:11:00
21	85	18	04:25:00
32	87	18	04:31:00
1	89	18	04:04:00
87	90	18	05:13:00
41	92	18	04:35:00
77	93	18	05:05:00
38	95	18	04:33:00
105	96	18	05:44:00
42	97	18	04:36:00
83	98	18	05:09:00
69	99	18	04:56:00
6	100	18	04:11:00
46	101	18	04:37:00
13	102	18	04:16:00
66	103	18	04:55:00
95	104	18	05:27:00
10	105	18	04:14:00
94	106	18	05:25:00
27	108	18	04:28:00
85	111	18	05:12:00
2	112	18	04:07:00
73	113	18	05:02:00
77	114	18	05:05:00
58	115	18	04:48:00
18	116	18	04:22:00
28	117	18	04:29:00
100	118	18	05:35:00
32	119	18	04:31:00
85	120	18	05:12:00
80	121	18	05:08:00
107	122	18	05:45:00
90	123	18	05:19:00
65	124	18	04:54:00
102	125	18	05:40:00
36	126	18	04:32:00
82	1	19	05:02:00
3	3	19	04:15:00
49	4	19	04:42:00
73	5	19	04:56:00
93	6	19	05:14:00
86	7	19	05:06:00
107	9	19	05:50:00
69	10	19	04:54:00
101	11	19	05:30:00
57	12	19	04:46:00
17	13	19	04:23:00
44	14	19	04:40:00
73	16	19	04:56:00
6	17	19	04:18:00
35	18	19	04:35:00
8	19	19	04:19:00
59	21	19	04:47:00
44	23	19	04:40:00
99	24	19	05:28:00
97	25	19	05:25:00
30	26	19	04:32:00
66	27	19	04:52:00
1	28	19	04:01:00
99	29	19	05:28:00
2	30	19	04:06:00
51	32	19	04:43:00
3	33	19	04:15:00
61	34	19	04:48:00
79	36	19	04:59:00
76	37	19	04:57:00
57	38	19	04:46:00
17	39	19	04:23:00
86	40	19	05:06:00
11	41	19	04:21:00
43	42	19	04:39:00
59	43	19	04:47:00
9	44	19	04:20:00
95	45	19	05:21:00
22	46	19	04:26:00
78	47	19	04:58:00
91	48	19	05:11:00
84	49	19	05:05:00
23	50	19	04:27:00
23	52	19	04:27:00
68	53	19	04:53:00
70	54	19	04:55:00
89	55	19	05:07:00
106	56	19	05:48:00
40	57	19	04:38:00
11	58	19	04:21:00
64	59	19	04:50:00
73	60	19	04:56:00
32	61	19	04:34:00
20	62	19	04:25:00
32	63	19	04:34:00
70	64	19	04:55:00
56	65	19	04:45:00
48	67	19	04:41:00
79	68	19	04:59:00
30	69	19	04:32:00
25	70	19	04:28:00
11	71	19	04:21:00
27	72	19	04:31:00
9	73	19	04:20:00
40	74	19	04:38:00
104	75	19	05:39:00
66	76	19	04:52:00
38	77	19	04:37:00
104	78	19	05:39:00
49	80	19	04:42:00
51	81	19	04:43:00
90	82	19	05:09:00
20	83	19	04:25:00
61	84	19	04:48:00
51	85	19	04:43:00
55	87	19	04:44:00
5	89	19	04:16:00
86	90	19	05:06:00
32	92	19	04:34:00
81	93	19	05:00:00
11	95	19	04:21:00
94	96	19	05:19:00
37	97	19	04:36:00
44	98	19	04:40:00
98	99	19	05:27:00
6	100	19	04:18:00
35	101	19	04:35:00
40	102	19	04:38:00
64	103	19	04:50:00
95	104	19	05:21:00
27	105	19	04:31:00
84	106	19	05:05:00
17	108	19	04:23:00
103	111	19	05:38:00
15	112	19	04:22:00
44	113	19	04:40:00
51	114	19	04:43:00
92	115	19	05:13:00
70	116	19	04:55:00
38	117	19	04:37:00
107	118	19	05:50:00
15	119	19	04:22:00
83	120	19	05:04:00
76	121	19	04:57:00
109	122	19	05:55:00
63	123	19	04:49:00
25	124	19	04:28:00
102	125	19	05:36:00
27	126	19	04:31:00
9	1	20	07:36:00
82	3	20	08:40:00
15	4	20	07:43:00
32	5	20	07:55:00
66	6	20	08:26:00
40	7	20	08:03:00
54	9	20	08:14:00
57	10	20	08:18:00
80	11	20	08:38:00
21	12	20	07:48:00
46	13	20	08:07:00
74	14	20	08:32:00
1	16	20	07:11:00
83	17	20	08:41:00
88	18	20	08:45:00
63	19	20	08:24:00
46	21	20	08:07:00
66	23	20	08:26:00
17	24	20	07:46:00
45	25	20	08:06:00
11	26	20	07:39:00
61	27	20	08:21:00
104	28	20	09:14:00
28	29	20	07:52:00
95	30	20	09:01:00
37	32	20	08:00:00
4	33	20	07:24:00
61	34	20	08:21:00
19	36	20	07:47:00
84	37	20	08:42:00
95	38	20	09:01:00
77	39	20	08:34:00
38	40	20	08:01:00
103	41	20	09:13:00
90	42	20	08:47:00
60	43	20	08:20:00
63	44	20	08:24:00
19	45	20	07:47:00
98	46	20	09:02:00
10	47	20	07:37:00
107	48	20	09:19:00
29	49	20	07:54:00
2	50	20	07:16:00
89	52	20	08:46:00
72	53	20	08:31:00
78	54	20	08:36:00
13	55	20	07:42:00
34	56	20	07:59:00
106	57	20	09:17:00
75	58	20	08:33:00
17	59	20	07:46:00
75	60	20	08:33:00
71	61	20	08:30:00
50	62	20	08:09:00
38	63	20	08:01:00
101	64	20	09:09:00
78	65	20	08:36:00
93	67	20	08:52:00
105	68	20	09:16:00
68	69	20	08:27:00
40	70	20	08:03:00
43	71	20	08:04:00
99	72	20	09:05:00
102	73	20	09:10:00
63	74	20	08:24:00
108	75	20	09:27:00
80	76	20	08:38:00
24	77	20	07:49:00
92	78	20	08:51:00
5	80	20	07:27:00
40	81	20	08:03:00
34	82	20	07:59:00
51	83	20	08:10:00
95	84	20	09:01:00
84	85	20	08:42:00
25	87	20	07:50:00
94	89	20	09:00:00
29	90	20	07:54:00
6	92	20	07:30:00
91	93	20	08:48:00
56	95	20	08:17:00
34	96	20	07:59:00
70	97	20	08:28:00
21	98	20	07:48:00
52	99	20	08:13:00
16	100	20	07:45:00
33	101	20	07:56:00
26	102	20	07:51:00
21	103	20	07:48:00
72	104	20	08:31:00
29	105	20	07:54:00
100	106	20	09:07:00
87	108	20	08:44:00
109	111	20	09:28:00
57	112	20	08:18:00
52	113	20	08:13:00
68	114	20	08:27:00
43	115	20	08:04:00
26	116	20	07:51:00
49	117	20	08:08:00
8	118	20	07:34:00
3	119	20	07:22:00
46	120	20	08:07:00
13	121	20	07:42:00
84	122	20	08:42:00
7	123	20	07:32:00
57	124	20	08:18:00
12	125	20	07:41:00
55	126	20	08:15:00
\.


--
-- Data for Name: stage; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY stage (idstage, datestage, idclass, distance, idstart, idend) FROM stdin;
1	2058-07-01	1	170	1	2
2	2058-07-02	1	303	2	3
3	2058-07-03	3	1168	3	7
4	2058-07-04	1	209	7	9
5	2058-07-05	2	769	9	8
6	2058-07-06	1	438	8	6
7	2058-07-07	2	607	6	4
8	2058-07-08	2	528	4	5
9	2058-07-10	3	1656	5	11
10	2058-07-11	2	678	11	13
11	2058-07-12	1	253	13	16
12	2058-07-13	1	214	16	15
13	2058-07-14	2	643	15	18
14	2058-07-15	1	296	18	21
15	2058-07-16	2	792	21	20
16	2058-07-18	3	1051	20	17
17	2058-07-19	2	859	17	19
18	2058-07-20	2	504	19	14
19	2058-07-21	2	502	14	12
20	2058-07-22	3	1453	12	10
\.


--
-- Name: stage_idstage_seq; Type: SEQUENCE SET; Schema: eurotour; Owner: -
--

SELECT pg_catalog.setval('stage_idstage_seq', 21, false);


--
-- Data for Name: team; Type: TABLE DATA; Schema: eurotour; Owner: -
--

COPY team (idteam, name, idcountry) FROM stdin;
DYN	Dynatronics Accessories	USA
EXS	Express services	FRA
SIN	Superior Interactive	NED
GAG	Gamma Gas	AUS
EMI	Eli Moore Inc	BEL
COP	Copacal	BRA
LAV	Lavazzi	ITA
BEL	Belgicom	BEL
LAC	Lacaso	ESP
POT	Pol telecom	POL
INC	Intrecom	GRE
DBX	Drepbox	IRL
SHK	Sharks	SWE
NOV	Novibase	POR
\.


--
-- Name: city_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (idcity);


--
-- Name: class_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY class
    ADD CONSTRAINT class_pkey PRIMARY KEY (idclass);


--
-- Name: competitor_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY competitor
    ADD CONSTRAINT competitor_pkey PRIMARY KEY (idcompetitor);


--
-- Name: country_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (idcountry);


--
-- Name: performance_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY performance
    ADD CONSTRAINT performance_pkey PRIMARY KEY (idcompetitor, idstage);


--
-- Name: stage_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stage
    ADD CONSTRAINT stage_pkey PRIMARY KEY (idstage);


--
-- Name: team_pkey; Type: CONSTRAINT; Schema: eurotour; Owner: -; Tablespace: 
--

ALTER TABLE ONLY team
    ADD CONSTRAINT team_pkey PRIMARY KEY (idteam);


--
-- Name: city_idcountry_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_idcountry_fkey FOREIGN KEY (idcountry) REFERENCES country(idcountry);


--
-- Name: competitor_idcountry_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY competitor
    ADD CONSTRAINT competitor_idcountry_fkey FOREIGN KEY (idcountry) REFERENCES country(idcountry);


--
-- Name: competitor_idteam_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY competitor
    ADD CONSTRAINT competitor_idteam_fkey FOREIGN KEY (idteam) REFERENCES team(idteam);


--
-- Name: performance_idcompetitor_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY performance
    ADD CONSTRAINT performance_idcompetitor_fkey FOREIGN KEY (idcompetitor) REFERENCES competitor(idcompetitor);


--
-- Name: performance_idstage_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY performance
    ADD CONSTRAINT performance_idstage_fkey FOREIGN KEY (idstage) REFERENCES stage(idstage);


--
-- Name: stage_idclass_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY stage
    ADD CONSTRAINT stage_idclass_fkey FOREIGN KEY (idclass) REFERENCES class(idclass);


--
-- Name: stage_idend_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY stage
    ADD CONSTRAINT stage_idend_fkey FOREIGN KEY (idend) REFERENCES city(idcity);


--
-- Name: stage_idstart_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY stage
    ADD CONSTRAINT stage_idstart_fkey FOREIGN KEY (idstart) REFERENCES city(idcity);


--
-- Name: team_idcountry_fkey; Type: FK CONSTRAINT; Schema: eurotour; Owner: -
--

ALTER TABLE ONLY team
    ADD CONSTRAINT team_idcountry_fkey FOREIGN KEY (idcountry) REFERENCES country(idcountry);


--
-- PostgreSQL database dump complete
--

