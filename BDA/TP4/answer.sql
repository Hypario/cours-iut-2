SET search_path TO eurotour;

/**
* 2.1
* 1
**/
CREATE OR REPLACE FUNCTION estFrancais(IN id INT) RETURNS boolean AS
$$
    DECLARE
        n varchar;
    BEGIN
        n = (SELECT co.name 
        FROM competitor c
        JOIN country co ON c.idCountry = co.idCountry
        WHERE idCompetitor = id);
        IF n = 'France' then
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END
$$
LANGUAGE plpgsql;

/**
* 3
**/
CREATE OR REPLACE FUNCTION listePoids(IN poidsTotal DECIMAL) RETURNS SETOF competitor AS
$$
    DECLARE
        c competitor;
        sumPoids DECIMAL = 0;
    BEGIN
        FOR c IN (SELECT * FROM competitor ORDER BY weight) LOOP
        EXIT WHEN sumPoids + c.weight > poidsTotal;
        sumPoids = sumPoids + c.weight;
        RETURN NEXT c;
        /**
        ORcRank
        sumPoids = sumPoids + c.weight;
            IF sumPoids < poidsTotal then
                RETURN NEXT c;
            ELSE
                RETURN;
            END IF;
        */
        END LOOP;
        return;
    END
$$
LANGUAGE plpgsql;

/**
* 2.2
* 1
**/
CREATE OR REPLACE FUNCTION classement(IN n INT, OUT r INT, OUT id INT, OUT sur VARCHAR, OUT given VARCHAR, OUT dur INTERVAL) RETURNS SETOF record AS
$$
    DECLARE
        cRank INT = 0;
    BEGIN
        FOR r, id, sur, given, dur IN (SELECT p.rank, c.idCompetitor, c.surname, c.givenName, p.duration 
            FROM competitor AS c
            JOIN performance AS p ON c.idCompetitor = p.idCompetitor 
            WHERE p.idStage = n 
            ORDER BY rank, c.surname, c.givenName) 
        LOOP
            IF cRank = r then
                r = null;
            ELSE
                cRank = r;
            END IF;
            RETURN NEXT;
        END LOOP;
        return;
    END
$$
LANGUAGE plpgsql;

/**
* 2
**/
CREATE OR REPLACE FUNCTION perf(IN idC INT, IN idS INT, IN d INTERVAL) RETURNS boolean as
$$
    BEGIN
        PERFORM 1 FROM competitor WHERE idCompetitor = idC;
        IF NOT found THEN RETURN false; END IF;
    
        PERFORM 1 FROM stage WHERE idStage = idS;
        IF NOT found THEN RETURN false; END IF;

        PERFORM 1 FROM performance WHERE idCompetitor = idC and idStage = idS;
        IF found THEN
            UPDATE performance
            SET duration = d
            WHERE idCompetitor = idC and idStage = idS;
        ELSE
            IF idS > 1 then
                PERFORM 1 FROM performance WHERE idCompetitor = idC AND idStage = idS-1;
                IF NOT found THEN RETURN FALSE; END IF;
            END IF;
            INSERT INTO performance VALUES (null, idC, idS, d)
        END IF;
        UPDATE performance p1
        SET RANK = (SELECT COUNT(*) FROM performance p2 WHERE p2.duration < p1.duration AND p1.idStage = p2.idStage)
        WHERE idStage = idS AND duration >= d;
        RETURN true;
    END
$$
LANGUAGE plpgsql;