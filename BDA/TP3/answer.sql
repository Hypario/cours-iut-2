SET search_path TO eurotour;

CREATE OR REPLACE FUNCTION f1(integer) RETURNS varchar AS
$$
    SELECT name
    FROM team
    WHERE idTeam = $1;
$$
LANGUAGE sql;