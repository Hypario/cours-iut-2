drop schema if exists commandes cascade;
create schema commandes;
set search_path to commandes;

create table article(
  idArticle serial primary key,
  nom varchar(50) not null,
  prix decimal(6,2)
);

create table client(
  idClient serial primary key,
  nom varchar(50) not null,
  email varchar(80),
  adresse varchar(250)
);

create table transporteur(
  idTransporteur serial primary key,
  nom varchar(50) not null,
  email varchar(80),
  adresse varchar(250)
);

create table commande(
  idCommande int primary key,
  dateCommande date default CURRENT_DATE,
  etat varchar(10) check (etat in ('COMMANDE','EN COURS','LIVRE')) default 'COMMANDE',
  idClient int references client(idClient)
);

create table livrer (
  idCommande int references commande(idCommande),
  idTransporteur int references transporteur(idTransporteur),
  dateLivraison date,
  primary key (idCommande,idTransporteur)
);

create table ligne(
  idLigne serial primary key,
  idCommande int references commande(idCommande),
  idArticle int references article(idArticle),
  quantite int
);

-- data
insert into article(nom, prix) values
  ('souris',10.5),
  ('clé',17.2),
  ('clavier',25);

insert into client(nom,email,adresse) values
  ('toto','toto@toto.fr','Lens'),
  ('toto','toto@titi.fr','Lens'),
  ('titi','titi@toto.fr','Carvin'),
  ('tutu','tutu@toto.fr','Liévin');

