SET search_path TO commandes;

DROP FUNCTION IF EXISTS f1(IN id INT, out nom varchar, out prix decimal);
create or replace function f1(IN idC INT, out nom varchar, out prix decimal, OUT quantite INT,
                              OUT prixLigne decimal) returns setof record as
$$
begin
    for nom, prix, quantite, prixLigne in (select a.nom, a.prix, l.quantite, l.quantite * a.prix
                                           from commande c
                                                    join ligne l on l.idcommande = c.idcommande
                                                    join article a on l.idarticle = a.idarticle
                                           where c.idcommande = idC)
        loop
            return next;
        end loop;
    return;
end;
$$
    language plpgsql;

create or replace function f2(IN idCmde INT, IN idCli INT, IN nomA VARCHAR, IN qte int) returns boolean as
$$
declare
    nb  int;
    idA int;
begin
    /* si l'id client existe pas */
    perform 1 from client where idclient = idCli;
    if (not FOUND) then
        return false;
    end if;

    /* si la commande existe pour un autre client */
    perform 1 from commande where idcommande = idCmde and idclient != idCli;
    if (FOUND) then
        return false;
    end if;

    /* si l'article n'existe pas ou qu'il y en a plusieurs on retourne false */
    select count(*)
    into nb
    from article
    where nom = nomA;
    if nb != 1 then return false; end if;

    /* si la commande n'existe pas on la crée pour le client */
    perform 1 from commande where idcommande = idCmde;
    if (not FOUND) then
        INSERT INTO commande VALUES (default, current_date, 'EN COURS', idCli);
    end if;

    /* helper pour récupérer l'id de l'article */
    select idArticle
    into idA
    from article
    where nom = nomA;

    /* si la ligne existe déjà on update */
    update ligne
    set quantite = quantite + qte
    where idcommande = idCmde
      and idarticle = idA;

    /* sinon on fait un insert */
    if (not FOUND) then
        insert into ligne(idcommande, idarticle, quantite) values (idCmde, idA, qte);
    end if;

    return true;
end;
$$
    language plpgsql;

create or replace function check_prix() returns trigger as
$$
begin
    if (new.prix < 5) then
        new.prix = 5;
    elsif (new.prix > 50) then
        new.prix = 50;
    end if;
    return new;
end;
$$
    language plpgsql;

drop trigger if exists tg_prix on article;
create trigger tg_prix
    before insert or update
    on article
    for each row
execute procedure check_prix();

select * from version();
