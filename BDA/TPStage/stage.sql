drop schema if exists stage cascade;
create schema stage;
set search_path to stage;

create table personne(
  id serial primary key,
  nom varchar not null,
  prenom varchar not null
);

create table moniteur(
  id serial primary key,
  nom varchar not null,
  prenom varchar not null,
  tauxHoraire decimal(5,2)
);

create table stage(
  id serial primary key,
  intitule varchar not null,
  debut date not null,
  fin date not null,
  montant decimal(5,2),
  idMoniteur int references moniteur(id)
);

create table inscrire(
  idPersonne int references personne(id),
  idStage int references stage(id),
  dateInscription date
);

insert into personne(nom,prenom) values
  ('Dupond','Jean'),('Duval','Marie');
  
insert into moniteur(nom,prenom,tauxHoraire) values
  ('Legrand','Mireille',15.50),('LeRouge','Marc',12.80);
  
insert into stage(intitule,debut,fin,montant) values
  ('dessin','2014-5-22','2014-6-2',450.00),
  ('peinture','2014-5-10','2014-5-23',300.00),
  ('musique','2014-5-13','2014-5-20',150.00);
  
select 'personne' as tablename, count(*) as "nb lignes" from personne
union
select 'moniteur', count(*) from moniteur
union
select 'stage',count(*) from stage
union
select 'inscrire', count(*) from inscrire;

