set search_path to stage;

create or replace function fct_personne()
    returns trigger as
$$
begin
    raise notice 'insertion de % %', new.prenom, new.nom;
    return new;
end
$$
    language plpgsql;

create trigger trg_personne
    after insert
    on personne
    for each row
execute procedure fct_personne();

create or replace function fct_success() returns trigger as
$$
begin
    raise notice 'insertion % % réussie', new.prenom, new.nom;
    return new;

end
$$
    language plpgsql;

create trigger trg_success
    after insert
    on personne
    for each row
execute procedure fct_success();

drop trigger trg_personne on personne;
drop trigger trg_success on personne;

create function fct_personne_count() returns trigger as
$$
declare
    ct bigint = (
        select count(*)
        from personne
    );
begin
    if TG_when = 'BEFORE' then
        raise notice 'nbr personnes: %', ct;
    else
        raise notice 'nbr personnes restantes: %', ct;
    end if;
    return old;
end
$$
    language plpgsql;

create trigger trg_personne_supp1
    before delete
    on personne
    for each row
execute procedure fct_personne_count();

create trigger trg_personne_supp1
    after delete
    on personne
    for each row
execute procedure fct_personne_count();

create function fct_horaire() returns trigger as
$$
begin
    if new.tauxhoraire not between 10.00 and 50.00 then
        return null;
    else
        return new;
    end if;
end;
$$
    language plpgsql;

create trigger trg_horaire
    before insert or update
    on moniteur
    for each statement
execute procedure fct_horaire();

create or replace function fct_inscription()
    returns trigger as
$$
begin
    if new.debut > new.fin then
        return null;
    else
        return new;
    end if;
end
$$
    language plpgsql;

create trigger trg_inscription
    before update or insert
    on stage
    for each statement
execute procedure fct_inscription();

create table evenements
(
    id          serial primary key,
    dateEtHeure timestamp,
    action      text,
    tablename   text
);

create function fct_evenement() returns trigger as
$$
begin
    if TG_OP = 'insert' or TG_OP = 'update' then
        return new;
    else
        return old;
    end if;
end
$$
    language plpgsql;

create trigger trg_evenement_personne
    after insert or update or delete
    on personne
    for each row
execute procedure fct_evenement();

create trigger trg_evenement_inscrire
    after insert or update or delete
    on inscrire
    for each row
execute procedure fct_evenement();

create trigger trg_evenement_stage
    after insert or update or delete
    on stage
    for each row
execute procedure fct_evenement();

create trigger trg_evenement_responsable
    after insert or update or delete
    on moniteur
    for each row
execute procedure fct_evenement();
