
/**
 * TP4 - 2018/2019
 * <p>
 * Nous voulons faire un programme Java permettant d'analyser une requête/réponse DNS (comme en TD).
 * <p>
 * Un objet de la classe  AnalayseurTrameDNS permet d'analyser une trame Ethernet2 contenant une requête ou une réponse DNS (comme vu en cours/TD).
 * <p>
 * IMPORTANT !!!! A LIRE !!!
 * <p>
 * Nous supposerons que le message DNS contenu dans la trame est soit une requête DNS, soit une réponse DNS.
 * Dans le cas d'une requête DNS, on suppose qu'il s'agit d'une (et une seule) demande d'adresse IPv4 d'un domaine donné.
 * Dans le cas d'une réponse DNS, il s'agit d'une réponse à une requête du type précédemment mentionné.
 * <p>
 * Dans la suite vous devez coder les 6 méthodes manquantes et ne pas modifier les autres méthodes.
 * Après chaque définition de méthode vous pouvez les tester en lançant simplement le programme.
 * <p>
 * A la fin du fichier se trouve l'affichage que vous devriez avoir après avoir encoder l'ensemble des méthodes manquantes.
 *
 * @author Jean-François Condotta
 */

public class AnalyseurTrameDNS_TP4 {

	private String trame;            // la trame (une séquence de caractères correspondant à des chiffres hexadécimaux)
	private String enteteEthernet;    // la partie de la trame correspondant à l'entête Ethernet2
	private String messageEthernet;    // la partie de la trame correspondant au message Ethernet2
	private String enteteIP;        // la partie de la trame correspondant à l'entête IP
	private String messageIP;        // la partie de la trame correspondant au message IP
	private String enteteUDP;        // la partie de la trame correspondant à l'entête UDP
	private String messageUDP;        // la partie de la trame correspondant au message UDP
	private String enteteDNS;        // la partie de la trame correspondant à l'entête DNS
	private String messageDNS;        // la partie de la trame correspondant au message DNS

	private String adrIPsour;        // la partie de la trame correspondant à l'adresse IP source
	private String adrIPdest;        // la partie de la trame correspondant à l'adresse IP destination
	private int adrIPsource[];        // l'adresse IP destination
	private int adrIPdestination[];    // l'adresse IP source

	private String portUDPsour;        // la partie de la trame correspondant au port UDP source
	private String portUDPdest;        // la partie de la trame correspondant au port UDP destination
	private int portUDPsource;        // le port UDP source
	private int portUDPdestination;    // le port UDP destination


	private String champ2EnteteDNS;    // la partie de la trame correspondant au deuxieme champ de 16 bits de l'entete DNS
	private String champ3EnteteDNS;    // la partie de la trame correspondant au troisieme champ de 16 bits de l'entete DNS
	private String champ4EnteteDNS;    // la partie de la trame correspondant au quatrieme champ de 16 bits de l'entete DNS
	private int requeteOuReponseDNS;// indique si c'est une requête DNS (valeur 0) ou une réponse (valeur 1) à une requête DNS
	private int recursifOuIteratif; // indique le mode demandé/utilisé entre itératif (valeur 0) ou récursif (valeur 1)
	private int codeErreur;            // l'entier correspondant au code erreur de la requête/réponse DNS
	private int nbQuestions;        // le nombre de questions dans le message DNS
	private int nbReponses;            // le nombre de reponses dans le message DNS

	private String domaineQuesDNS;    // la partie de la trame correspondant au domaine dans la question DNS
	private String domaineQuestionDNS; // le domaine de la question

	private String reponseAdresseIP_DNS;// la partie de la trame correspondant à l'adresse IP donnée par le serveur DNS (si réponse et pas d'erreur)
	private int reponseAdrIP_DNS[];    // l'adresse IP donnée par le serveur DNS (si réponse et pas d'erreur)


	/**
	 * NE PAS MODIFIER
	 * Permet de construire un analyseur pour une trame donnée en paramètre.
	 *
	 * @param trame la trame à analyser.
	 */
	public AnalyseurTrameDNS_TP4(String trame) {
		this.trame = trame.replaceAll(" ", "");
		enteteEthernet = null;
		messageEthernet = null;
		enteteIP = null;
		messageIP = null;
		enteteUDP = null;
		messageUDP = null;
		enteteDNS = null;
		messageDNS = null;
		adrIPsource = null;
		adrIPdestination = null;
		adrIPsour = null;
		adrIPdest = null;
		portUDPsour = null;
		portUDPdest = null;
		portUDPsource = -1;
		portUDPdestination = -1;

		champ2EnteteDNS = null;
		champ3EnteteDNS = null;
		champ4EnteteDNS = null;
		requeteOuReponseDNS = -1;
		codeErreur = -1;
		recursifOuIteratif = -1;
		nbQuestions = -1;
		nbReponses = -1;

		domaineQuesDNS = null;
		domaineQuestionDNS = null;
		reponseAdresseIP_DNS = null;
		reponseAdrIP_DNS = null;
	}


	/**
	 * Question 1 : écrire le code de la méthode extractionDifferentesParties() permettant d'initialiser les attributs :
	 * <p>
	 * private String enteteEthernet;	// la partie de la trame correspondant à l'entête Ethernet2
	 * private String messageEthernet;	// la partie de la trame correspondant au message Ethernet2
	 * private String enteteIP;		// la partie de la trame correspondant à l'entête IP
	 * private String messageIP;		// la partie de la trame correspondant au message IP
	 * private String enteteUDP;		// la partie de la trame correspondant à l'entête UDP
	 * private String messageUDP;		// la partie de la trame correspondant au message UDP
	 * private String enteteDNS;		// la partie de la trame correspondant à l'entête DNS
	 * private String messageDNS;		// la partie de la trame correspondant au message DNS
	 */
	private void extractionDifferentesParties() {
		enteteEthernet = trame.substring(0, 28);
		messageEthernet = trame.substring(28);

		enteteIP = messageEthernet.substring(0, 40);
		messageIP = messageEthernet.substring(40);

		enteteUDP = messageIP.substring(0, 16);
		messageUDP = messageIP.substring(16);

		enteteDNS = messageIP.substring(0, 24);
		messageDNS = messageIP.substring(24);
	}


	/**
	 * NE PAS MODIFIER
	 * Méthode utilitaire permettant de convertir un caractère représentant un chiffre hexadécimal en l'entier décimal correspondant.
	 * Exemple : hexaToDec('f') => 15
	 *
	 * @param chiffreHexa un caractère représentant un chiffre hexadécimal.
	 * @return la conversion du caractère chiffreHexa en un entier compris entre 0 et 15.
	 */
	private int hexaToDec(char chiffreHexa) {
		int valHexa = 0;
		switch (chiffreHexa) {
			case '0':
				valHexa = 0;
				break;
			case '1':
				valHexa = 1;
				break;
			case '2':
				valHexa = 2;
				break;
			case '3':
				valHexa = 3;
				break;
			case '4':
				valHexa = 4;
				break;
			case '5':
				valHexa = 5;
				break;
			case '6':
				valHexa = 6;
				break;
			case '7':
				valHexa = 7;
				break;
			case '8':
				valHexa = 8;
				break;
			case '9':
				valHexa = 9;
				break;
			case 'A' | 'a':
				valHexa = 10;
				break;
			case 'B' | 'b':
				valHexa = 11;
				break;
			case 'C' | 'c':
				valHexa = 12;
				break;
			case 'D' | 'd':
				valHexa = 13;
				break;
			case 'E' | 'e':
				valHexa = 14;
				break;
			case 'F' | 'f':
				valHexa = 15;
				break;
			default:
				System.out.println("hexaToDec => le caractère " + chiffreHexa + " n'est pas un chiffre hexadécimal !");
				System.exit(1);
		}
		return valHexa;
	}


	/**
	 * Question 2 : écrire le code de la méthode extractionAdressesIP() permettant d'initialiser les attributs :
	 * <p>
	 * private String adrIPsour;		// la partie de la trame correspondant à l'adresse IP source
	 * private String adrIPdest;		// la partie de la trame correspondant à l'adresse IP destination
	 * private int adrIPsource[];		// l'adresse IP destination
	 * private int adrIPdestination[];	// l'adresse IP source
	 */
	private void extractionAdressesIP() {
		adrIPsour = enteteIP.substring(24, 32);
		adrIPdest = enteteIP.substring(32, 40);

		adrIPsource = new int[4];
		adrIPdestination = new int[4];

		// on transforme les ip de hexa à décimaux
		for (int i = 0; i < 4; i++) {
			adrIPsource[i] = 16 * hexaToDec(adrIPsour.charAt(2 * i)) + hexaToDec(adrIPsour.charAt(2 * i + 1));
			adrIPdestination[i] = 16 * hexaToDec(adrIPdest.charAt(2 * i)) + hexaToDec(adrIPdest.charAt(2 * i + 1));
		}


	}

	/**
	 * Question 3 : écrire le code de la méthode extractionPortUDP() permettant d'initialiser les attributs :
	 * <p>
	 * private String portUDPsour;		// la partie de la trame correspondant au port UDP source
	 * private String portUDPdest;		// la partie de la trame correspondant au port UDP destination
	 * private int portUDPsource;		// le port UDP source
	 * private int portUDPdestination;	// le port UDP destination
	 */
	private void extractionPortUDP() {
		portUDPdest = enteteUDP.substring(0, 4);
		portUDPsour = enteteUDP.substring(4, 8);

		portUDPdestination = 16 ^ 3 * hexaToDec(portUDPdest.charAt(0)) + 16 ^ 2 * hexaToDec(portUDPdest.charAt(1)) + 16 * hexaToDec(portUDPdest.charAt(2)) + hexaToDec(portUDPdest.charAt(3));
		portUDPsource = 16 ^ 3 * hexaToDec(portUDPsour.charAt(0)) + 16 ^ 2 * hexaToDec(portUDPsour.charAt(1)) + 16 * hexaToDec(portUDPsour.charAt(2)) + hexaToDec(portUDPsour.charAt(3));
	}


	/**
	 * NE PAS MODIFIER
	 * Méthode utilitaire permettant d'extraire un bit d'un nombre entier (supposé codé sur 16bits).
	 *
	 * @param numBit Cet entier correspond au numéro du bit à extraire, 0 pour le bit de poids le plus faible (le plus à droite), 15 pour le bit de poids le plus fort (le plus à gauche)
	 * @param nombre Le nombre entier positif pour lequel on veut faire l'extraction
	 * @return le bit correspondant (0 ou 1)
	 */
	private int extractBit(int numBit, int nombre) {
		int masque = 1;
		for (int i = 1; i <= numBit; i++) masque = masque * 2;
		if ((masque & nombre) != 0) return 1;
		else return 0;
	}

	/**
	 * Question 4 : écrire le code de la méthode extractionDNS() permettant d'initialiser les attributs :
	 * <p>
	 * private String champ2EnteteDNS;	// la partie de la trame correspondant au deuxieme champ de 16 bits de l'entete DNS
	 * private String champ3EnteteDNS;	// la partie de la trame correspondant au troisieme champ de 16 bits de l'entete DNS
	 * private String champ4EnteteDNS;	// la partie de la trame correspondant au quatrieme champ de 16 bits de l'entete DNS
	 * private int requeteOuReponseDNS;// indique si c'est une requête DNS (valeur 0) ou une réponse (valeur 1) à une requête DNS
	 * private int codeErreur;			// l'entier correspondant au code erreur de la requête/réponse DNS
	 * private int recursifOuIteratif; // indique le mode demandé/utilisé entre itératif (valeur 0) ou récursif (valeur 1)
	 * private int nbQuestions;		// le nombre de questions dans le message DNS
	 * private int nbReponses;			// le nombre de reponses dans le message DNS
	 */
	private void extractionDNS() {

	}

	/**
	 * Question 5 : écrire le code de la méthode extractionDomaineDNS() permettant d'initialiser les attributs :
	 * <p>
	 * private String domaineQuesDNS;	  // la partie de la trame correspondant au domaine dans la question DNS
	 * private String domaineQuestionDNS; // le domaine de la question
	 */
	private void extractionDomaineDNS() {
		if (nbQuestions == 1) {


		}
	}

	/**
	 * Question 6 : écrire le code de la méthode extractionIPDNS() permettant d'initialiser les attributs :
	 * <p>
	 * private String reponseAdresseIP_DNS;// la partie de la trame correspondant à l'adresse IP donnée par le serveur DNS (si réponse et pas d'erreur)
	 * private int reponseAdrIP_DNS[]; 	// l'adresse IP donnée par le serveur DNS (si réponse et pas d'erreur)
	 */
	private void extractionIPDNS() {
		if ((nbReponses == 1) && (codeErreur == 0)) {


		}
	}


	/**
	 * NE PAS MODIFIER
	 * Méthode affichant les résultats de l'analyse
	 */
	public void printInfos() {
		extractionDifferentesParties();
		System.out.println("Trame : \t\t" + trame);
		if (enteteEthernet != null) System.out.println("Entête Ethernet : \t" + enteteEthernet);
		else System.out.println("Entête Ethernet : \t?");
		if (messageEthernet != null) System.out.println("Message Ethernet : \t" + messageEthernet);
		else System.out.println("Message Ethernet : \t?");
		if (enteteIP != null) System.out.println("Entête IP : \t\t" + enteteIP);
		else System.out.println("Entête IP : \t\t?");
		if (messageIP != null) System.out.println("Message IP : \t\t" + messageIP);
		else System.out.println("Message IP : \t\t?");
		if (enteteUDP != null) System.out.println("Entête UDP : \t\t" + enteteUDP);
		else System.out.println("Entête UDP : \t\t?");
		if (messageUDP != null) System.out.println("Message UDP : \t\t" + messageUDP);
		else System.out.println("Message UDP : \t\t?");
		if (enteteDNS != null) System.out.println("Entête DNS : \t\t" + enteteDNS);
		else System.out.println("Entête DNS : \t\t?");
		if (messageDNS != null) System.out.println("Message DNS : \t\t" + messageDNS);
		else System.out.println("Message DNS : \t\t?");
		extractionAdressesIP();
		if (adrIPsour != null) System.out.println("IP source (hexa) : \t" + adrIPsour);
		else System.out.println("IP source (hexa) : \t ?");
		if (adrIPsource != null)
			System.out.println("IP source : \t\t" + adrIPsource[0] + "." + adrIPsource[1] + "." + adrIPsource[2] + "." + adrIPsource[3]);
		else System.out.println("IP source : \t\t ?.?.?.?");
		if (adrIPdest != null) System.out.println("IP dest. (hexa) : \t" + adrIPdest);
		else System.out.println("IP dest. (hexa) :  ?");
		if (adrIPdestination != null)
			System.out.println("IP destination : \t" + adrIPdestination[0] + "." + adrIPdestination[1] + "." + adrIPdestination[2] + "." + adrIPdestination[3]);
		else System.out.println("IP destination : \t ?.?.?.?");
		extractionPortUDP();
		if (portUDPsour != null) System.out.println("Port source (hexa) : \t" + portUDPsour);
		else System.out.println("Port source (hexa) : \t ?");
		if (portUDPsource != -1) System.out.println("Port source : \t\t" + portUDPsource);
		else System.out.println("Port source : \t\t ?");
		if (portUDPdest != null) System.out.println("Port dest. (hexa) : \t" + portUDPdest);
		else System.out.println("Port dest. (hexa) : \t ?");
		if (portUDPdestination != -1) System.out.println("Port destination : \t" + portUDPdestination);
		else System.out.println("Port destination : \t ?");
		extractionDNS();
		if (champ2EnteteDNS != null) System.out.println("Champ 2 DNS : \t\t" + champ2EnteteDNS);
		else System.out.println("Champ 2 DNS : \t\t ?");
		if (champ3EnteteDNS != null) System.out.println("Champ 3 DNS : \t\t" + champ3EnteteDNS);
		else System.out.println("Champ 3 DNS : \t\t ?");
		if (champ4EnteteDNS != null) System.out.println("Champ 4 DNS : \t\t" + champ4EnteteDNS);
		else System.out.println("Champ 4 DNS : \t\t ?");
		if (requeteOuReponseDNS == 0) System.out.println("Requête/Réponse : \t" + "Requête DNS");
		else if (requeteOuReponseDNS == 1) System.out.println("Requête/Réponse : \t" + "Réponse DNS");
		else System.out.println("Requête/Réponse : \t ?");
		if (codeErreur != -1) System.out.println("Code Erreur DNS : \t" + codeErreur);
		else System.out.println("Code Erreur DNS : \t ?");

		if (recursifOuIteratif == 0) System.out.println("Itératif/Récursif : \t" + "Itératif");
		else if (recursifOuIteratif == 1) System.out.println("Itératif/Récursif : \t" + "Récursif");
		else System.out.println("Itératif/Récursif : \t ?");

		if (nbQuestions != -1) System.out.println("Nb. Questions DNS : \t" + nbQuestions);
		else System.out.println("Nb. Questions DNS : \t ?");
		if (nbReponses != -1) System.out.println("Nb. Réponses DNS : \t" + nbReponses);
		else System.out.println("Nb. Réponses DNS : \t ?");
		extractionDomaineDNS();
		if (domaineQuesDNS != null) System.out.println("Domaine Q (encodé) : \t" + domaineQuesDNS);
		else System.out.println("Domaine Q (encodé) : \t ?");
		if (domaineQuestionDNS != null) System.out.println("Domaine Q : \t\t" + domaineQuestionDNS);
		else System.out.println("Domaine Q : \t\t ?");
		extractionIPDNS();
		if (reponseAdresseIP_DNS != null) System.out.println("IP réponse DNS (hexa) : " + reponseAdresseIP_DNS);
		else System.out.println("IP réponse DNS (hexa) :  ?");
		if (reponseAdrIP_DNS != null)
			System.out.println("IP réponse DNS : \t" + reponseAdrIP_DNS[0] + "." + reponseAdrIP_DNS[1] + "." + reponseAdrIP_DNS[2] + "." + reponseAdrIP_DNS[3]);
		else System.out.println("IP réponse DNS : \t ?.?.?.?");


	}


	public static void main(String[] args) {
		AnalyseurTrameDNS_TP4 analyseur;

		String trame1 = "d8a2 5e97 fda9 4c09 b404 7702 0800 4500"
				+ "0059 0000 4000 4011 b72b c0a8 0101 c0a8"
				+ "0117 0035 ecd4 0045 2d2a 71b3 8180 0001"
				+ "0001 0000 0000 0377 7777 0869 7574 2d6c"
				+ "656e 730b 756e 6976 2d61 7274 6f69 7302"
				+ "6672 0000 ff00 01c0 0c00 0100 0100 0151"
				+ "1b00 04c1 3173 47";

		String trame2 = "4c09 b404 7702 d8a2 5e97 fda9 0800 4500"
				+ "0049 185e 0000 4011 dedd c0a8 0117 c0a8"
				+ "0101 d33a 0035 0035 61a8 7fb6 0100 0001"
				+ "0000 0000 0000 0377 7777 0869 7574 2d6c"
				+ "656e 730b 756e 6976 2d61 7274 6f69 7302"
				+ "6672 0000 ff00 01";


		analyseur = new AnalyseurTrameDNS_TP4(trame1);
		System.out.println("\n******************** Analyse de la Trame 1 ************************\n\n");
		analyseur.printInfos();

		analyseur = new AnalyseurTrameDNS_TP4(trame2);
		System.out.println("\n******************** Analyse de la Trame 2 ************************\n\n");
		analyseur.printInfos();

	}
}

/*

******************** Analyse de la Trame 1 ************************


Trame : 		d8a25e97fda94c09b4047702080045000059000040004011b72bc0a80101c0a801170035ecd400452d2a71b38180000100010000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001c00c000100010001511b0004c1317347
Entête Ethernet : 	d8a25e97fda94c09b40477020800
Message Ethernet : 	45000059000040004011b72bc0a80101c0a801170035ecd400452d2a71b38180000100010000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001c00c000100010001511b0004c1317347
Entête IP : 		45000059000040004011b72bc0a80101c0a80117
Message IP : 		0035ecd400452d2a71b38180000100010000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001c00c000100010001511b0004c1317347
Entête UDP : 		0035ecd400452d2a
Message UDP : 		71b38180000100010000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001c00c000100010001511b0004c1317347
Entête DNS : 		71b381800001000100000000
Message DNS : 		03777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001c00c000100010001511b0004c1317347
IP source (hexa) : 	c0a80101
IP source : 		192.168.1.1
IP dest. (hexa) : 	c0a80117
IP destination : 	192.168.1.23
Port source (hexa) : 	0035
Port source : 		53
Port dest. (hexa) : 	ecd4
Port destination : 	60628
Champ 2 DNS : 		8180
Champ 3 DNS : 		0001
Champ 4 DNS : 		0001
Requête/Réponse : 	Réponse DNS
Code Erreur DNS : 	0
Itératif/Récursif : 	Récursif
Nb. Questions DNS : 	1
Nb. Réponses DNS : 	1
Domaine Q (encodé) : 	03777777086975742d6c656e730b756e69762d6172746f697302667200
Domaine Q : 		www.iut-lens.univ-artois.fr.
IP réponse DNS (hexa) : c1317347
IP réponse DNS : 	193.49.115.71

******************** Analyse de la Trame 2 ************************


Trame : 		4c09b4047702d8a25e97fda9080045000049185e00004011deddc0a80117c0a80101d33a0035003561a87fb60100000100000000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001
Entête Ethernet : 	4c09b4047702d8a25e97fda90800
Message Ethernet : 	45000049185e00004011deddc0a80117c0a80101d33a0035003561a87fb60100000100000000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001
Entête IP : 		45000049185e00004011deddc0a80117c0a80101
Message IP : 		d33a0035003561a87fb60100000100000000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001
Entête UDP : 		d33a0035003561a8
Message UDP : 		7fb60100000100000000000003777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001
Entête DNS : 		7fb601000001000000000000
Message DNS : 		03777777086975742d6c656e730b756e69762d6172746f69730266720000ff0001
IP source (hexa) : 	c0a80117
IP source : 		192.168.1.23
IP dest. (hexa) : 	c0a80101
IP destination : 	192.168.1.1
Port source (hexa) : 	d33a
Port source : 		54074
Port dest. (hexa) : 	0035
Port destination : 	53
Champ 2 DNS : 		0100
Champ 3 DNS : 		0001
Champ 4 DNS : 		0000
Requête/Réponse : 	Requête DNS
Code Erreur DNS : 	0
Itératif/Récursif : 	Récursif
Nb. Questions DNS : 	1
Nb. Réponses DNS : 	0
Domaine Q (encodé) : 	03777777086975742d6c656e730b756e69762d6172746f697302667200
Domaine Q : 		www.iut-lens.univ-artois.fr.
IP réponse DNS (hexa) :  ?
IP réponse DNS : 	 ?.?.?.?

*/
