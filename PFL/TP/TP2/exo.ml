Exercice 1

let rec taille l = match l with
|[] -> 0
|_::q -> 1 + (taille q);;

let rec somme l = match l with
| [] -> 0
|a::q -> a + somme q;;

let rec moy l = float_of_int (somme l) /. float_of_int (taille l);;

let rec applique_liste f l = match l with
|[] -> []
|t::q -> (f t)::(applique_liste f q);;

let rec min l = match l with
| [] -> failwith "erreur"
| [a] -> a
| t::q -> let x = min q in if t < x then t else x;;

Exercice 2

1) let n_a_un n = if (n < 1) then failwith "Erreur" else if n = 1 then [1] else n::(n_a_un(n-1));;

let n_a_un n = if (n < 1) then failwith "Erreur" else
	let rec n_a_unBis n l i = if i = 0 then l else n_a_unBis n ((n-i)::l) (i-1)
in n_a_unBis n [] n;;

2) let un_a_n n = 
	let rec ajout n l = if n < 1 then failwith "Erreur" else if n = 1 then 1::l else ajout (n-1) (n::l)
in ajout n [];;

3) let supprime l n = if (taille l) < n then failwith "Erreur" else 
	let rec supprimeBis l' n' = match (l', n') with
		| (l', 0) -> l'
		| (_::r, n') -> supprimeBis r (n'-1)
	in supprimeBis l n;;

4) let long_prefixe l =
	let rec long l f acc = match l with
		| [] -> acc
		| f::r -> if q = f long r (acc + 1)
	in match l with
		| [] -> 0
		| f::r -> long r f 1

5) let inverser l = 
	let rec inverserBis l acc = match l with
		| [] -> acc
		| f::l'-> inverserBis l' (f::acc)
in inverserBis l [];;

6) let long_suffixe l = long_prefixe (inverser l);;

7) let rec appartient e l = match l with
	| [] -> false
	| a::r -> if a = e then true else appartient e r;;
	
8) let nb_occ e l =
	let rec count acc e' l' = match l' with
		| [] -> acc
		| a::r -> if a = e' then count (acc + 1) e' r else count acc e' r
	in count 0 e l;;
	
9) let ensemble l = let rec boucle l acc = match l with
	| [] -> acc
	| a::r -> if appartient a result then boucle r acc else boucle r a::acc
in boucle l [];;

10) let regroupe l = 
let rec regroupeBis l l' acc = match l' with
	| [] -> acc
	| a::r -> regroupeBis l r (a, nb_occ a l)::acc
in regroupeBis l (ensemble l) [];;

11) let inclus l l' = 
let rec inclusBis l l' = match l with
	| [] -> true
	| a::r -> if appartient a l' then inclusBis r l' else false
in inclusBis l l';;

12) let est_egale liste1 liste2 = 
let rec est_egaleBis l l' = match l with
	| [] -> true
	| a::r if appartient a l' && nb_occ a l' then est_egaleBis r l' else false
in est_egaleBis liste1 liste2;;

13) let subst a b l = 
let rec substBis a' b' l' acc = match l' with
	| [] -> inverser acc
	| n::r -> if n = a' then substBis a' b' r b'::acc else substBis a' b' r n::acc
in substBis a b l [];;

14) let subst1 a b liste =
let rec substi a' b' l' i acc = match l' with
	| [] -> inverser acc
	| n::r -> if n = a' && i > 0 then subst1Bis a' b' r (i-1) b'::acc else substBis a' b' r i n::acc
in subst1Bis a b l 1 [];;

Exercice 3

1) let inserer sorted n = 
let rec insererBis l n' i acc = match l with
	| [] -> acc
    | a::r -> if n' > a && i > 0 then insererBis r n' (i-1) (a::n'::acc) else insererBis r n' i (a::acc)
in insererBis (inverser sorted) n 1 [];;

2) let triInsertion l =
let rec sort l' acc = match l' with
	| [] -> acc
	| a::r -> sort r (inserer acc a)
in sort l [];;

let rec merge_sort l = let rec merge (l1, l2) = match (l1, l2) with
    | list, [] -> list
    | [], list -> list
    | t1::r1, t2::r2 when t1 <= t2 -> t1::merge (l1, t2::l2)
    | t1::r1, t2::r2 -> t2::merge (t1::l1, l2)
  in let rec half l = match l with
      | [] -> [], []
      | t::r -> let l1, l2 = half r in (t::l2, l1)
  in match l with
  | [] -> []
  | list -> let l1, l2 = half list in merge (merge_sort l1, merge_sort l2)
;;

décrire les concept suivant dans OCaml : polymorphisme, fonction anonyme, déclaration globale et déclaration locale.

une fonction anonyme est une fonction qui ne possède pas de nom, 
c'est une syntaxe différente pour l'écriture de fonction

une déclaration globale est lorsque l'on déclare une variable dans la globalité
de notre programme, elle est donc disponible partout après sa déclaration

une déclration locale est lorsque l'on déclare une variable mais uniquement
pour une fonction ou un sous-ensemble de fonction.