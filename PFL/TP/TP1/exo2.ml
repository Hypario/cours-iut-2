
(* 1 *)
let square x = x * x;;
let square x = x ** 2.;;
square 2.;;

(* 2 *)
let abs x = if x < 0 then x * -1 else x;;

(* 3 *)
let even x = (x mod 2) = 0;;

(* 4 *)
let divisible x y = (x mod y) = 0;;

(* 5 *)
let before x y = x < b;;
let before x y = (Char.code x) < (Char.code b);;

(* 6 *)
let implication a b = (not a) || b;;