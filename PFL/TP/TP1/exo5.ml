let rec fact x = if x = 0 then 1 else x * fact (x - 1);;
let rec fact n = match n with
| 0 -> 1
| _ -> n * fact(n-1);;

let rec fibo x = if x <= 2 then 1 else fibo (x - 1) + fibo (x - 2);;
let rec fibo n = match n with
| 0 -> 1
| 1 -> 1
| _ -> fibo (n - 1) + fibo (n - 2);;
let rec fibo n a b = if n = 0 then a else if n = 1 then b else (fibo (n-1) b (a+b));;

let rec syra x n = if n = 0 then x else if x mod 2 = 0 then (syra (x/2) n-1) else (syra (3*x+1) n-1);;

let rec syra x n y = if n = 0 then y else if y mod 2 = 0 then (syra x (n-1) (y/2)) else (syra x n-1 3*y+1);; permet de garder la valeur de départ

let rec syr x n = x::(if n = 0 then [] else if x mod 2 = 0 then (syra (x/2) (n-1)) else (syra (3*y+1) (n-1)));;

let rec pgcd a b = if a != b then if a > b then (pgcd (a-b) b) else (pgcd a (b - a)) else a;;

let rec coefbino k n = if n < k then 0 else if n = k then 1 else if k = 0 then 1 else (coefbino (k-1)  (n-1)) + (coefbino k (n-1));;

let rec dec x n = 
let nb_part y m = if (y = 0) || (y < m) then 0 else if m = y then 1 else (nb_part(y-1) (m-1)) + (nb_part(y-m) m) in 
let rec nb_part2 x n = match n with
| 0 -> 0
| _ -> (nb_part x n) + (nb_part2 x (n-1))
in nb_part2 x n;

let somme1 n m = 
    let rec fct n m result i j = 
    if i > m then 
        result 
    else if j > m then 
        fct n m result (i+1) (i+1) 
    else fct n m (j+result) i (j+1) 
    in 
fct n m 0 n n;;

