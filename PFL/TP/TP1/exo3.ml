let conjuge x s = let radical = String.sub s 0 ((String.length s) -2) 
in
if x = 1 then radical ^ "e" 
else if x = 2 then radical ^ "es" 
else if x = 2 then radical ^ "ons" 
else if x = 3 then radical ^ "ez" 
else if x = 4 then radical ^ "ent"
else "not possible";;

exercice 4

let poly (a, b, c) x = a * (x * x) + b * x + c;;
let polyNC (a, b) x = a * (x * x) + b *x;;
let polyNB (a, c) x = a * (x * x) + c;;
